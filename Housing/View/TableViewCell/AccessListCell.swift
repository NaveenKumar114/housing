//
//  AccessListCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-22.
//

import UIKit

class AccessListCell: UITableViewCell {

    @IBOutlet weak var accessName: UILabel!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
