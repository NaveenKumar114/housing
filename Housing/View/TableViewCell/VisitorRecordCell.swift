//
//  VisitorRecordCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-23.
//

import UIKit

class VisitorRecordCell: UITableViewCell {
    var additionalSeparator:UIView = UIView()

    @IBOutlet weak var outTime: UILabel!
    @IBOutlet weak var outDate: UILabel!
    @IBOutlet weak var inTime: UILabel!
    @IBOutlet weak var inDate: UILabel!
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        self.createSeparator()
        setConstraintForSeparator()
        // Configure the view for the selected state
    }
    func createSeparator() {

          self.additionalSeparator.translatesAutoresizingMaskIntoConstraints = false
          self.contentView.addSubview(self.additionalSeparator)
      }
    func setConstraintForSeparator() {
          self.additionalSeparator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: self.separatorInset.left).isActive = true
          self.additionalSeparator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: -self.separatorInset.right).isActive = true
          self.additionalSeparator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
          self.additionalSeparator.heightAnchor.constraint(equalToConstant: 2).isActive = true
        self.additionalSeparator.backgroundColor = UIColor.darkGray
      }
}
