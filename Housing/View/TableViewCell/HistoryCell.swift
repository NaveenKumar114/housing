//
//  HistoryCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-12.
//

import UIKit
import SwipeCellKit
class HistoryCell: UITableViewCell {

    @IBOutlet weak var heathImage: UIImageView!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var type: UILabel!
    @IBOutlet weak var favButton: UIButton!
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var name: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func layoutSubviews() {
        super.layoutSubviews()

        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
    }
}
