//
//  ServicesCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-12.
//

import UIKit

class ServicesCell: UITableViewCell {

    @IBOutlet weak var number: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var numberButton: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
