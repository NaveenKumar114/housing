//
//  NoticeCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-18.
//

import UIKit

class NoticeCell: UITableViewCell {

    @IBOutlet weak var detailLabel: UILabel!
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var announcementImage: UIImageView!
    @IBOutlet weak var dateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
