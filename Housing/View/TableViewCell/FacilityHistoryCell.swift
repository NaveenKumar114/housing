//
//  FacilityHistory.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-05.
//

import UIKit

class FacilityHistoryCell: UITableViewCell {

    @IBOutlet weak var facilityName: UILabel!
    @IBOutlet weak var active: UILabel!
    @IBOutlet weak var dateAndTime: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
