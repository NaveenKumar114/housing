//
//  IntercomCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-14.
//

import UIKit

class IntercomCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    @IBOutlet weak var numberLabel: UILabel!
    @IBOutlet weak var timingLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
