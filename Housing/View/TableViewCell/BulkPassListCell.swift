//
//  BulkPassListCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-30.
//

import UIKit

class BulkPassListCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var icOrPassport: UILabel!
    @IBOutlet weak var proofPhoto: UIImageView!
    @IBOutlet weak var facePhoto: UIImageView!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var phoneNumber: UILabel!
    @IBOutlet weak var vehicle: UILabel!
    @IBOutlet weak var mainView: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
