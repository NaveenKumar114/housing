//
//  StatementOfAccountCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-08.
//

import UIKit

class StatementOfAccountCell: UITableViewCell {
    @IBOutlet weak var paidBy: UILabel!
    @IBOutlet weak var name: UILabel!
    var additionalSeparator:UIView = UIView()

    @IBOutlet weak var attachment: UIImageView!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var dateAndTime: UILabel!
    @IBOutlet weak var amount: UILabel!
    @IBOutlet weak var status: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        attachment.layer.cornerRadius = 20
        self.createSeparator()
        setConstraintForSeparator()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func createSeparator() {

          self.additionalSeparator.translatesAutoresizingMaskIntoConstraints = false
          self.contentView.addSubview(self.additionalSeparator)
      }
    func setConstraintForSeparator() {
          self.additionalSeparator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0).isActive = true
          self.additionalSeparator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0).isActive = true
          self.additionalSeparator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
          self.additionalSeparator.heightAnchor.constraint(equalToConstant: 2).isActive = true
        self.additionalSeparator.backgroundColor = UIColor.darkGray
      }
}
