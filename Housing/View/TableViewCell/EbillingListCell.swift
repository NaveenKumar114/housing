//
//  EbillingListCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-06.
//

import UIKit

class EbillingListCell: UITableViewCell {
    var additionalSeparator:UIView = UIView()

    @IBOutlet weak var billAttachment: UIButton!
    
    @IBOutlet weak var afterDue: UILabel!
    @IBOutlet weak var billTo: UILabel!
    @IBOutlet weak var billFrom: UILabel!
    @IBOutlet weak var billName: UILabel!
    @IBOutlet weak var status: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var amount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        billAttachment.layer.cornerRadius = 20
        self.createSeparator()
        setConstraintForSeparator()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func createSeparator() {

          self.additionalSeparator.translatesAutoresizingMaskIntoConstraints = false
          self.contentView.addSubview(self.additionalSeparator)
      }
    func setConstraintForSeparator() {
          self.additionalSeparator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor, constant: 0).isActive = true
          self.additionalSeparator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor, constant: 0).isActive = true
          self.additionalSeparator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor, constant: 0).isActive = true
          self.additionalSeparator.heightAnchor.constraint(equalToConstant: 2).isActive = true
        self.additionalSeparator.backgroundColor = UIColor.darkGray
      }
}
