//
//  CurvedButton.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-14.
//

import UIKit

@IBDesignable class CurvedButton: UIButton {

    public func setupShadow() {
        //print("Setup shadow!")
        self.layer.cornerRadius = self.frame.height / 2
        //self.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        self.layer.borderWidth = 2
        self.layer.borderColor = UIColor.darkGray.cgColor

    
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //print("Layout subviews!")
        setupShadow()
    }

}
