//
//  CurvedButtonNoBorder.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-02.
//

import Foundation
import UIKit
@IBDesignable class CurvedButtonNoBorder: UIButton {

    public func setupShadow() {
        //print("Setup shadow!")
        self.layer.cornerRadius = self.frame.height / 2
        //self.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
    //    self.layer.borderWidth = 2
      //  self.layer.borderColor = UIColor(named: "AccentColor")?.cgColor

    
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //print("Layout subviews!")
        setupShadow()
    }

}
