//
//  CircularButton.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-09.
//

import UIKit

@IBDesignable class CircularButton: UIButton {

    override func layoutSubviews() {
         super.layoutSubviews()
         if (frame.width != frame.height) {
//             NSLog("Ended up with a non-square frame -- so it may not be a circle");
         }
         layer.cornerRadius = frame.width / 2
         layer.masksToBounds = true
        //layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        layer.borderColor = UIColor.darkGray.cgColor

        layer.borderWidth = 2
     }

}
