//
//  CurvedView.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-10.
//

import UIKit
import Foundation
@IBDesignable class CurvedView: UIView {
    public func setupShadow() {
        //print("Setup shadow!")
        self.layer.cornerRadius = self.frame.height / 2
        //self.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        self.layer.borderColor = UIColor.darkGray.cgColor
        self.layer.borderWidth = 2
    
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        //print("Layout subviews!")
        setupShadow()
    }

}
