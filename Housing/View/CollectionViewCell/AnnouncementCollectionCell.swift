//
//  AnnouncementCollectionCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-17.
//

import UIKit

class AnnouncementCollectionCell: UICollectionViewCell {

    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var announceLabel: UILabel!
    @IBOutlet weak var announceView: UIView!
    @IBOutlet weak var announceImage: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        //announceView.layer.cornerRadius = 5
       // labelView.layer.cornerRadius = 5
      //  labelView.layer.borderWidth = 2
        //labelView.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
        //labelView.backgroundColor = ConstantsUsedInProject.appThemeColor
        //labelView.alpha = 0.2
    }
    
}
