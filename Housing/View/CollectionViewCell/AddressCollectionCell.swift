//
//  AddressCollectionCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-19.
//

import UIKit

class AddressCollectionCell: UICollectionViewCell {

    @IBOutlet weak var addressButton: CircularButton!
    @IBOutlet weak var addressImagr: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
