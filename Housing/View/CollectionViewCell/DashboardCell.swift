//
//  DashboardCell.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-06.
//

import UIKit

class DashboardCell: UICollectionViewCell {

    @IBOutlet weak var iconImage1: UIImageView!
    @IBOutlet weak var cellHeight: NSLayoutConstraint!
    @IBOutlet weak var iconImage: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
