//
//  VisitorPassSecurity.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-10.
//

import UIKit

class VisitorPassSecurity: UIViewController {
    
    @IBOutlet weak var timeInLabel: UILabel!
    @IBOutlet weak var timeOutLabel: UILabel!
    @IBOutlet weak var vehicleNo: UILabel!
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var role: UILabel!
    @IBOutlet weak var contactTo: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var inOuOutTimeButton: CurvedButton!
    
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var rejectButton: CurvedButton!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var mobileNumber: UILabel!
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var facilityName: UILabel!
    var passData : SecurityVisitorPassJSON?
    var visitorIn = false  // false means visitor is entering
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.layer.cornerRadius = 25
        //self.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        rejectButton.layer.cornerRadius = rejectButton.frame.height / 2
        if passData != nil
        {
            if let safeData = passData?.visitor
            {
                userName.text = "\(safeData.name ?? "")"
                vehicleNo.text = ": \(safeData.vehicleNo ?? "")"
                role.text = ": \(safeData.category ?? "")"
                contactTo.text = ": \(safeData.createBy ?? "")"
                email.text = safeData.email
                mobileNumber.text = safeData.contactNo
                startLabel.text = ": \(convertDateFormaterDate(safeData.validityStart!)) \(safeData.startTime ?? "")"
                endLabel.text = ": \(convertDateFormaterDate(safeData.validityEnd!)) \(safeData.endTime ?? "")"
                facilityName.text = safeData.visitorType
                address.text = safeData.address
                if safeData.status == "REJECTED"
                {
                    rejectButton.isHidden = true
                    inOuOutTimeButton.isHidden = true
                    statusLabel.text = "REJECTED"
                    statusLabel.textColor = .red
                }
                else
                {
                testTime()
                }
            }
        }
    }
    
    func convertDateFormaterDate(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func testTime()
    {
        let date = NSDate()
        let formatterForDate = DateFormatter()
        formatterForDate.dateFormat = "yyyy-MM-dd"
        let currentDateFormat = formatterForDate.string(from: date as Date)
        let currentDate = formatterForDate.date(from: currentDateFormat)!
        
        let formatterForTime = DateFormatter()
        formatterForTime.dateFormat = "HH:mm:ss"
        let currentTimeFormat = formatterForTime.string(from: date as Date)
        let currentTIme = formatterForTime.date(from: currentTimeFormat)!
        let startDate = formatterForDate.date(from: (passData?.visitor?.validityStart)!)!
      //  let endDate = formatterForDate.date(from: (passData?.visitor?.validityEnd)!)!
        let startTime = formatterForTime.date(from: (passData?.visitor?.startTime)!)!
        //let endTime = formatterForTime.date(from: (passData?.visitor?.endTime)!)!
        var passExpired = false
        var passNotOpen = false
        if currentDate > startDate
        {
            passExpired = true
        }
        else if currentDate < startDate
        {
            passNotOpen = true
        }
        else if currentTIme > startTime
        {
            passExpired = true
        }
        else if currentTIme < startTime
        {
            passNotOpen = true
        }
        if passExpired
        {
            rejectButton.isHidden = true
            inOuOutTimeButton.isHidden = true
            statusLabel.text = "PASS EXPIRED"
            statusLabel.textColor = .red
        }
        if passNotOpen
        {
            rejectButton.isHidden = true
            inOuOutTimeButton.isHidden = true
            statusLabel.text = "PASS NOT ACTIVE"
            statusLabel.textColor = .red
        }
    //    let currentTimeFormat2 = formatterForTime.string(from: date.addingTimeInterval(15 * 60) as Date )
     //   let currentTIme2 = formatterForTime.date(from: currentTimeFormat2)!
        let pass = "\(passData!.visitor!.validityStart!) \(passData!.visitor!.startTime!)"
        let end = "\(passData!.visitor!.validityEnd!) \(passData!.visitor!.endTime!)"
        formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let passDate = formatterForTime.date(from: pass)!
        let passendDate = formatterForTime.date(from: end)!
        let currentDateF = formatterForTime.string(from: date as Date)
        let curr = formatterForTime.date(from: currentDateF) // curretn time
        let passPlusGrace = passendDate.addingTimeInterval(15*60) // pass plus 15 grace
        
        if curr! > passDate && curr! < passPlusGrace
        {
            rejectButton.isHidden = false
            inOuOutTimeButton.isHidden = false
            statusLabel.text = "PASS ACTIVE"
            statusLabel.textColor = .green
          
        }
        else
        {
            if curr! < passDate
            {
                rejectButton.isHidden = true
                inOuOutTimeButton.isHidden = true
                statusLabel.text = "PASS NOT ACTIVE"
                statusLabel.textColor = .red
            }
            if curr! > passDate
            {
                rejectButton.isHidden = true
                inOuOutTimeButton.isHidden = true
                statusLabel.text = "PASS EXPIRED"
                statusLabel.textColor = .red
            }
        }
        if passData?.visitor?.inTime != nil && passData?.visitor?.inTime != "00:00:00"
        {
            visitorIn = true

            let pass = "\(passData!.visitor!.validityStart!) \(passData!.visitor!.inTime!)"
            formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let inDate = formatterForTime.date(from: pass)!
            rejectButton.isHidden = true
            inOuOutTimeButton.setTitle("OUT TIME", for: .normal)
            statusLabel.text = "REACHED"
            let defaultTimeZoneStr = formatterForTime.string(from: inDate as Date)
            timeInLabel.text = "TIME IN: \(convertDateFormater(defaultTimeZoneStr))"
            print("outtime")
        }
        if passData?.visitor?.outTime != nil && passData?.visitor?.outTime != "00:00:00"
        {
            visitorIn = true

            let pass = "\(passData!.visitor!.validityStart!) \(passData!.visitor!.inTime!)"
            formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let inDate = formatterForTime.date(from: pass)!
            let defaultTimeZoneStr = formatterForTime.string(from: inDate as Date)
            timeInLabel.text = "TIME IN: \(convertDateFormater(defaultTimeZoneStr))"
            rejectButton.isHidden = true
            inOuOutTimeButton.isHidden = true
            let passo = "\(passData!.visitor!.validityStart!) \(passData!.visitor!.outTime!)"
            formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let outDate = formatterForTime.date(from: passo)!
            let defaultTimeZoneStr2 = formatterForTime.string(from: outDate as Date)
            statusLabel.text = "VISITED"
            timeOutLabel.text = "TIME OUT: \(convertDateFormater(defaultTimeZoneStr2))"
        }
    }
    @IBAction func rejectPassPressed(_ sender: Any) {
        makePostCallReject()
    }
    @IBAction func inTimeOrOutTimePressed(_ sender: Any) {
        if visitorIn
        {
            makePostCallOut()
        }
        else
        {
            makePostCallIn()
        }
        
    }
    func makePostCallReject() {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["in_time":"","status":"REJECTED","visitor_id":"\(passData!.visitor!.visitorID!)","code":0  , "userid" : "\(passData!.visitor!.userid!)" , "subscriber_id" : "\(passData!.visitor!.subscriberID!)" , "user_memberid" : "\(passData!.visitor!.userMemberId!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)update_intime")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(SecurityVisitorPassJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            makeGetCall(visitorId: loginBaseResponse!.visitor!.visitorID!)

                            //self.performSegue(withIdentifier: segueIdentifiers.qrScanToVisitorPass, sender: loginBaseResponse)
                            print(loginBaseResponse as Any)
                            rejectButton.isHidden = true
                            inOuOutTimeButton.isHidden = true
                            statusLabel.text = "REJECTED"
                            statusLabel.textColor = .red
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Security", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostCallIn() {
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        let decoder = JSONDecoder()
        let json: [String: Any] = ["in_time":"\(defaultTimeZoneStr)","status":"REACHED","visitor_id":"\(passData!.visitor!.visitorID!)","code":0 , "userid" : "\(passData!.visitor!.userid!)" , "subscriber_id" : "\(passData!.visitor!.subscriberID!)" , "user_memberid" : "\(passData!.visitor!.userMemberId!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)update_intime")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    let loginBaseResponse = try? decoder.decode(SecurityVisitorPassJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            visitorIn = true
                            //self.performSegue(withIdentifier: segueIdentifiers.qrScanToVisitorPass, sender: loginBaseResponse)
                            print(loginBaseResponse as Any)
                            makeGetCall(visitorId: loginBaseResponse!.visitor!.visitorID!)
                            rejectButton.isHidden = true
                            statusLabel.text = "REACHED"
                            timeInLabel.text = "TIME IN: \(defaultTimeZoneStr)"
                            let formatterForTime = DateFormatter()
                            
                            let pass = "\(loginBaseResponse!.visitor!.validityStart!) \(loginBaseResponse!.visitor!.inTime!)"
                            formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let inDate = formatterForTime.date(from: pass)!
                            statusLabel.text = "REACHED"
                            let defaultTimeZoneStr = formatterForTime.string(from: inDate as Date)
                            timeInLabel.text = "TIME IN: \(convertDateFormater(defaultTimeZoneStr))"
                            inOuOutTimeButton.setTitle("OUT TIME", for: .normal)

                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Security", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makeGetCall(visitorId: String) { // for notification
    let request = NSMutableURLRequest(url: NSURL(string: "https://e-visitor.my/housing_android_api/userapi/push_notification_apps_visitors/\(visitorId)")! as URL)
        request.httpMethod = "GET"
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                return
            }
        
        }
        task.resume()
    }
    func makePostCallOut() {
        let decoder = JSONDecoder()
        let date = NSDate()
        let formatter = DateFormatter()
        formatter.dateFormat = "HH:mm:ss"
        let defaultTimeZoneStr = formatter.string(from: date as Date)
        let json: [String: Any] = ["out_time":"\(defaultTimeZoneStr)","status":"VISITED","visitor_id":"\(passData!.visitor!.visitorID!)","code":0  , "userid" : "\(passData!.visitor!.userid!)" , "subscriber_id" : "\(passData!.visitor!.subscriberID!)" , "user_memberid" : "\(passData!.visitor!.userMemberId!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)update_outtime")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(SecurityVisitorPassJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            
                            //self.performSegue(withIdentifier: segueIdentifiers.qrScanToVisitorPass, sender: loginBaseResponse)
                            print(loginBaseResponse as Any)
                            rejectButton.isHidden = true
                            inOuOutTimeButton.isHidden = true
                            statusLabel.text = "VISITED"
                            timeOutLabel.text = "TIME OUT: \(defaultTimeZoneStr)"
                            makeGetCall(visitorId: loginBaseResponse!.visitor!.visitorID!)

                            let formatterForTime = DateFormatter()
                            let pass = "\(loginBaseResponse!.visitor!.validityStart!) \(loginBaseResponse!.visitor!.inTime!)"
                            formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let inDate = formatterForTime.date(from: pass)!
                            let defaultTimeZoneStr = formatterForTime.string(from: inDate as Date)
                            timeInLabel.text = "TIME IN: \(convertDateFormater(defaultTimeZoneStr))"
                            let passo = "\(loginBaseResponse!.visitor!.validityStart!) \(loginBaseResponse!.visitor!.outTime!)"
                            formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
                            let outDate = formatterForTime.date(from: passo)!
                            let defaultTimeZoneStr2 = formatterForTime.string(from: outDate as Date)
                            statusLabel.text = "VISITED"
                            timeOutLabel.text = "TIME OUT: \(convertDateFormater(defaultTimeZoneStr2))"
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Security", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            return  dateFormatter.string(from: date!)

        }
}
