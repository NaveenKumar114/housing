//
//  ViewController.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-08.
//

import UIKit
import Messages
import Firebase
import FirebaseAuth
class ViewController: UIViewController, UITextFieldDelegate {
    var timerJob : Timer?
    var timerCountJob = 0
    var timerBuild : Timer?
    var timerCountBuild = 0
    var tokenID = ""

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    let child = SpinnerViewController()

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordView: UIView!
    @IBOutlet weak var emailView: UIView!
    @IBOutlet weak var folloLabel: UILabel!
    @IBOutlet weak var signUpButton: UIButton!
    @IBOutlet weak var livingLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        Messaging.messaging().token { token, error in
          if let error = error {
            print("Error fetching FCM registration token: \(error)")
          } else if let token = token {
            print("FCM registration token: \(token)")
            self.tokenID = token
          //  self.fcmRegTokenMessage.text  = "Remote FCM registration token: \(token)"
          }
        }

        emailTextField.delegate = self
        passwordTextField.delegate = self
        
        emailTextField.font = UIFont(name: "Arial-Rounded", size: 15.0)
        passwordTextField.font = UIFont(name: "Arial-Rounded", size: 15.0)

     /*   loginButton.layer.cornerRadius = 25
        loginButton.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        loginButton.layer.borderWidth = 2
        
        signUpButton.layer.cornerRadius = 25
        signUpButton.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        signUpButton.layer.borderWidth = 2
    
        emailView.layer.cornerRadius = 25
        emailView.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        emailView.layer.borderWidth = 2
        
        passwordView.layer.cornerRadius = 25
        passwordView.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        passwordView.layer.borderWidth = 2 */
        passwordTextField.isSecureTextEntry = true
        self.navigationController?.navigationBar.isHidden = true

    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }

    @IBAction func showPasswordSwitch(_ sender: UISwitch) {
        passwordTextField.isSecureTextEntry = sender.isOn ? false : true
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if emailTextField.text == "" || passwordTextField.text == ""
        {
            let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            makePostCall(email: emailTextField.text!, password: passwordTextField.text!)
            createSpinnerView()

        }
    }
 
    
    func makePostCall(email : String , password : String) {
        print(email , password)
        let decoder = JSONDecoder()
        let json: [String: Any] = ["email": "\(email)",
                                   "password": "\(password)" , "tokenid_ios" : "\(tokenID)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        print(json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)ios_userlogin_post")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(LoginJson.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    self.removeSpinnerView()

                    if code_str == 200 {
                        
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        //print(loginBaseResponse as Any)
                        let defaults = UserDefaults.standard
                        defaults.setValue("true", forKey: "isLoggedIn")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberIdentificationType!, forKey: "identType")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberIndentificationNo!, forKey: "identNumber")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberType!, forKey: "memberRoleText")

                        defaults.setValue(loginBaseResponse?.userLogindetails?.subscriberID!, forKey: "subID")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.userid!, forKey: "userID")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberName!, forKey: "name")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberEmail!, forKey: "email")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberHousingname!, forKey: "housingName")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberContact!, forKey: "phoneNo")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.memberRole, forKey: "memberRole")
                        defaults.setValue(loginBaseResponse?.userLogindetails?.userMemberid, forKey: "userMemberId")
                       
                        if loginBaseResponse?.userLogindetails?.profileImg != nil
                        {
                            defaults.setValue(loginBaseResponse?.userLogindetails?.profileImg, forKey: "img")

                        }
                        if loginBaseResponse?.subscriberDetails?.terms != nil
                        {
                            defaults.setValue(loginBaseResponse?.subscriberDetails?.terms, forKey: "\(userDefaultsKey.termsAndCondition)")

                        }
                        if loginBaseResponse?.subscriberDetails?.privacy != nil
                        {
                            defaults.setValue(loginBaseResponse?.subscriberDetails?.privacy, forKey: "\(userDefaultsKey.privacyPolicy)")

                        }
                        defaults.setValue(loginBaseResponse?.subscriberDetails?.email, forKey: "\(userDefaultsKey.housingEmail)")
                        defaults.setValue(loginBaseResponse?.subscriberDetails?.address, forKey: "\(userDefaultsKey.housinAddress)")
                        defaults.setValue(loginBaseResponse?.subscriberDetails?.mobile, forKey: "\(userDefaultsKey.housingPhone)")
                        defaults.setValue(loginBaseResponse?.subscriberDetails?.images, forKey: "\(userDefaultsKey.housingImage)")
                        defaults.setValue(loginBaseResponse?.subscriberDetails?.identificationType, forKey: "\(userDefaultsKey.housingIdentType)")
                        defaults.setValue(loginBaseResponse?.subscriberDetails?.identificationNo, forKey: "\(userDefaultsKey.housingIdentNUmber)")
                      /*  defaults.setValue((loginBaseResponse?.userLogindetails.id)!, forKey: "id")
                        defaults.setValue((loginBaseResponse?.logindetails?.firstName)!, forKey: "name")
                        if let str = loginBaseResponse?.logindetails?.profileImg
                        {
                            defaults.setValue(str, forKey: "img")

                        } */
                        //defaults.setValue((loginBaseResponse?.logindetails?.lastName)!, forKey: "id")


                        let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

                        let dashboard = storyboard2.instantiateViewController(identifier: "dash")
                        (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "\(loginBaseResponse?.response ?? "Invalid Email & Password")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }

}

