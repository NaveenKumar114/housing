//
//  EBilling.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-06.
//

import UIKit
import Alamofire

class EBilling: UIViewController, eBillAttachmentDelegate {
    func imageUpdated() {
        if addressId != nil
        {
            makePostCallEbilling(addressID: addressId!)
        }
        

    }
    
    let child = SpinnerViewController()
    var details : BillingDetailsList?
    @IBOutlet weak var totalDueAmount: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var ebillingListTableView: UITableView!
    var addressList : AddressJSON?
    var ebillingData : EbillingListJSON?
    var totalAmount = 0.0
    var addressId : String?
    var profileImg : UIImage?

    override func viewDidLoad() {
        super.viewDidLoad()
      
        addressLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)

        let gesture = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressLabel.addGestureRecognizer(gesture)
        makePostCallAddress()
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            addressLabel.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            addressId = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            makePostCallEbilling(addressID: addressId!)
            createSpinnerView()

        }
        ebillingListTableView.delegate = self
        ebillingListTableView.dataSource = self
        ebillingListTableView.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        ebillingListTableView.register(UINib(nibName: reuseNibIdentifier.EbillingListNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.EbillingListCellIdentifier)
        self.ebillingListTableView.tableFooterView = UIView()

    }
    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                }
            }
            multipart.append(image, withName: "profile_image", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(EbillingListJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                print("success")
                                
                                let alert = UIAlertController(title: "Update Profile", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                                    self.makePostCallEbilling(addressID: self.addressId!)
                                   // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                                    print("Success")

                                } ))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Update Profile", message: "Unable To Update", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressLabel.text = UIAlertAction.title
                        print(n)
                        addressId = listOfAllAddress[n].addressID!
                        makePostCallEbilling(addressID: listOfAllAddress[n].addressID!)
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }

    func makePostCallEbilling(addressID : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID)" , "status" : "Pending"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_billing_details")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(EbillingListJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                               print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        //print(loginBaseResponse as Any)
                        self.totalAmount = 0
                        self.ebillingData = loginBaseResponse
                        self.ebillingListTableView.reloadData()
                        self.removeSpinnerView()
                        if loginBaseResponse?.billingDetailsList?.count == 0
                          {
                             let alert = UIAlertController(title: "Ebilling", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                             alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                             self.present(alert, animated: true, completion: nil)
                            }
                        else
                        {
                            totalAmount = 0
                            for n in 0 ... (loginBaseResponse?.billingDetailsList!.count)! - 1
                            {
                                 if let safeData = loginBaseResponse?.billingDetailsList?[n]
                                 {
                                  
                                    var a = Double(safeData.pendingAmount ?? "0")
                                    if safeData.facilityID != "0"
                                    {
                                        a = Double(safeData.advanceAmount ?? "0")
                                    }
                                    totalAmount = totalAmount + (a ?? 0)
                                    totalDueAmount.text = "\(String(format: "%.2f", totalAmount)) MYR"
                                 }
                            }
                        }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    }


}

extension EBilling: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = ebillingListTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.EbillingListCellIdentifier) as! EbillingListCell
        if let safeData = ebillingData?.billingDetailsList?[indexPath.row]
        {
            cell.status.text = safeData.status?.uppercased()
            cell.date.text = convertDateFormater(safeData.dueDate!)
            
            //cell.amount.text = "\(safeData.pendingAmount ?? "0") MYR"
            let number = Double(safeData.pendingAmount ?? "0")
            
            cell.amount.text = "\(String(format: "%.2f", number ?? "0")) MYR"
            cell.billName.text = safeData.billingName
            cell.billFrom.text = convertDateFormater(safeData.billingFrom!)
            cell.billTo.text = convertDateFormater(safeData.billingTo!)
            let number2 = Double(safeData.penaltyAmount ?? "0")

            cell.afterDue.text = "After Due Date Charge : \(String(format: "%.2f", number2 ?? "0")) MYR"
            if safeData.facilityID != "0"
            {
                let number = Double(safeData.advanceAmount ?? "0")
                
                cell.amount.text = "\(String(format: "%.2f", number ?? "0")) MYR"
                cell.billName.text = safeData.billingName
                cell.billFrom.text = convertDateFormater(safeData.billingFrom!)
                cell.billTo.text = convertDateFormater(safeData.billingTo!)
                let number2 = Double(safeData.bookingAmount ?? "0")

                cell.afterDue.text = "After Due Date Charge : \(String(format: "%.2f", number2 ?? "0")) MYR"
                cell.date.text = convertDateFormater(safeData.billingTo!)
            }
            if safeData.status == "PENDING" || safeData.status == "Pending"
            {
                cell.status.textColor = .red
            }
            if safeData.attachmentImage != nil
            {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/billing/attachments/\(safeData.attachmentImage!)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                    if   let image = UIImage(data: data!)
                    {
                      
                        cell.billAttachment.setImage(image, for: .normal)
                        cell.billAttachment.layer.cornerRadius = 20
                        cell.billAttachment.clipsToBounds = true

                    }
                    }
                }
            }
            //let a = Double(safeData.pendingAmount ?? "0")
            //totalAmount = totalAmount + (a ?? 0)
           // totalDueAmount.text = "\(String(format: "%.2f", totalAmount)) MYR"
            cell.billAttachment.addTarget(self, action: #selector(attachmentImage), for: .touchUpInside)
            cell.billAttachment.tag = indexPath.row
        }
        return cell
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
        if date != nil
        {
            return  dateFormatter.string(from: date!)
        }
        else
        {
        return "\(date)"
        }
        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 170
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ebillingData?.billingDetailsList?.count ?? 0
    }
    
    @objc func attachmentImage(sender : UIButton)
    {
        let x = sender.tag
        let data = ebillingData?.billingDetailsList![x]
        //performSegue(withIdentifier: segueIdentifiers.ebillToAttachment, sender: x)
        details = data
        toImage()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.ebillToAttachment
        {
            let x = sender as! Int
            let i = IndexPath(row: x, section: 0)
            let data = ebillingData?.billingDetailsList![x] as! BillingDetailsList
            let dv = segue.destination as! EbillingAttachment
            dv.details = data
            dv.delegate = self
            let cell = ebillingListTableView.cellForRow(at: i) as! EbillingListCell
            if let img = cell.billAttachment.image(for: .normal)
            {
                dv.attachImage = img
                print("ss")
            }
            
            
        }
    }
    
}


extension EBilling : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                let imageData = image.jpegData(compressionQuality: 0.50)
    //    let id = UserDefaults.standard.string(forKey: "id")!
        let id = details?.billingID
        let json: [String: Any] = ["billing_id" : "\(id!)"]
        profileImg = image
      
        uploadImage(image: imageData!, to: URL(string: "http://e-visitor.my/housing_android_api/Imagehelper/users_billing_attachment")!, params: json)

        
    }
}
