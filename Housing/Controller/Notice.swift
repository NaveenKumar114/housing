//
//  Notice.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-18.
//

import UIKit

class Notice: UIViewController {
    let child = SpinnerViewController()

    @IBOutlet weak var announcementTableView: UITableView!
    var announcementData : AnnouncementJSON?
    var single : Int?
    var singleData : AnnouncementList?
    var singleImage : UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        if Dashboard.announcementStaticData != nil
        {
            announcementData = Dashboard.announcementStaticData
        }
        else
        {
            makePostCallAnnouncement()
            createSpinnerView()
        }
        announcementTableView.delegate = self
        announcementTableView.dataSource = self
        announcementTableView.register(UINib(nibName: reuseNibIdentifier.noticeNibIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.noticeCellIdentifier)
        self.announcementTableView.tableFooterView = UIView()
        if single != nil
        {

            print("notnil")
            //performSegue(withIdentifier: segueIdentifiers.noticeToSingle, sender: singleImage!)
            let storyboard2 = UIStoryboard(name: "Notice", bundle: nil)

            let secondViewController = storyboard2.instantiateViewController(identifier: "singleNotice") as! SingleAnnouncement
            secondViewController.aImage = singleImage
            secondViewController.singleData = singleData
            self.navigationController?.pushViewController(secondViewController, animated: false)
        
        }


    }
    func createSpinnerView() {
         addChild(child)
         child.view.frame = view.frame
         view.addSubview(child.view)
         child.didMove(toParent: self)
     }
     func removeSpinnerView()
     {
         child.willMove(toParent: nil)
         child.view.removeFromSuperview()
         child.removeFromParent()
     }

    func makePostCallAnnouncement() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        let subID = defaults.string(forKey: "subID")
        let userID = defaults.string(forKey: "userID")
        let json: [String: Any] = ["subscriber_id" : "\(subID ?? "")" , "userid" : "\(userID ?? "")"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_announcementlist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AnnouncementJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.announcementData = loginBaseResponse
                        self.announcementTableView.reloadData()
                        self.removeSpinnerView()
                                if loginBaseResponse?.announcementList?.count == 0
                                 {
                                    let alert = UIAlertController(title: "Announcement", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    self.present(alert, animated: true, completion: nil)
                                   }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Announcement", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    
}

extension Notice : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return announcementData?.announcementList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = announcementTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.noticeCellIdentifier) as! NoticeCell
        if let safeData = announcementData?.announcementList?[indexPath.row]
        {
            cell.dateLabel.text = convertDateFormater(safeData.startDate!)
            cell.headingLabel.text = safeData.heading ?? ""
            cell.detailLabel.text = safeData.announcementListDescription ?? ""
            if let img = safeData.image
            {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)announce/\(img)")
                //print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!)
                    DispatchQueue.main.async { [self] in
                        cell.announcementImage.image = UIImage(data: data!)
                    }
                }
            }
        }
        return cell
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 250
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = announcementTableView.cellForRow(at: indexPath) as! NoticeCell
        let img = cell.announcementImage.image
        performSegue(withIdentifier: segueIdentifiers.noticeToSingle, sender: indexPath)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            print("prepare")
        let indexPath = sender as! IndexPath

         
            let destinationVC = segue.destination as! SingleAnnouncement
        
        let cell = announcementTableView.cellForRow(at: indexPath) as! NoticeCell
        let img = cell.announcementImage.image
        destinationVC.aImage = img
        destinationVC.singleData = announcementData?.announcementList?[indexPath.row]
    }
}
