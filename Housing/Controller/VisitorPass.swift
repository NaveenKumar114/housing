//
//  VisitorPass.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-19.
//

import UIKit
import SnapshotKit
import WebKit
class VisitorPass: UIViewController {
    @IBOutlet weak var viewForScreenShot: UIView!
    @IBOutlet weak var viewContainingWebview: UIView!
    @IBOutlet weak var viewForBlur: UIView!
    @IBOutlet weak var passWebView: WKWebView!
        @IBOutlet var viewForClose: UIView!
    var visitorHistoryData : VisitorList?
    var newVisitorData : Visitor?
    override func viewDidLoad() {
        super.viewDidLoad()
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = viewForBlur.frame
        viewForBlur.addSubview(blurEffectView)
        if let safeData = visitorHistoryData
        {
           
            /*if let img = safeData.qrImage
            {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/visitors/qr_image/\(img)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                        if let x = data
                        {
                            let image = UIImage(data: x)
                            qrCode.image = image
                        }
                       // logoImageVIew.image = UIImage(data: data!)
                      //  logoImageVIew.contentMode = .scaleAspectFit
                        
                    }
                }
            } */
            let housingName = safeData.housingName!.uppercased()
            let passType = safeData.visitorType!.uppercased()
            let imgUrl = "\(ConstantsUsedInProject.baseImgUrl)users/visitors/qr_image/\(safeData.qrImage!)"
            let address = safeData.address!.uppercased()
            let startDate = "\(convertDateFormater(safeData.validityStart!)) \(safeData.startTime!)"
            let endDate = "\(convertDateFormater(safeData.validityEnd!)) \(safeData.endTime!)"
            passWebView.loadHTMLString("<div class=\"container-fluid printhide\" style=\"border:2px solid #315b69;width:100%;\">\n" +
                                        "        <div class=\"row\">\n" +
                                        "            <div class=\"\">\n" +
                                        "                <div class=\"\">\n" +
                                        "                    <div class=\"\">\n" +
                                        "                        <div class=\"\">\n" +
                                        "                            <h1 class=\"font-weight-bold text-center \" style=\"text-align:center;color:#315b69;font-size:100px\">\n" +
                                        "                                " + "\(housingName)" + "\n" +
                                        "                            </h1>\n" +
                                        "                            <h6 class=\"font-weight-bold text-center \" style=\"text-align:center;color:#315b69;font-size:45px\">\n" +
                                        "                                " + "\(passType)" + "\n" +
                                        "                            </h6>\n" +
                                        "                                    <center>\n" +
                                        "                                        <img style=\"height:550px;\" src=" + "\(imgUrl)" + " class=\"\" alt=\"avatar\"> \n" +
                                        "<p style=\"text-align:center;font-size:62px;\">Single Entry</p>\n" +
                                        "                                   <p style=\"text-align:center;color:#315b69;font-size:70px;\">" + "\(address)" + "</p>\n" +
                                        "                                    <p style=\"color:#315b69;font-size:50px;\"> Validity From :" + "\(startDate)" + " <br>\n" +
                                        "                                        Validity To :" + "\(endDate)" + "  <br>\n" +
                                        "                                        [ Grace Period : 15 minutes ]</p></center>\n" +
                                        "                                \n" +
                                        "                        </div>\n" +
                                        "                       \n" +
                                        "                    </div>\n" +
                                        "                </div>\n" +
                                        "            </div>\n" +
                                        "        </div>\n" +
                                        "    </div>", baseURL: nil)
        }
        if let safeData = newVisitorData
        {
           
            let housingName = safeData.housingName!.uppercased()
            let passType = safeData.visitorType!.uppercased()
            let imgUrl = "\(ConstantsUsedInProject.baseImgUrl)users/visitors/qr_image/\(safeData.qrImage!)"
            let address = safeData.address
            let startDate = "\(convertDateFormater(safeData.validityStart!)) \(safeData.startTime!)"
            let endDate = "\(convertDateFormater(safeData.validityEnd!)) \(safeData.endTime!)"
            passWebView.loadHTMLString("<div class=\"container-fluid printhide\" style=\"border:2px solid #315b69;width:100%;\">\n" +
                                        "        <div class=\"row\">\n" +
                                        "            <div class=\"\">\n" +
                                        "                <div class=\"\">\n" +
                                        "                    <div class=\"\">\n" +
                                        "                        <div class=\"\">\n" +
                                        "                            <h1 class=\"font-weight-bold text-center \" style=\"text-align:center;color:#315b69;font-size:100px\">\n" +
                                        "                                " + "\(housingName)" + "\n" +
                                        "                            </h1>\n" +
                                        "                            <h6 class=\"font-weight-bold text-center \" style=\"text-align:center;color:#315b69;font-size:45px\">\n" +
                                        "                                " + "\(passType)" + "\n" +
                                        "                            </h6>\n" +
                                        "                                    <center>\n" +
                                        "                                        <img style=\"height:550px;\" src=" + "\(imgUrl)" + " class=\"\" alt=\"avatar\"> \n" +
                                        "<p style=\"text-align:center;font-size:68px;\">Single Entry</p>\n" +
                                        "                                   <p style=\"text-align:center;color:#315b69;font-size:74px;\">" + "\(address!)" + "</p>\n" +
                                        "                                    <p style=\"color:#315b69;font-size:55px;\"> Validity From :" + "\(startDate)" + " <br>\n" +
                                        "                                        Validity To :" + "\(endDate)" + "  <br>\n" +
                                        "                                        [ Grace Period : 15 minutes ]</p></center>\n" +
                                        "                                \n" +
                                        "                        </div>\n" +
                                        "                       \n" +
                                        "                    </div>\n" +
                                        "                </div>\n" +
                                        "            </div>\n" +
                                        "        </div>\n" +
                                        "    </div>", baseURL: nil)
        }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        viewForClose.addGestureRecognizer(gesture)
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    @objc func dismissVC()
    {
        self.dismiss(animated: true, completion: nil)

    }
    @IBAction func shareButtonPressed(_ sender: Any) {
        self.passWebView.scrollView.asyncTakeSnapshotOfFullContent { (image) in
            self.viewForBlur.asyncTakeSnapshotOfFullContent { [self] (back) in
                self.combineImage(bottomImage: back! , topImage: image! , width: passWebView.frame.width , height: passWebView.frame.height)
                
            
            }
        }

            
    }
    func combineImage (bottomImage : UIImage , topImage : UIImage , width : CGFloat , height : CGFloat)
    {
        
           let newSize = CGSize(width: width, height: height)
        let newSize2 = CGSize(width: width + 80, height: height + 100)

           UIGraphicsBeginImageContextWithOptions(newSize2, false, 0.0)
           bottomImage.draw(in: CGRect(origin: CGPoint.zero, size: newSize2))
        let x : CGPoint = CGPoint(x: 40, y: 50)
           topImage.draw(in: CGRect(origin: x, size: newSize))
            
           let newImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
        let imageToShare = [ newImage! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}


