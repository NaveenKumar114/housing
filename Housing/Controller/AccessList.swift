//
//  AccessList.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-22.
//

import UIKit

class AccessList: UIViewController {
    var accessListData : AccessJSON?
    var spinner = UIActivityIndicatorView(style: .whiteLarge)
    let child = SpinnerViewController()

    @IBOutlet weak var accessListTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Access"

        accessListTable.delegate = self
        accessListTable.dataSource = self
        accessListTable.register(UINib(nibName: reuseNibIdentifier.accessNibIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.accessCellIdentifier)
        makePostCall()
        accessListTable.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        createSpinnerView()
        self.accessListTable.tableFooterView = UIView()

    }
    func createSpinnerView() {
        //let child = SpinnerViewController()

        // add the spinner view controller
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "\(userDefaultsKey.userMemberID)")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        print(json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_accesslist")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AccessJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
          print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        //print(loginBaseResponse as Any)
                        accessListData = loginBaseResponse
                        accessListTable.reloadData()
                        removeSpinnerView()
                        if loginBaseResponse?.accessList?.count == 0
                        {
                            let alert = UIAlertController(title: "Access", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                        }
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Access", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        
        task.resume()
    }
    }

}

extension AccessList : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return accessListData?.accessList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = accessListTable.dequeueReusableCell(withIdentifier: reuseCellIdentifier.accessCellIdentifier) as! AccessListCell
        cell.accessName.text = accessListData?.accessList?[indexPath.row].accessName ?? "error"
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        openAlert(position: indexPath.row)
        accessListTable.deselectRow(at: indexPath, animated: true)
    }
    @objc func openAlert(position : Int)
    {
        let storyboard = UIStoryboard(name: "Access", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "accessSingle") as! AccessListQR
        myAlert.qrData = accessListData?.accessList?[position]
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
}
