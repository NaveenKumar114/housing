//
//  AccessListQR.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-22.
//

import UIKit
import WebKit
class AccessListQR: UIViewController {
    @IBOutlet weak var viewForBlur: UIView!
    @IBOutlet weak var borderView: UIView!
    var qrData : AccessLists?
    @IBOutlet var viewForDismissing: UIView!
    @IBOutlet weak var mainVIew: UIView!
    
    @IBOutlet weak var name: UILabel!
    
    
    @IBOutlet weak var passType: UILabel!
    @IBOutlet weak var qrCodeExpireLabel: UILabel!
    @IBOutlet weak var houseName: UILabel!
    @IBOutlet weak var qrImageView1: UIImageView!
    @IBOutlet weak var qrImageView3: UIImageView!
    @IBOutlet weak var qrImageView4: UIImageView!
    @IBOutlet weak var qrImageView2: UIImageView!
    @IBOutlet weak var qrImageView5: UIImageView!
    var collectionOfQrImage = [UIImageView]()
    override func viewDidLoad() {
        super.viewDidLoad()
        borderView.layer.borderWidth = 1
        borderView.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
        borderView.layer.cornerRadius = 5
        collectionOfQrImage.append(qrImageView1)
        collectionOfQrImage.append(qrImageView2)
        collectionOfQrImage.append(qrImageView3)
        collectionOfQrImage.append(qrImageView4)
        collectionOfQrImage.append(qrImageView5)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        viewForDismissing.addGestureRecognizer(gesture)
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = view.frame
        viewForBlur.addSubview(blurEffectView)
        if let safeData = qrData
        {
            houseName.text = UserDefaults.standard.string(forKey: userDefaultsKey.housingName)?.uppercased()
            passType.text = safeData.accessName?.uppercased()
            name.text = safeData.userName?.uppercased()
            qrCodeExpireLabel.text = "QR CODE EXPIRES BY \n \(convertDateFormater(safeData.endDate!)) \(convertTimeFormater(safeData.endTime!))".uppercased()
            if let img = safeData.qrImage
            {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)access/qr_image/\(img)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                        if let x = data
                        {
                            let image = UIImage(data: x)
                            for view in collectionOfQrImage
                            {
                                view.image = image
                            }
                        }
                        // logoImageVIew.image = UIImage(data: data!)
                        //  logoImageVIew.contentMode = .scaleAspectFit
                        
                    }
                }
            }
            
        }
    }
    @objc func dismissVC()
    {
        self.dismiss(animated: true, completion: nil)
        
    }
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MMM-yyyy"
        return  dateFormatter.string(from: date!)
        
    }
    func convertTimeFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "HH-mm-ss"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "hh:mm a"
        return  dateFormatter.string(from: date!)
        
    }
    
}
