//
//  SingleAnnouncement.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-18.
//

import UIKit

class SingleAnnouncement: UIViewController {
    var aImage : UIImage?
    var singleData : AnnouncementList?

    @IBOutlet weak var imageHeight: NSLayoutConstraint!

    @IBOutlet weak var heading: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var announceImage: UIImageView!
    @IBOutlet weak var detail: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        if aImage != nil
        {
            announceImage.image = aImage
            let ratio = aImage!.size.width / aImage!.size.height
            let newHeight = announceImage.frame.width / ratio
            imageHeight.constant = newHeight
            view.layoutIfNeeded()

        }
        if let img = singleData?.image
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)announce/\(img)")
            //print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!)
                DispatchQueue.main.async { [self] in
                    if data != nil
                    {
                    announceImage.image = UIImage(data: data!)
                    }
                }
            }
        }
        
        //print(singleData)
        date.text = convertDateFormater(singleData!.startDate!)
        heading.text = singleData?.heading
        detail.text = singleData?.announcementListDescription
    }
    
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }

  

}

