//
//  Dashboard.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-14.
//

import UIKit
import ImageSlideshow
import Alamofire
import FirebaseInstanceID
import Firebase
class Dashboard: UIViewController, ImageSlideshowDelegate {
    @IBOutlet weak var securityStack: UIStackView!
    @IBOutlet weak var userStack: UIStackView!
    var addressData : AddressJSON?
    var announcementData : AnnouncementJSON?
    static var announcementStaticData : AnnouncementJSON?
    @IBOutlet weak var announcementCollectionView: UICollectionView!
    
    @IBOutlet weak var welcomeLabel: UILabel!
    @IBOutlet weak var announcementTextLabel: UILabel!
    @IBOutlet weak var viewContainingAnnouncement: UIView!
    
    @IBOutlet weak var slideShow: ImageSlideshow!
    @IBOutlet weak var dashboardCollection: UICollectionView!
    @IBOutlet weak var userVisitor: UIView!
    @IBOutlet weak var userAccess: UIView!
    @IBOutlet weak var userServices: UIView!
    @IBOutlet weak var userUSeful: UIView!
    @IBOutlet weak var userUsers: UIView!
    @IBOutlet weak var userFacilities: UIView!
    @IBOutlet weak var userFaq: UIView!
    @IBOutlet weak var userIntercom: UIView!
    @IBOutlet weak var userAddress: UIView!
    @IBOutlet weak var userVisitorRecord: UIView!
    @IBOutlet weak var userEbilling: UIView!
    @IBOutlet weak var userStatemnetOfAccount: UIView!
    var announcementImages = [UIImage]()
    var heading = [String]()
    var anoouncementId = [Int]()
    var isSecurity  = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDashBoardUserSegues()
                          
        announcementCollectionView.isHidden = true
        let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(didTapAnnouncement))
          slideShow.addGestureRecognizer(gestureRecognizer)
        let defaults = UserDefaults.standard
        let name = defaults.string(forKey: "name")
        welcomeLabel.text = "Welcome \(name ?? "")"
        //self.navigationItem.title = "Home"
        dashboardCollection.register(UINib(nibName: reuseCollectionNibIdentifier.dashboardNibIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCollectionIdentifier.dashboardCellIdentfier)
        dashboardCollection.dataSource = self
        dashboardCollection.delegate = self
        
        announcementCollectionView.dataSource = self
        announcementCollectionView.delegate = self
        announcementCollectionView.register(UINib(nibName: reuseCollectionNibIdentifier.announcemnetNibIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCollectionIdentifier.announcemnetCellIdentifier)
        makePostCallAnnouncement()
        slideShow.delegate = self
        slideShow.slideshowInterval = 4
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) == nil
        {
            print("No Address")
            makePostCallAddress()

        }
        else
        {
            print("Address")

        }
        if let memberRole =  UserDefaults.standard.string(forKey: userDefaultsKey.memberRole)
        {
            if memberRole == "4"
            {
                userStack.isHidden = true
                securityStack.isHidden = false
                print(memberRole)
                isSecurity = true
            }
            else
            {
                print(memberRole)
                print(memberRole)
                userStack.isHidden = false
                securityStack.isHidden = true
            }
        }
        else
        {
            let alert = UIAlertController(title: "Dashboard", message: "Error retrieving memeber Role", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        
    }

   @objc func didTapAnnouncement() {
        let id = slideShow.currentPage
    let img = announcementImages[id]

    let storyboard2 = UIStoryboard(name: "Notice", bundle: nil)

    let secondViewController = storyboard2.instantiateViewController(identifier: "NoticeStory") as! Notice
    secondViewController.single = id
    secondViewController.singleImage = img
    let data = announcementData?.announcementList?[anoouncementId[id]]
    secondViewController.singleData = data
    self.navigationController?.pushViewController(secondViewController, animated: false)
    }
    func setupDashBoardUserSegues()
    {
        let gestureVisitor = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userVisitor.addGestureRecognizer(gestureVisitor)
        let gestureAccess = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userAccess.addGestureRecognizer(gestureAccess)
        let gestureService = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userServices.addGestureRecognizer(gestureService)
        let gestureUseful = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userUSeful.addGestureRecognizer(gestureUseful)
        let gestureUser = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userUsers.addGestureRecognizer(gestureUser)
        let gestureFacility = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userFacilities.addGestureRecognizer(gestureFacility)
        let gestureFaq = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userFaq.addGestureRecognizer(gestureFaq)
        let gestureIntercom = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userIntercom.addGestureRecognizer(gestureIntercom)
        let gestureAddress = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userAddress.addGestureRecognizer(gestureAddress)
        let gestureRecord = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userVisitorRecord.addGestureRecognizer(gestureRecord)
        let gestureEilling = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userEbilling.addGestureRecognizer(gestureEilling)
        let gestureStatement = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        userStatemnetOfAccount.addGestureRecognizer(gestureStatement)
        
    }
    @objc func userViewsSegue(_ sender :  UIGestureRecognizer)
    {
        switch sender.view {
        case userVisitor:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.visitor, sender: nil)
        case userAccess:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.access, sender: nil)
        case userServices:
           // performSegue(withIdentifier: dashBoardUserSegueIdentifiers.service, sender: nil)
            let newViewController = NewServices()
            self.navigationController?.pushViewController(newViewController, animated: true)
        case userUSeful:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.useful, sender: nil)
        case userUsers:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.users, sender: nil)
        case userFacilities:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.facility, sender: nil)
        case userFaq:
           // performSegue(withIdentifier: dashBoardUserSegueIdentifiers.faq, sender: nil)
            let newViewController = NewFAQ()
            self.navigationController?.pushViewController(newViewController, animated: true)
        case userIntercom:
            openAlert()
        case userAddress:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.address, sender: nil)
        case userVisitorRecord:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.visitroRecord, sender: nil)
        case userStatemnetOfAccount:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.statement, sender: nil)
        case userEbilling:
            performSegue(withIdentifier: dashBoardUserSegueIdentifiers.ebilling, sender: nil)
        
        default:
            print("other")
        }
        
    }
    @IBAction func facilityScan(_ sender: Any) {
        self.performSegue(withIdentifier: segueIdentifiers.dashToSecurityScan, sender: securityScanType.facility)

    }
    @IBAction func visitorScan(_ sender: Any) {
        self.performSegue(withIdentifier: segueIdentifiers.dashToSecurityScan, sender: securityScanType.visitor)

    }
    
    @IBAction func accessScan(_ sender: Any) {
        self.performSegue(withIdentifier: segueIdentifiers.dashToSecurityScan, sender: securityScanType.access)

    }
 
    @IBAction func intercomPressed(_ sender: Any) {
        openAlert()
        print("intercom")
    }
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        if let safeData = loginBaseResponse?.addressList
                        {
                            if safeData.count > 1
                            {
                                print(safeData[0])
                                
                                UserDefaults.standard.setValue("\(safeData[0].houseNo ?? ""),  \(safeData[0].address ?? "")", forKey: "address")
                                UserDefaults.standard.setValue(safeData[0].addressID, forKey: "addressID")
                                NotificationCenter.default.post(name: Notification.Name("navgation"), object: nil)

                                self.addressData = loginBaseResponse
                                self.performSegue(withIdentifier: segueIdentifiers.dashToAddress, sender: nil)
                            }
                            else
                            if safeData.count == 1
                            {
                                print(safeData[0])

                                UserDefaults.standard.setValue("\(safeData[0].houseNo ?? ""),  \(safeData[0].address ?? "")", forKey: "address")
                                UserDefaults.standard.setValue(safeData[0].addressID, forKey: "addressID")
                                NotificationCenter.default.post(name: Notification.Name("navgation"), object: nil)

                            }
                        }
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        //print(loginBaseResponse as Any)
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @objc func openAlert()
    {
        let storyboard = UIStoryboard(name: "Intercom", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "intercom")
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
        //  performSegue(withIdentifier: "qr", sender: nil)
        //present(alert() , animated: true)
    }
    func makePostCallAnnouncement() {
        let decoder = JSONDecoder()
        let defaults = UserDefaults.standard
        let subID = defaults.string(forKey: "subID")
        let userID = defaults.string(forKey: "userID")
        let json: [String: Any] = ["subscriber_id" : "\(subID ?? "")" , "userid" : "\(userID ?? "")"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_announcementlist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                let loginBaseResponse = try? decoder.decode(AnnouncementJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.announcementData = loginBaseResponse
                        Dashboard.announcementStaticData = loginBaseResponse
                        self.announcementCollectionView.reloadData()
                        if loginBaseResponse?.announcementList?.count == 0
                        {
                            self.announcementCollectionView.isHidden = true
                        }
                        else
                        {
                            for n in 0 ... (loginBaseResponse?.announcementList!.count)! - 1
                            {
                                if let img = announcementData?.announcementList?[n].image
                                {
                                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)announce/\(img)")
                                    //print(urlStr)
                                    let url = URL(string: urlStr)
                                    
                                    DispatchQueue.global().async {
                                        let data = try? Data(contentsOf: url!)
                                        DispatchQueue.main.async { [self] in
                                            if let d = data
                                            {
                                                self.announcementImages.append(UIImage(data: d)!)
                                                self.reloadAnnouncementImage()
                                                self.anoouncementId.append(n)
                                                self.heading.append(loginBaseResponse?.announcementList![n].heading ?? "")
                                                if announcementImages.count == 1
                                                {
                                                    print(loginBaseResponse?.announcementList![n].heading ?? "")
                                                    announcementTextLabel.text = loginBaseResponse?.announcementList![n].heading ?? ""
                                                }
                                                

                                            }
                                        }
                                    }
                                }
                            }
                        }
                                                
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Announcement", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }

}

extension Dashboard : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == announcementCollectionView
        {
        var count = 0
        if let data = announcementData
        {
            if let safe = data.announcementList
            {
                count = safe.count 
            }
        }
        return count
        }
        else
        {
            if isSecurity {
                return 8

            }
            else
            {
                return 12

            }
        }
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == announcementCollectionView
        {
            let cell = announcementCollectionView.cellForItem(at: indexPath) as! AnnouncementCollectionCell
            let img = cell.announceImage.image
//            performSegue(withIdentifier: segueIdentifiers.dashToSingle, sender: img)
            print("select")

            //performSegue(withIdentifier: segueIdentifiers.dashToNotice, sender: indexPath)

           // performSegue(withIdentifier: segueIdentifiers.noticeToSingle, sender: img)
            let storyboard2 = UIStoryboard(name: "Notice", bundle: nil)

            let secondViewController = storyboard2.instantiateViewController(identifier: "NoticeStory") as! Notice
            secondViewController.single = indexPath.row
            secondViewController.singleImage = img
         let data = announcementData?.announcementList?[indexPath.row]
            secondViewController.singleData = data
            self.navigationController?.pushViewController(secondViewController, animated: false)
        }
        else
        {
            if isSecurity
            {
                for n in 0 ... 7
                {
                    let cell = dashboardCollection.cellForItem(at: IndexPath(row: n, section: 0)) as! DashboardCell
                    cell.iconImage.tintColor = .darkGray
                    cell.titleLabel.textColor = .darkGray
                    var rect = cell.iconImage.frame
                    rect.size.height = 20
                   // cell.iconImage.frame = rect
                    cell.iconImage.isHidden = false
                    cell.iconImage1.isHidden = true
                }
                let cell = dashboardCollection.cellForItem(at: indexPath) as! DashboardCell
                cell.iconImage.tintColor = ConstantsUsedInProject.appThemeColor
                cell.titleLabel.textColor = ConstantsUsedInProject.appThemeColor
                var rect = cell.iconImage.frame
                rect.size.height = 30
               // cell.iconImage.frame = rect
                cell.iconImage.isHidden = true
                cell.iconImage1.isHidden = false
                
                switch indexPath.row{
                case 0:
                    self.performSegue(withIdentifier: segueIdentifiers.dashToSecurityScan, sender: securityScanType.visitor)
                case 1:
                    self.performSegue(withIdentifier: segueIdentifiers.dashToSecurityScan, sender: securityScanType.access)
                case 2 :
                    self.performSegue(withIdentifier: segueIdentifiers.dashToSecurityScan, sender: securityScanType.facility)
                case 3:
                    let newViewController = NewServices()
                    self.navigationController?.pushViewController(newViewController, animated: true)
                case 5:
                    performSegue(withIdentifier: dashBoardUserSegueIdentifiers.useful, sender: nil)
                case 6:
                    let newViewController = NewFAQ()
                    self.navigationController?.pushViewController(newViewController, animated: true)
                case 7:
                   // performSegue(withIdentifier: dashBoardUserSegueIdentifiers.faq, sender: nil)
                    openAlert()
                case 4:
                    performSegue(withIdentifier: segueIdentifiers.dashToSecurityRecord, sender: nil)

                default:
                    print("default")
                }
            }
            else
            {
            for n in 0 ... 11
            {
                let cell = dashboardCollection.cellForItem(at: IndexPath(row: n, section: 0)) as! DashboardCell
                cell.iconImage.tintColor = .darkGray
                cell.titleLabel.textColor = .darkGray
                var rect = cell.iconImage.frame
                rect.size.height = 20
               // cell.iconImage.frame = rect
                cell.iconImage.isHidden = false
                cell.iconImage1.isHidden = true
            }
            let cell = dashboardCollection.cellForItem(at: indexPath) as! DashboardCell
            cell.iconImage.tintColor = ConstantsUsedInProject.appThemeColor
            cell.titleLabel.textColor = ConstantsUsedInProject.appThemeColor
            var rect = cell.iconImage.frame
            rect.size.height = 30
           // cell.iconImage.frame = rect
            cell.iconImage.isHidden = true
            cell.iconImage1.isHidden = false
            
            switch indexPath.row{
            case 0:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.visitor, sender: nil)
            case 1:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.access, sender: nil)
            case 2 :
                let newViewController = NewServices()
                self.navigationController?.pushViewController(newViewController, animated: true)
            case 3:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.useful, sender: nil)
            case 4:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.users, sender: nil)
            case 5:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.facility, sender: nil)
            case 6:
               // performSegue(withIdentifier: dashBoardUserSegueIdentifiers.faq, sender: nil)
                let newViewController = NewFAQ()
                self.navigationController?.pushViewController(newViewController, animated: true)
            case 7:
                openAlert()
            case 8:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.address, sender: nil)
            case 9:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.visitroRecord, sender: nil)
            case 11:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.statement, sender: nil)
            case 10:
                performSegue(withIdentifier: dashBoardUserSegueIdentifiers.ebilling, sender: nil)
            default:
                print("default")
            }
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.dashToSingle {
            let img = sender as! UIImage
            let destinationVC = segue.destination as! SingleAnnouncement
            destinationVC.aImage = img
        }
        else
        if segue.identifier == segueIdentifiers.dashToAddress
        {
            
        }
        else
        if segue.identifier == segueIdentifiers.dashToSecurityScan
        {
            let data = sender as! securityScanType
            let destVC = segue.destination as! SecurityScanning
            destVC.scanType = data
            
        }
        if segue.identifier == segueIdentifiers.dashToNotice{
            print("prepare")
            let indexPath = sender as! IndexPath
            if indexPath.row == announcementCollectionView.numberOfItems(inSection: 0) - 1
            {
                
            }
            else
            {
                let cell = announcementCollectionView.cellForItem(at: indexPath) as! AnnouncementCollectionCell
                let img = cell.announceImage.image
               // performSegue(withIdentifier: segueIdentifiers.noticeToSingle, sender: img)
                let destinationVC = segue.destination as! Notice
                destinationVC.single = indexPath.row
                destinationVC.singleImage = img
                
            }

        }
            
        
        
    }
    func reloadAnnouncementImage()
    {
        var imageSource = [ImageSource]()
        for n in 0 ... announcementImages.count - 1
        {
            let x =  ImageSource(image: announcementImages[n])
            imageSource.append(x)
        }
        slideShow.setImageInputs(imageSource)
        slideShow.contentScaleMode = .scaleToFill

    }
    func imageSlideshow(_ imageSlideshow: ImageSlideshow, didChangeCurrentPageTo page: Int) {
        announcementTextLabel.text = heading[page]
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == announcementCollectionView
        {
        let cell = announcementCollectionView.dequeueReusableCell(withReuseIdentifier: reuseCollectionIdentifier.announcemnetCellIdentifier, for: indexPath) as! AnnouncementCollectionCell
        if indexPath.row == (announcementData?.announcementList!.count)!
        {
            cell.announceImage.isHidden = true
            cell.announceLabel.isHidden = false
            cell.labelView.layer.cornerRadius = 5
            cell.labelView.layer.borderWidth = 0
            cell.labelView.backgroundColor = .clear
        }
        else
        {
       /* if let img = announcementData?.announcementList?[indexPath.row].image
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)announce/\(img)")
            //print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) 
                DispatchQueue.main.async { [self] in
                    if let d = data
                    {
                        cell.announceImage.image = UIImage(data: d)
                        self.announcementImages.append(UIImage(data: d)!)
                        print("i")
                       // self.reloadAnnouncementImage()

                    }
                }
            }
        } */
            cell.announceLabel.text = ""
      //  cell.announceView.layer.cornerRadius = 5
        //cell.announceLabel.text = announcementData?.announcementList?[indexPath.row].heading ?? ""
        }
        return cell
        }
        else
        {
            let cell = dashboardCollection.dequeueReusableCell(withReuseIdentifier: reuseCollectionIdentifier.dashboardCellIdentfier, for: indexPath) as! DashboardCell
            if isSecurity
            {
                switch indexPath.row {
                case 0:
                    cell.titleLabel.text = "Visitors Scan"
                    cell.iconImage.image = #imageLiteral(resourceName: "visitors")
                case 1:
                    cell.titleLabel.text = "Access Scan"
                    cell.iconImage.image = #imageLiteral(resourceName: "access-1")
                case 3:
                    cell.titleLabel.text = "Services"
                    cell.iconImage.image = #imageLiteral(resourceName: "services-1")
                case 5:
                    cell.titleLabel.text = "Useful Info"
                    cell.iconImage.image = #imageLiteral(resourceName: "ebill")
             
                case 2:
                    cell.titleLabel.text = "Facilities Scan"
                    cell.iconImage.image = #imageLiteral(resourceName: "facilities")
                case 6:
                    cell.titleLabel.text = "FAQ"
                    cell.iconImage.image = #imageLiteral(resourceName: "faq")
                case 7:
                    cell.titleLabel.text = "Intercoms"
                    cell.iconImage.image = #imageLiteral(resourceName: "intercom")
              
                case 4:
                    cell.titleLabel.text = "Visitor Record"
                    cell.iconImage.image = #imageLiteral(resourceName: "visitorrecord")
             
                default:
                    print("")
                }

            }
            else{
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = "Visitors"
                cell.iconImage.image = #imageLiteral(resourceName: "visitors")
            case 1:
                cell.titleLabel.text = "Access"
                cell.iconImage.image = #imageLiteral(resourceName: "access-1")
            case 2:
                cell.titleLabel.text = "Services"
                cell.iconImage.image = #imageLiteral(resourceName: "services-1")
            case 3:
                cell.titleLabel.text = "Useful Info"
                cell.iconImage.image = #imageLiteral(resourceName: "ebill")
            case 4:
                cell.titleLabel.text = "Users"
                cell.iconImage.image = #imageLiteral(resourceName: "users")
            case 5:
                cell.titleLabel.text = "Facilities"
                cell.iconImage.image = #imageLiteral(resourceName: "facilities")
            case 6:
                cell.titleLabel.text = "FAQ"
                cell.iconImage.image = #imageLiteral(resourceName: "faq")
            case 7:
                cell.titleLabel.text = "Intercoms"
                cell.iconImage.image = #imageLiteral(resourceName: "intercom")
            case 8:
                cell.titleLabel.text = "Address"
                cell.iconImage.image = #imageLiteral(resourceName: "address")
            case 9:
                cell.titleLabel.text = "Visitor Record"
                cell.iconImage.image = #imageLiteral(resourceName: "visitorrecord")
            case 10:
                cell.titleLabel.text = "E-Billing"
                cell.iconImage.image = #imageLiteral(resourceName: "ebill")
            case 11:
                cell.titleLabel.text = "Statement of Account"
                cell.iconImage.image = #imageLiteral(resourceName: "statement")
            default:
                print("")
            }
            }
            cell.layer.addBorder(edge: .left, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1), thickness: 1)
            cell.iconImage1.image = cell.iconImage.image

            return cell
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == announcementCollectionView
        {
        if indexPath.row == (announcementData?.announcementList!.count)!
        {
            return CGSize(width: 90, height: announcementCollectionView.frame.height)

        }
        else
        {
            return CGSize(width: view.frame.width, height: announcementCollectionView.frame.height)

        }
        }
        else
        {
            let approximateWidthOfContent = view.frame.width / 4.4
                // x is the width of the logo in the left

              let size = CGSize(width: approximateWidthOfContent, height: 1000)

              //1000 is the large arbitrary values which should be taken in case of very high amount of content
            let str = "Statement of account"
           
            let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
             let estimatedFrame = NSString(string: str).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
            let h = dashboardCollection.frame.height / 3.3
            return CGSize(width: view.frame.width / 4.4, height: h)
        }
        
    }
    
}

extension CALayer {

func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {

    let border = CALayer()

    switch edge {
    case .top:
        border.frame = CGRect(x: 0, y: 0, width: frame.width, height: thickness)
    case .bottom:
        border.frame = CGRect(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
    case .left:
        let h = frame.height / 4
        border.frame = CGRect(x: 0, y: h / 2, width: thickness, height: frame.height - h)
    case .right:
        border.frame = CGRect(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
    default:
        break
    }
    border.backgroundColor = color.cgColor
    addSublayer(border)
    }
}
