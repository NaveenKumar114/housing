//
//  VisitorRecord.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-23.
//

import UIKit

class VisitorRecord: UIViewController {
    let child = SpinnerViewController()
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var visitorRecordTableView: UITableView!
    var addressList : AddressJSON?
    var visitorRecordData : VisitorRecordUserJSON?
    var addressIDselected : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressLabel.addGestureRecognizer(gesture)
        addressLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)

        visitorRecordTableView.delegate = self
        visitorRecordTableView.dataSource = self
        visitorRecordTableView.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        visitorRecordTableView.register(UINib(nibName: reuseNibIdentifier.visitorRecordNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.visitorRecordCellIdentifier)
        makePostCallAddress()
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            addressLabel.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            let addressId = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            makePostCallVisitorRecord(addressID: addressId!)
            createSpinnerView()
            addressIDselected = addressId

        }
        self.visitorRecordTableView.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
                  refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
                visitorRecordTableView.addSubview(refreshControl)

    }
    @objc func refresh(_ sender: AnyObject) {
          print("refresh")
        makePostCallVisitorRecord(addressID: addressIDselected!)
      }

    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    func makePostCallVisitorRecord(addressID : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_visitors")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(VisitorRecordUserJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.visitorRecordData = loginBaseResponse
                        self.visitorRecordTableView.reloadData()
                        self.removeSpinnerView()
                        self.refreshControl.endRefreshing()

                           if loginBaseResponse?.visitorList?.count == 0
                            {
                               let alert = UIAlertController(title: "Visitor Record", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                               alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                               self.present(alert, animated: true, completion: nil)
                              }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    }
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressLabel.text = UIAlertAction.title
                        print(n)
                        createSpinnerView()
                        makePostCallVisitorRecord(addressID: listOfAllAddress[n].addressID!)
                        addressIDselected = listOfAllAddress[n].addressID!
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }

}

extension VisitorRecord: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = visitorRecordTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.visitorRecordCellIdentifier) as! VisitorRecordCell
        if let safeData = visitorRecordData?.visitorList?[indexPath.row]
        {
            cell.name.text = safeData.name
            cell.date.text = convertDateFormater(safeData.validityStart!)
            cell.status.text = safeData.status
            cell.type.text = safeData.purpose
            cell.vehicleNo.text = safeData.vehicleNo
            cell.inDate.text = convertDateFormater(safeData.validityStart!)
            cell.outDate.text = convertDateFormater(safeData.validityEnd!)
            cell.inTime.text = safeData.inTime
            cell.outTime.text = safeData.outTime
            cell.status.textColor = .systemGreen
            if safeData.status == "PENDING" || safeData.status == "REJECTED"
            {
                cell.status.textColor = .red
            }
            if safeData.status == "PENDING"
            {
                let formatterForTime = DateFormatter()

                let date = Date()
          //      let currentTimeFormat2 = formatterForTime.string(from: date.addingTimeInterval(15 * 60) as Date )
               // let currentTIme2 = formatterForTime.date(from: currentTimeFormat2)!
                let pass = "\(safeData.validityStart!) \(safeData.startTime!)"
                formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let passDate = formatterForTime.date(from: pass)!
                let currentDateF = formatterForTime.string(from: date as Date)
                let curr = formatterForTime.date(from: currentDateF) // curretn time
                let passPlusGrace = passDate.addingTimeInterval(15*60) // pass plus 15 grace
                
                if curr! > passDate && curr! < passPlusGrace
                {
                    
                }
                else
                {
                    if curr! > passDate
                    {
                        cell.status.text = "EXPIRED"
                    }
                }
              
            }

        }
        return cell
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 150
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visitorRecordData?.visitorList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        visitorRecordTableView.deselectRow(at: indexPath, animated: true)
        //openPass(index: indexPath)
    }
    @objc func openPass(index : IndexPath)
    {
        let storyboard = UIStoryboard(name: "VisitorPass", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "visitorPass") as! VisitorPass
        myAlert.visitorHistoryData = visitorRecordData?.visitorList?[index.row]
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }

    
    
}
