//
//  FacilitySecurity.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-10.
//

import UIKit

class FacilitySecurity: UIViewController {

    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var validity: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    @IBOutlet weak var address: UILabel!
    
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var facilityName: UILabel!
    var passData : SecurityFacilityJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.layer.cornerRadius = 25
        //self.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
    
        if passData != nil
        {
            if let safeData = passData?.facilities
            {
                userName.text = safeData.createBy
                validity.text = "Active"
                startLabel.text = ": \(safeData.facilitiesStartdate ?? "") \(safeData.facilitiesStarttime ?? "")"
                endLabel.text = ": \(safeData.facilitiesEnddate ?? "") \(safeData.facilitiesEndtime ?? "")"
                facilityName.text = safeData.facilitiesName
                address.text = safeData.address
               // testTime()
            }
        }
    }

    func testTime() //delete later
    {
        let date = NSDate()
        let formatterForDate = DateFormatter()
        formatterForDate.dateFormat = "yyyy-MM-dd"
        let currentDateFormat = formatterForDate.string(from: date as Date)
        let currentDate = formatterForDate.date(from: currentDateFormat)!
        
        let formatterForTime = DateFormatter()
        formatterForTime.dateFormat = "HH:mm:ss"
        let currentTimeFormat = formatterForTime.string(from: date as Date)
        let currentTIme = formatterForTime.date(from: currentTimeFormat)!
        let startDate = formatterForDate.date(from: (passData?.facilities?.facilitiesStartdate)!)!
        let endDate = formatterForDate.date(from: (passData?.facilities?.facilitiesEnddate)!)!
        let startTime = formatterForTime.date(from: (passData?.facilities?.facilitiesStarttime)!)!
        let endTime = formatterForTime.date(from: (passData?.facilities?.facilitiesEndtime)!)!
        var passExpired = false
        var passNotOpen = false
        if currentDate > startDate
        {
            passExpired = true
        }
        else if currentDate < startDate
        {
            passNotOpen = true
        }
        else if currentTIme > startTime
        {
            passExpired = true
        }
        else if currentTIme < startTime
        {
            passNotOpen = true
        }
        if passExpired
        {
            
            validity.text = "PASS EXPIRED"
            validity.textColor = .red
        }
        if passNotOpen
        {
            
            validity.text = "PASS NOT ACTIVE"
            validity.textColor = .red
        }
    }
}
