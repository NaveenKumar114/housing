//
//  NewBulkPassHolder.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-30.
//

import UIKit

class NewBulkPassHolder: UIViewController , UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    var pickerViewArray = [UITextField]()
    var identificationTypes = ["NRIC" , "Passport"]
    var textfieldOpenFirstTime = [true]

    @IBOutlet weak var identificationType: UITextField!
    @IBOutlet weak var facePhoto: UIImageView!
    @IBOutlet weak var proofPhoto: UIImageView!
    @IBOutlet weak var vehicleNumber: UITextField!
    @IBOutlet weak var vehicleModel: UITextField!
    @IBOutlet weak var icOrPassport: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var faceContaigningView: UIView!
    @IBOutlet weak var identiContaingingView: UIView!
    var faceImageBool : Bool = false
    var proofImageBool : Bool = false
    var delegate : addBulkUserProtocol?
    var selectedImageView : UIImageView?

    override func viewDidLoad() {
        super.viewDidLoad()
        vehicleModel.delegate = self
        vehicleModel.font = UIFont(name: "Arial-Rounded", size: 15.0)
        vehicleNumber.delegate = self
        vehicleNumber.font = UIFont(name: "Arial-Rounded", size: 15.0)
        icOrPassport.delegate = self
        icOrPassport.font = UIFont(name: "Arial-Rounded", size: 15.0)
        email.delegate = self
        email.font = UIFont(name: "Arial-Rounded", size: 15.0)
        phoneNumber.font = UIFont(name: "Arial-Rounded", size: 15.0)
        name.font = UIFont(name: "Arial-Rounded", size: 15.0)
        phoneNumber.delegate = self
        name.delegate = self
        faceContaigningView.layer.borderWidth = 1
        faceContaigningView.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
        faceContaigningView.layer.cornerRadius = 5
        
        identiContaingingView.layer.borderWidth = 1
        identiContaingingView.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
        identiContaingingView.layer.cornerRadius = 5
        
        mainView.layer.borderWidth = 1
        mainView.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
        mainView.layer.cornerRadius = 5
        
        let gestureident = UITapGestureRecognizer(target: self, action: #selector(toImage(sender:)))
        proofPhoto.addGestureRecognizer(gestureident)
        let gestureface = UITapGestureRecognizer(target: self, action: #selector(toImage(sender:)))
        facePhoto.addGestureRecognizer(gestureface)
       pickerViewArray  = [identificationType]
        identificationType.delegate = self
        identificationType.font = UIFont(name: "Arial-Rounded", size: 15.0)
            createPickerView(textField: identificationType, tag: 100)
            
        
        dismissPickerView()

    }
    @IBAction func clsoeButton(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)

    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        
        identificationType.inputAccessoryView = toolBar
        
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    @objc func toImage(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UIImageView
        selectedImageView = x
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }

    @IBAction func addPressed(_ sender: Any) {
        if vehicleNumber.text == "" || vehicleModel.text == "" || icOrPassport.text == "" || email.text == "" || phoneNumber.text == "" || name.text == "" || !faceImageBool || !proofImageBool || identificationType.text == ""
        {
            let alert = UIAlertController(title: "Add User", message: "Please enter all details", preferredStyle: UIAlertController.Style.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let data = seaseonBulkDataType(name: name.text!, email: email.text!, phoneNumber: phoneNumber.text!, icOrPassport: icOrPassport.text!, vehicleModel: vehicleModel.text!, vehicleNumber: vehicleNumber.text!, proofPhoto: proofPhoto.image!, facePhoto: facePhoto.image!, identifiationType: identificationType.text!)
            delegate?.addUser(data: data)
            self.dismiss(animated: true, completion: nil)

        }
    }
}


extension NewBulkPassHolder : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        if selectedImageView == facePhoto
        {
            faceImageBool = true
            
            print("face")
        }
        else
        {
            if selectedImageView == proofPhoto
            {
                proofImageBool = true
                print("proof")
            }
        }
        selectedImageView?.image = pickedImage
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
}





protocol addBulkUserProtocol
{
    func addUser(data : seaseonBulkDataType)
}

extension NewBulkPassHolder : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""
        {
        switch textField.tag {
        case 100:
            pickerViewArray[0].text = identificationTypes[0]

        default:
            print("err")
        }
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
       
           print("change")
           var isPickerView = false
           if textField.text != ""
           {
           switch textField.tag {
           case 100:
               isPickerView = true
           default:
               print("errTextDidBegin")
           }
           }
           if textField.text != "" && isPickerView && textfieldOpenFirstTime[textField.tag - 100] == false
           {
               view.endEditing(true)

           }
           else
           {
               if isPickerView
               {
               textfieldOpenFirstTime[textField.tag - 100] = false
               }
           }

       }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 100:
            return identificationTypes.count
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 100:
            return identificationTypes[row]

    
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 100:
            pickerViewArray[0].text = identificationTypes[row]

        default:
            print("err")
        }
    }
}
