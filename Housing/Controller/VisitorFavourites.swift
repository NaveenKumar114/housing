//
//  VisitorFavourites.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-27.
//

import UIKit
import SwipeCellKit
class VisitorFavourites: UIViewController{
    let child = SpinnerViewController()
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var addressLabel: UILabel!
    var addressList : AddressJSON?
    var visitorFavouriteData : VisitorFavouritesJSON?
    @IBOutlet weak var historyTabel: UITableView!
    var addressIdSelected : String?

    override func viewDidLoad() {
        super.viewDidLoad()
        createSpinnerView()
        historyTabel.delegate = self
        historyTabel.dataSource = self
        historyTabel.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        historyTabel.register(UINib(nibName: reuseNibIdentifier.historyNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.historyCellIdentifier)
        makePostCallAddress()
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressLabel.addGestureRecognizer(gesture)
        addressLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)

        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            addressLabel.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            let addressId = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            makePostCallVisitorRecord(addressID: addressId!)
            addressIdSelected = addressId!

        }
        makePostCallAddress()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUser(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.passAddedOrUpdated)"), object: nil)
        self.historyTabel.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
                  refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
                historyTabel.addSubview(refreshControl)
    }
  


    @objc func refresh(_ sender: AnyObject) {
          print("refresh")
          makePostCallVisitorRecord(addressID: addressIdSelected!)
      }

    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }

    @objc func updateUser(notfication: NSNotification) {
    visitorFavouriteData = nil
    makePostCallVisitorRecord(addressID: addressIdSelected!)
    }
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
  
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressLabel.text = UIAlertAction.title
                        print(n)
                        createSpinnerView()
                        makePostCallVisitorRecord(addressID: listOfAllAddress[n].addressID!)
                        addressIdSelected = listOfAllAddress[n].addressID!

                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    
    func makePostCallVisitorRecord(addressID : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_visitors_fav")
      {
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(VisitorFavouritesJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.visitorFavouriteData = loginBaseResponse
                        self.historyTabel.reloadData()
                        self.removeSpinnerView()
                        self.refreshControl.endRefreshing()

                        if loginBaseResponse?.visitorList?.count == 0
                                {
                                   let alert = UIAlertController(title: "Favourites", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                   //self.present(alert, animated: true, completion: nil)
                                  }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    }
}

extension VisitorFavourites: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTabel.dequeueReusableCell(withIdentifier: reuseCellIdentifier.historyCellIdentifier) as! HistoryCell
        if let safeData = visitorFavouriteData?.visitorList?[indexPath.row]
        {
            cell.name.text = safeData.name
            cell.date.text = convertDateFormater(safeData.validityStart!)
            cell.status.text = safeData.status
            cell.type.text = safeData.purpose
            cell.vehicleNo.text = safeData.vehicleNo
            
                cell.status.textColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
            cell.status.text = ""
            cell.layer.cornerRadius = 5
            cell.heathImage.image = UIImage(named: "like")?.withTintColor(.systemRed)
        }
        cell.selectedBackgroundView = .none
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visitorFavouriteData?.visitorList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(visitorFavouriteData?.visitorList?[indexPath.row] as Any)
        historyTabel.deselectRow(at: indexPath, animated: true)

        
    }
    
   func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
        let titleForSwipe = "REMOVE"
       //let colorForTitle : UIColor = .systemRed
   
        let action = UIContextualAction(style: .destructive, title:  titleForSwipe, handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                print("Update action ...")
                success(true)
            self.makePostCallRemoveVisitor(visitorID: (self.visitorFavouriteData?.visitorList?[indexPath.row].visitorID)!, indexpath: indexPath)
            
            })
      //  action.backgroundColor = .yellow
        let actionApply = UIContextualAction(style: .normal, title:  "USE NOW", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                print("Apply")
            success(true)

            self.applyFavourites(pass: (self.visitorFavouriteData?.visitorList?[indexPath.row])!)
            NotificationCenter.default.post(name: Notification.Name("\(notificationNames.applyFavChangeView)"), object: nil, userInfo: nil)

                            
            })
    let actionMore = UIContextualAction(style: .normal, title:  "MORE", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            print("more")
        success(true)
        self.performSegue(withIdentifier: segueIdentifiers.vistorFavTOFavDetail, sender: indexPath)


                        
        })
    actionMore.backgroundColor = .lightGray
    actionMore.image = UIImage(systemName: "ellipsis")
    actionApply.backgroundColor = .red
    action.backgroundColor = .systemRed
    actionApply.backgroundColor = #colorLiteral(red: 0.9450980392, green: 0.5764705882, blue: 0.2, alpha: 1)
    actionApply.image = UIImage(systemName: "hand.tap.fill")
    action.image = UIImage(systemName: "trash.fill")

            return UISwipeActionsConfiguration(actions: [action , actionApply , actionMore])
        }
  /*  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath, for orientation: SwipeActionsOrientation) -> [SwipeAction]? {
        guard orientation == .right else { return nil }

        let deleteAction = SwipeAction(style: .default, title: "Delete") { action, indexPath in
            // handle action by updating model with deletion
        }

        // customize the action appearance
        deleteAction.image = UIImage(named: "trash")
    
        

        return [deleteAction]
    } */
    func applyFavourites(pass: FavList)
    {
        let data : [String : FavList] = ["pass" : pass]
        NotificationCenter.default.post(name: Notification.Name("\(notificationNames.applyFavoutites)"), object: nil, userInfo: data)
     
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.vistorFavTOFavDetail
        {
            let i = sender as! IndexPath
            let x = visitorFavouriteData?.visitorList?[i.row]
            let vc = segue.destination as! FavouritesDetails
            vc.visitroData = x
        }
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func makePostCallRemoveVisitor(visitorID : String , indexpath: IndexPath) {
     
        let decoder = JSONDecoder()
        let json: [String: Any] = ["visitor_id":"\(visitorID)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)remove_visitor_fav")
        {
            
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(RemoveVisitorAndFavouritesJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            self.visitorFavouriteData?.visitorList!.remove(at: indexpath.row)

                            //self.makePostCallVisitorRecord(addressID: self.addressIdSelected!)
                            self.historyTabel.deleteRows(at: [indexpath], with: .top)
                            let alert = UIAlertController(title: "Visitor", message: "Successfully Deleted", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                           // self.present(alert, animated: true, completion: nil)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    
}

extension UIImage {
     func colored(in color: UIColor) -> UIImage {
         let renderer = UIGraphicsImageRenderer(size: size)
         return renderer.image { context in
             color.set()
             self.withRenderingMode(.alwaysTemplate).draw(in: CGRect(origin: .zero, size: size))
         }
     }
 }

