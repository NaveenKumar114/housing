//
//  SettingsTab.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-15.
//

import UIKit
import WebKit
import Alamofire
class SettingsTab: UIViewController {
    @IBOutlet weak var privacyView: DashboardStyle!
    @IBOutlet weak var contactUsVIew: DashboardStyle!
    @IBOutlet weak var viewContainingWebview: UIView!
    var profileImageUploadimage : UIImage?
    @IBOutlet weak var forgotPasswordContainer: UIView!
    
    @IBOutlet weak var confirmPasswordContainer: UIView!
    @IBOutlet weak var contactUsContainerView: UIView!
    @IBOutlet weak var profileVIew: UIView!
    @IBOutlet weak var labelForwebview: UILabel!
    @IBOutlet weak var settingsTopView: UIView!
    @IBOutlet weak var settWebView: WKWebView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var changePasswordView: DashboardStyle!
    @IBOutlet weak var termsAndConditionView: DashboardStyle!
    @IBOutlet weak var myProfileView: DashboardStyle!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var settingsCollection: UICollectionView!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var logOutVIew: DashboardStyle!
    override func viewDidLoad() {
        super.viewDidLoad()
        NotificationCenter.default.addObserver(self, selector: #selector(confirmEmail(notfication:)), name: NSNotification.Name(rawValue: "confirmEmail"), object: nil)
        settingsCollection.delegate = self
        settingsCollection.dataSource = self
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toImage))
            profileImage.isUserInteractionEnabled = true
            profileImage.addGestureRecognizer(tapGestureRecognizer)
        settingsCollection.register(UINib(nibName: reuseCollectionNibIdentifier.dashboardNibIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCollectionIdentifier.dashboardCellIdentfier)
      //  setUpSettingsSegue()
        let defaults = UserDefaults.standard
        nameLabel.text = defaults.string(forKey: userDefaultsKey.name)
        emailLabel.text = defaults.string(forKey: userDefaultsKey.email)
        addressLabel.text = defaults.string(forKey: userDefaultsKey.address)
        if let img = defaults.string(forKey: userDefaultsKey.profileImg)
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                if   let image = UIImage(data: data!)
                {
                  
                  
                    profileImage.image = image
                    profileImage.layer.cornerRadius = 0.5 * profileImage.bounds.size.width
                    profileImage.clipsToBounds = true
                    profileImage.layer.borderWidth = 8
                    profileImage.layer.borderColor = UIColor.white.cgColor
                    viewForShadow.dropShadow()
                    viewForShadow.layer.cornerRadius = 0.5 * profileImage.bounds.size.width

                }
                }
            }
        }
        else
        {
            profileImage.layer.cornerRadius = 0.5 * profileImage.bounds.size.width
            profileImage.clipsToBounds = true
            profileImage.layer.borderWidth = 8
            profileImage.layer.borderColor = UIColor.white.cgColor
            viewForShadow.dropShadow()
            viewForShadow.layer.cornerRadius = 0.5 * profileImage.bounds.size.width

        }
    }
    @objc func confirmEmail(notfication: NSNotification)
    {
        self.profileVIew.isHidden = true
        self.viewContainingWebview.isHidden = true
        self.contactUsContainerView.isHidden = true
        self.settingsTopView.isHidden = true
        self.forgotPasswordContainer.isHidden = true
        self.confirmPasswordContainer.isHidden = false
    }
    @objc func confirmPassword(notfication: NSNotification)
    {
        
    }
    func setUpSettingsSegue()
    {
        let gestureVisitor = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        myProfileView.addGestureRecognizer(gestureVisitor)
        let gestureAccess = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        termsAndConditionView.addGestureRecognizer(gestureAccess)
        let gestureService = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        changePasswordView.addGestureRecognizer(gestureService)
        let gestureUseful = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        logOutVIew.addGestureRecognizer(gestureUseful)
        let gestureUser = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        privacyView.addGestureRecognizer(gestureUser)
        let gestureFacility = UITapGestureRecognizer(target: self, action: #selector(userViewsSegue(_:)))
        contactUsVIew.addGestureRecognizer(gestureFacility)
        
    }
    @objc func userViewsSegue(_ sender :  UIGestureRecognizer)
    {
        switch sender.view {
        case myProfileView:
            performSegue(withIdentifier: settingsSegue.profile, sender: nil)
        case termsAndConditionView:
            let newViewController = TermsAndCondition()
            self.navigationController?.pushViewController(newViewController, animated: true)
        case changePasswordView:
            performSegue(withIdentifier: settingsSegue.forgotPassword, sender: nil)
        case logOutVIew:
            logOut()
        case privacyView:
            let newViewController = PrivacyPolicy()
            self.navigationController?.pushViewController(newViewController, animated: true)
        case contactUsVIew:
            performSegue(withIdentifier: settingsSegue.contactUs, sender: nil)
        
        default:
            print("other")
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
   super.viewWillAppear(animated);
   self.navigationController?.isNavigationBarHidden = true
   }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        self.navigationController?.isNavigationBarHidden = false
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.settingsToMyProfile
        {
            let destVC = segue.destination as! ShowUser
            destVC.showProfile = true
            NotificationCenter.default.addObserver(self, selector: #selector(updateUser(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.imageUpdated)"), object: nil)
        }
     
    }
    @objc func updateUser(notfication: NSNotification) {
        let defaults = UserDefaults.standard
        if let img = defaults.string(forKey: userDefaultsKey.profileImg)
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                if   let image = UIImage(data: data!)
                {
                  
                    profileButton.setImage(image, for: .normal)
                    profileButton.layer.cornerRadius = 0.5 * profileButton.bounds.size.width
                    profileButton.clipsToBounds = true
                    profileButton.layer.borderWidth = 1
                    profileButton.layer.borderColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor

                }
                }
            }
        }
    }
    
    @IBAction func termsAndConditionClicked(_ sender: Any) {
        let newViewController = TermsAndCondition()
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func privacyClicked(_ sender: Any) {
        let newViewController = PrivacyPolicy()
        self.navigationController?.pushViewController(newViewController, animated: true)
    }
    @IBAction func logOutPressed(_ sender: Any) {
    
     }
    func logOut()
    {
        
         let alert = UIAlertController(title: "LogOut", message: "Are You Sure You Want To Sign Out", preferredStyle: .alert)
         
         let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
             let defaults = UserDefaults.standard
  
             defaults.removeObject(forKey: "isLoggedIn")
             defaults.removeObject(forKey: "subID")
             defaults.removeObject(forKey: "userID")
             defaults.removeObject(forKey: "name")
             defaults.removeObject(forKey: userDefaultsKey.addressId)
             defaults.removeObject(forKey: userDefaultsKey.address)
             defaults.removeObject(forKey: userDefaultsKey.email)
             defaults.removeObject(forKey: userDefaultsKey.housingName)
             defaults.removeObject(forKey: userDefaultsKey.phoneNumber)
             defaults.removeObject(forKey: userDefaultsKey.memberRole)
             defaults.removeObject(forKey: userDefaultsKey.identifiactionType)
             defaults.removeObject(forKey: userDefaultsKey.identificationNumber)
             defaults.removeObject(forKey: userDefaultsKey.memberRoleText)
             defaults.removeObject(forKey: userDefaultsKey.userMemberID)
            defaults.removeObject(forKey: userDefaultsKey.profileImg)









             let storyboard = UIStoryboard(name: "Main", bundle: nil)
             
             // if user is logged in before
             
             // instantiate the main tab bar controller and set it as root view controller
             // using the storyboard identifier we set earlier
             let loginController = storyboard.instantiateViewController(identifier: "login")
             (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginController)
         })
         alert.addAction(ok)
         let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
         })
         alert.addAction(cancel)
         DispatchQueue.main.async(execute: {
             self.present(alert, animated: true)
         })
    }
}


extension SettingsTab : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
     
            return 6
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for n in 0 ... 5
        {
            let cell = settingsCollection.cellForItem(at: IndexPath(row: n, section: 0)) as! DashboardCell
            cell.iconImage.tintColor = .darkGray
            cell.titleLabel.textColor = .darkGray
            var rect = cell.iconImage.frame
            rect.size.height = 20
           // cell.iconImage.frame = rect
            cell.iconImage.isHidden = false
            cell.iconImage1.isHidden = true
        }
        let cell = settingsCollection.cellForItem(at: indexPath) as! DashboardCell
        cell.iconImage.tintColor = ConstantsUsedInProject.appThemeColor
        cell.titleLabel.textColor = ConstantsUsedInProject.appThemeColor
        var rect = cell.iconImage.frame
        rect.size.height = 30
       // cell.iconImage.frame = rect
        cell.iconImage.isHidden = true
        cell.iconImage1.isHidden = false
            switch indexPath.row{
        
            case 0:
                //performSegue(withIdentifier: settingsSegue.profile, sender: nil)
                for view in self.settingsTopView.subviews {
                    view.removeFromSuperview()
                }
                self.profileVIew.isHidden = false
                self.viewContainingWebview.isHidden = true
                self.contactUsContainerView.isHidden = true
                self.forgotPasswordContainer.isHidden = true
                self.confirmPasswordContainer.isHidden = true

            case 4:
                for view in self.settingsTopView.subviews {
                    view.removeFromSuperview()
                }
               
                let x = UserDefaults.standard.string(forKey: userDefaultsKey.termsAndCondition)
                self.settWebView.loadHTMLString(x!, baseURL: nil)
                self.viewContainingWebview.isHidden = false
                self.profileVIew.isHidden = true
                self.settingsTopView.isHidden = true
                self.labelForwebview.text = "Terms And Conditions"
                self.contactUsContainerView.isHidden = true
                self.forgotPasswordContainer.isHidden = true
                self.confirmPasswordContainer.isHidden = true


            case 1:
                //performSegue(withIdentifier: settingsSegue.forgotPassword, sender: nil)

                for view in self.settingsTopView.subviews {
                    view.removeFromSuperview()
                }
               
                self.profileVIew.isHidden = true
                self.viewContainingWebview.isHidden = true
                self.contactUsContainerView.isHidden = true
                self.settingsTopView.isHidden = true
                self.forgotPasswordContainer.isHidden = false
                self.confirmPasswordContainer.isHidden = true


            case 5:
                logOut()
            case 3:
                for view in self.settingsTopView.subviews {
                    view.removeFromSuperview()
                }
               
                let x = UserDefaults.standard.string(forKey: userDefaultsKey.privacyPolicy)
                self.settWebView.loadHTMLString(x!, baseURL: nil)
                self.labelForwebview.text = "Privacy Policy"
                self.viewContainingWebview.isHidden = false
                self.profileVIew.isHidden = true
                self.settingsTopView.isHidden = true
                self.contactUsContainerView.isHidden = true
                self.forgotPasswordContainer.isHidden = true
                self.confirmPasswordContainer.isHidden = true


            case 2:
                for view in self.settingsTopView.subviews {
                    view.removeFromSuperview()
                }
                self.settingsTopView.isHidden = true

                self.profileVIew.isHidden = true
                self.viewContainingWebview.isHidden = true
                self.contactUsContainerView.isHidden = false
                print("con")
                self.forgotPasswordContainer.isHidden = true
                self.confirmPasswordContainer.isHidden = true



            default:
                print("other")
            }
            
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let cell = settingsCollection.dequeueReusableCell(withReuseIdentifier: reuseCollectionIdentifier.dashboardCellIdentfier, for: indexPath) as! DashboardCell
            switch indexPath.row {
            case 0:
                cell.titleLabel.text = "My Profile"
                cell.iconImage.image = #imageLiteral(resourceName: "loading_profile")
                let defaults = UserDefaults.standard
                if let img = defaults.string(forKey: userDefaultsKey.profileImg)
                {
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
                    print(urlStr)
                    let url = URL(string: urlStr)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        DispatchQueue.main.async { [self] in
                        if   let image = UIImage(data: data!)
                        {
                          
                          
                            cell.iconImage.image = image
                            cell.iconImage.layer.cornerRadius = 0.5 * cell.iconImage.bounds.size.width
                            profileImage.clipsToBounds = true
                            cell.iconImage.layer.borderWidth = 2
                            cell.iconImage.contentMode = .scaleToFill
                            cell.iconImage.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
                            cell.iconImage1.image = image
                            cell.iconImage1.layer.cornerRadius = 0.5 * cell.iconImage1.bounds.size.width
                            //profileImage1.clipsToBounds = true
                            cell.iconImage1.contentMode = .scaleToFill
                            cell.iconImage1.layer.borderWidth = 2
                            cell.iconImage1.layer.borderColor = ConstantsUsedInProject.appThemeColor.cgColor
                            

                        }
                        }
                    }
                }

               
            case 1:
                cell.titleLabel.text = "  Change Password"
                cell.iconImage.image = #imageLiteral(resourceName: "003-password")
            case 2:
                cell.titleLabel.text = "Contact Us"
                cell.iconImage.image = #imageLiteral(resourceName: "baseline_support_agent_black_48dp")
            case 3:
                cell.titleLabel.text = "Privacy"
                cell.iconImage.image = #imageLiteral(resourceName: "privacy")
            case 4:
                cell.titleLabel.text = "  Terms & Conditions"
                cell.iconImage.image = #imageLiteral(resourceName: "alert")

            case 5:
                cell.titleLabel.text = "Logout"
                cell.iconImage.image = #imageLiteral(resourceName: "009-logout")
          
            default:
                print("")
            }
        cell.iconImage1.image = cell.iconImage.image

            cell.layer.addBorder(edge: .left, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1), thickness: 1)
            return cell
        
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
   
            let approximateWidthOfContent = view.frame.width / 4.4
                // x is the width of the logo in the left

              let size = CGSize(width: approximateWidthOfContent, height: 1000)

              //1000 is the large arbitrary values which should be taken in case of very high amount of content
            var str = "Statement of account"
           
            let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
             let estimatedFrame = NSString(string: str).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        let h = settingsCollection.frame.height / 2.2
        return CGSize(width: view.frame.width / 3.3, height: h)
        
        
    }
    
    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                }
            }
            multipart.append(image, withName: "profile_image", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(ProfileUploadJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                print("success")
                                let defaults = UserDefaults.standard
                                defaults.setValue(loginBaseResponse?.userLogindetails?.profileImg, forKey: "img")

                                NotificationCenter.default.post(name: Notification.Name("\(notificationNames.imageUpdated)"), object: nil)
                                let alert = UIAlertController(title: "Update Profile", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in

                                   // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                                    print("Success")

                                } ))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Update Profile", message: "Unable To Update", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    
}

extension SettingsTab : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                let imageData = image.jpegData(compressionQuality: 0.50)
        let id = UserDefaults.standard.string(forKey: userDefaultsKey.userMemberID)!
        let json: [String: Any] = ["user_memberid" : "\(id)"]
        profileImageUploadimage = image
        profileImage.image = image
        profileImage.contentMode = .scaleAspectFill
        profileImage.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        uploadImage(image: imageData!, to: URL(string: "http://e-visitor.my/housing_android_api/Imagehelper/users_profileupload")!, params: json)
        viewForShadow.dropShadow()
        viewForShadow.layer.cornerRadius = 0.5 * profileImage.bounds.size.width
    }
}

