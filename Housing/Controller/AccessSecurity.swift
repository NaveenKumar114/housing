//
//  AccessSecurity.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-09.
//

import UIKit

class AccessSecurity: UIViewController {
    
    @IBOutlet weak var mainScrollView: UIScrollView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var validity: UILabel!
    @IBOutlet weak var startLabel: UILabel!
    
    @IBOutlet weak var endLabel: UILabel!
    @IBOutlet weak var accessName: UILabel!
    var passData : SecurityAccessJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        mainScrollView.layer.cornerRadius = 25
        //self.layer.borderColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
    
        if passData != nil
        {
            if let safeData = passData?.access
            {
                userName.text = safeData.userName
                validity.text = "Valid Card"
                startLabel.text = ": \(safeData.startDate ?? "") \(safeData.startTime ?? "")"
                endLabel.text = ": \(safeData.endDate ?? "") \(safeData.endTime ?? "")"
                accessName.text = safeData.accessName
                //testTime()
                
            }
        }
        
    }
    func testTime() //delete later
    {
        let date = NSDate()
        let formatterForDate = DateFormatter()
        formatterForDate.dateFormat = "yyyy-MM-dd"
        let currentDateFormat = formatterForDate.string(from: date as Date)
        let currentDate = formatterForDate.date(from: currentDateFormat)!
        
        let formatterForTime = DateFormatter()
        formatterForTime.dateFormat = "HH:mm:ss"
        let currentTimeFormat = formatterForTime.string(from: date as Date)
        let currentTIme = formatterForTime.date(from: currentTimeFormat)!
        let startDate = formatterForDate.date(from: (passData?.access?.startDate)!)!
        let endDate = formatterForDate.date(from: (passData?.access?.endDate)!)!
        let startTime = formatterForTime.date(from: (passData?.access?.startTime)!)!
        let endTime = formatterForTime.date(from: (passData?.access?.endTime)!)!
        var passExpired = false
        var passNotOpen = false
        if currentDate > startDate
        {
            passExpired = true
        }
        else if currentDate < startDate
        {
            passNotOpen = true
        }
        else if currentTIme > startTime
        {
            passExpired = true
        }
        else if currentTIme < startTime
        {
            passNotOpen = true
        }
        if passExpired
        {
            
            validity.text = "PASS EXPIRED"
            validity.textColor = .red
        }
        if passNotOpen
        {
            
            validity.text = "PASS NOT ACTIVE"
            validity.textColor = .red
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
