//
//  FavouritesDetails.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-06.
//

import UIKit

class FavouritesDetails: UIViewController {
    var visitroData : FavList?
    @IBOutlet weak var detailsTabelView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        detailsTabelView.dataSource = self
        detailsTabelView.delegate = self
        print(visitroData)
        detailsTabelView.separatorStyle = .none
        self.detailsTabelView.tableFooterView = UIView()
        self.title = "Details"
        // Do any additional setup after loading the view.
    }
    

  
}

extension FavouritesDetails : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        var heading = ""

            switch indexPath.section {
            case 0:
                heading = visitroData?.name ?? ""
                
            case 1:
                heading = visitroData?.email ?? ""
            case 2:
                heading = visitroData?.contactNo ?? ""
            case 3:
                heading = visitroData?.vehicleNo ?? ""
            case 4:
                heading = visitroData?.visitorType ?? ""
            default:
                print("error")
            }
        
        cell.textLabel?.attributedText = NSAttributedString(string: "\(heading)", attributes:
                                                                    [.underlineStyle: NSUnderlineStyle.single.rawValue])
        cell.textLabel?.textColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 15.0)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = UITableViewCell()
        var heading = ""
        switch section {
        case 0:
            heading = "Name:"
        case 1:
            heading = "Email Address:"
        case 2:
            heading = "Contact No:"
        case 3:
            heading = "Vehicle No:"
        case 4:
            heading = "Pass Type:"
        default:
            print("error")
        }
        cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 15.0)
        cell.textLabel?.text = heading
        //cell.textLabel?.textColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)

        return cell
    }
    
    
}
