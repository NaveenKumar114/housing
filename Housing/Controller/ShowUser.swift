//
//  ShowUser.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-11.
//

import UIKit

class ShowUser: UIViewController {
 
    

    @IBOutlet weak var viewContainingProfileImage: DashboardStyle!
    @IBOutlet weak var profileImageView: UIImageView!
    @IBOutlet weak var removeUserButton: CurvedButton!
    var showProfile = false
    @IBOutlet weak var headingLabel: UILabel!
    @IBOutlet weak var userDetailTableView: UITableView!
    var userDetail : UsermembersList?
    override func viewDidLoad() {
        super.viewDidLoad()
        userDetailTableView.delegate = self
        userDetailTableView.dataSource = self
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(addTapped))
          viewContainingProfileImage.isHidden = true

        if showProfile
        {
            viewContainingProfileImage.isHidden = true

            self.title = "My Profile"
            let gestureVisitor = UITapGestureRecognizer(target: self, action: #selector(toUploadProfile))
            viewContainingProfileImage.addGestureRecognizer(gestureVisitor)
            viewContainingProfileImage.isHidden = false
            viewContainingProfileImage.layer.masksToBounds = false
            viewContainingProfileImage.layer.shadowColor = UIColor.black.cgColor
            viewContainingProfileImage.layer.shadowOffset = CGSize(width: 0, height: 5)
            viewContainingProfileImage.layer.shadowRadius = CGFloat(5)
            viewContainingProfileImage.layer.shadowOpacity = 0.1
            viewContainingProfileImage.layer.cornerRadius = 20
            removeUserButton.isHidden = true
            let defaults = UserDefaults.standard
            if let img = defaults.string(forKey: userDefaultsKey.profileImg)
            {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                    if   let image = UIImage(data: data!)
                    {
                        
                      //  viewContainingProfileImage.isHidden = false
                       
                        profileImageView.image = image
                        profileImageView.layer.cornerRadius = 0.5 * profileImageView.bounds.size.width
                        profileImageView.clipsToBounds = true
                        profileImageView.layer.borderWidth = 1
                        profileImageView.contentMode = .scaleAspectFill

                        profileImageView.layer.borderColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor

                    }
                    }
                }
            }

        }
        else
        {
            self.title = "User"
        }
        if let safeData = userDetail
        {
            viewContainingProfileImage.isHidden = true

            if safeData.memberRole == "1"
            {
                removeUserButton.isHidden = true
            }
            if safeData.profileImg != nil
            {
                if let img = safeData.profileImg
                {
                    viewContainingProfileImage.isHidden = false
                    viewContainingProfileImage.layer.masksToBounds = false
                    viewContainingProfileImage.layer.shadowColor = UIColor.black.cgColor
                    viewContainingProfileImage.layer.shadowOffset = CGSize(width: 0, height: 5)
                    viewContainingProfileImage.layer.shadowRadius = CGFloat(5)
                    viewContainingProfileImage.layer.shadowOpacity = 0.1
                    viewContainingProfileImage.layer.cornerRadius = 20
                    let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
                    print(urlStr)
                    let url = URL(string: urlStr)
                    
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                        DispatchQueue.main.async { [self] in
                        if   let image = UIImage(data: data!)
                        {
                          
                           // viewContainingProfileImage.isHidden = false
                           
                            profileImageView.image = image
                            profileImageView.layer.cornerRadius = 0.5 * profileImageView.bounds.size.width
                            profileImageView.clipsToBounds = true
                            profileImageView.layer.borderWidth = 1
                            profileImageView.contentMode = .scaleAspectFill

                            profileImageView.layer.borderColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
                        }
                        }
                    }
                }
            }
        }
        self.userDetailTableView.tableFooterView = UIView()
        viewContainingProfileImage.isHidden = true

        
    }
    @objc func toUploadProfile()
    {
        performSegue(withIdentifier: segueIdentifiers.editProfileToUploadPRofileImage, sender: nil)
    }
    @objc func updateUser(notfication: NSNotification) {
        let defaults = UserDefaults.standard
        if let img = defaults.string(forKey: userDefaultsKey.profileImg)
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                if   let image = UIImage(data: data!)
                {
                    
                    viewContainingProfileImage.isHidden = false
                    viewContainingProfileImage.layer.masksToBounds = false
                    viewContainingProfileImage.layer.shadowColor = UIColor.black.cgColor
                    viewContainingProfileImage.layer.shadowOffset = CGSize(width: 0, height: 5)
                    viewContainingProfileImage.layer.shadowRadius = CGFloat(5)
                    viewContainingProfileImage.layer.shadowOpacity = 0.1
                    viewContainingProfileImage.layer.cornerRadius = 20
                    profileImageView.image = image
                    profileImageView.contentMode = .scaleAspectFill
                    profileImageView.layer.cornerRadius = 0.5 * profileImageView.bounds.size.width
                    profileImageView.clipsToBounds = true
                    profileImageView.layer.borderWidth = 1
                    profileImageView.layer.borderColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor

                }
                }
            }
        }
      
    }

    @IBAction func removeUserPressed(_ sender: Any) {
        let alert = UIAlertController(title: "Delete User", message: "Are you Sure", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { [self] action in
            if let safeData = userDetail
            {
                makePostCallRemoveUser(memberID: safeData.userMemberid!)
            }
            else
            {
                let alert = UIAlertController(title: "User", message: "Error", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
        
        
        
      
    }
    @objc func addTapped()
    {
        if showProfile
        {
        performSegue(withIdentifier: segueIdentifiers.showUserToEdit, sender: nil)
        }
        else
        {
            performSegue(withIdentifier: segueIdentifiers.showUserToEdit, sender: userDetail)

        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.showUserToEdit
        {
            if showProfile
            {
                print("ss")
                let dVC = segue.destination as! NewFamilyUser
                dVC.editProfile = true
            }
            else
            {
                print("ee")
                let data = sender as! UsermembersList
                let dVC = segue.destination as! NewFamilyUser
                dVC.editUserData = data
            }
        }
        if segue.identifier == segueIdentifiers.editProfileToUploadPRofileImage
        {
            let defaults = UserDefaults.standard
            if defaults.string(forKey: userDefaultsKey.profileImg) != nil
            {
                let dVC = segue.destination as! ProfileUplaod
                dVC.profileImage = profileImageView.image
                NotificationCenter.default.addObserver(self, selector: #selector(updateUser(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.imageUpdated)"), object: nil)

            }
        }
    }
    func makePostCallRemoveUser(memberID : String) {
     
        let decoder = JSONDecoder()
        let json: [String: Any] = ["user_memberid":"\(memberID)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)remove_usersmembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(RemoveVisitorAndFavouritesJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {

                            let alert = UIAlertController(title: "User", message: "Successfully Deleted", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.navigationController?.popViewController(animated: true)
                            }))
                            NotificationCenter.default.post(name: Notification.Name("\(notificationNames.userAddedORUpdated)"), object: nil)
                            // show the alert
                            self.present(alert, animated: true, completion: nil)                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "User", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

extension ShowUser : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        var heading = ""
        if showProfile
        {
            let defaults = UserDefaults.standard
        switch indexPath.section {
        case 0:
            heading = defaults.string(forKey: userDefaultsKey.name) ?? ""
        case 1:
            heading = defaults.string(forKey: userDefaultsKey.identifiactionType) ?? ""
        case 2: 
            heading = defaults.string(forKey: userDefaultsKey.identificationNumber) ?? ""
        case 3:
            heading = defaults.string(forKey: userDefaultsKey.phoneNumber) ?? ""
        case 4:
            heading = defaults.string(forKey: userDefaultsKey.email) ?? ""
        case 5:
            if defaults.string(forKey: userDefaultsKey.memberRoleText) == "1"
            {
            heading = "Admin"
            }
            else
            {
            heading = defaults.string(forKey: userDefaultsKey.memberRoleText) ?? ""
            }
            
        default:
            print("error")
        }
        }
        else
        {
            switch indexPath.section {
            case 0:
                heading = userDetail?.memberName ?? ""
            case 1:
                heading = userDetail?.memberIdentificationType ?? ""
            case 2:
                heading = userDetail?.memberIndentificationNo ?? ""
            case 3:
                heading = userDetail?.memberContact ?? ""
            case 4:
                heading = userDetail?.memberEmail ?? ""
            case 5:
                if userDetail?.memberRole == "1"
                {
                    heading = "Admin"

                }
                else
                {
                heading = userDetail?.userRole ?? ""
                }
            default:
                print("error")
            }
        }
        cell.textLabel?.text = heading
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = UITableViewCell()
        var heading = ""
        switch section {
        case 0:
            heading = "Preferred Name"
        case 1:
            heading = "Identification Name"
        case 2:
            heading = "Identification Number"
        case 3:
            heading = "Contact Number"
        case 4:
            heading = "Email Address"
        case 5:
            heading = "User Role"
        default:
            print("error")
        }
        cell.textLabel?.text = heading
        cell.textLabel?.textColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)
        return cell
    }
    
    
}
