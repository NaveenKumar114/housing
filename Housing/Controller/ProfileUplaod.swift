//
//  ProfileUplaod.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-23.
//

import UIKit
import Alamofire
class ProfileUplaod: UIViewController {

    @IBOutlet weak var profileName: UILabel!
    @IBOutlet weak var viewContainingProf: UIView!
    @IBOutlet weak var profileImageView: UIImageView!
    var profileImage : UIImage?
    override func viewDidLoad() {
        super.viewDidLoad()
        viewContainingProf.layer.cornerRadius = 20
        let name = UserDefaults.standard.string(forKey: userDefaultsKey.name)
        profileName.text = name
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(toImage))
            profileImageView.isUserInteractionEnabled = true
            profileImageView.addGestureRecognizer(tapGestureRecognizer)
        if profileImage != nil
        {
            profileImageView.image = profileImage
            profileImageView.contentMode = .scaleAspectFill
            profileImageView.cropAsCircleWithBorder(borderColor: UIColor(named: ConstantsUsedInProject.appThemeColorName)!, strokeWidth: 3)
        }
    }
    

    @objc func toImage()
    {
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    func uploadImage(image: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    multipart.append(data, withName: key)
                }
            }
            multipart.append(image, withName: "profile_image", fileName: "profile", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {

        
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(ProfileUploadJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                print("success")
                                let defaults = UserDefaults.standard
                                defaults.setValue(loginBaseResponse?.userLogindetails?.profileImg, forKey: "img")

                                NotificationCenter.default.post(name: Notification.Name("\(notificationNames.imageUpdated)"), object: nil)
                                let alert = UIAlertController(title: "Update Profile", message: "Successfully Updated", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default ,handler: { (_) in
                                    _ = self.navigationController?.popViewController(animated: true)

                                   // self.performSegue(withIdentifier: segueIdentifiers.profToPersonal, sender: nil)
                                    print("Success")

                                } ))

                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Update Profile", message: "Unable To Update", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }


}


extension ProfileUplaod : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        //journalImageView.image = pickedImage
        saveImage(image: pickedImage)
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
    func saveImage(image : UIImage)
    {                let imageData = image.jpegData(compressionQuality: 0.50)
        let id = UserDefaults.standard.string(forKey: userDefaultsKey.userMemberID)!
        let json: [String: Any] = ["user_memberid" : "\(id)"]
        profileImage = image
        profileImageView.image = image
        profileImageView.contentMode = .scaleAspectFill
        profileImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        uploadImage(image: imageData!, to: URL(string: "http://thefollo.com/housing/housing_android_api/Imagehelper/users_profileupload")!, params: json)
        
    }
}

