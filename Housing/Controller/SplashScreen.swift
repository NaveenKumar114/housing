//
//  SplashScreen.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-15.
//

import UIKit

class SplashScreen: UIViewController {
    @IBOutlet weak var follo: UILabel!
    var timerJob : Timer?
    var timerCountJob = 0
    var timerBuild : Timer?
    var timerCountBuild = 0
    var timerCountCenter = 1
    var timerFind : Timer?
    var findCount = 0
    @IBOutlet weak var folloHousing: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        timerJob = Timer.scheduledTimer(timeInterval: 0.13, target: self, selector: #selector(fireTimerJob), userInfo: nil, repeats: true)
    }
    
    
    @objc func fireTimerJob() {
        let job = "www.e-visitor.my "
        if timerCountJob <= job.count
        {
            follo.text = String(job.prefix(timerCountJob))
            timerCountJob = timerCountJob+1
        }
        let build = "e-visitor"
    folloHousing.text = String(build.prefix(timerCountBuild))
        timerCountBuild = timerCountBuild+1
        if timerCountJob > job.count
        {
            //print(timerCountJob , timerCountBuild)
            timerJob?.invalidate()
            ter()
        }
    }

   func ter()
   {
    timerJob?.invalidate()
    let storyboard = UIStoryboard(name: "Main", bundle: nil)
    
    // if user is logged in before
    
    // instantiate the main tab bar controller and set it as root view controller
    // using the storyboard identifier we set earlier
    let loginController = storyboard.instantiateViewController(identifier: "login")
    let storyboard2 = UIStoryboard(name: "Dashboard", bundle: nil)

    let dashboard = storyboard2.instantiateViewController(identifier: "dash")

    let loggedUsername = UserDefaults.standard.string(forKey: "isLoggedIn")
        if loggedUsername == "true" {
            // instantiate the main tab bar controller and set it as root view controller
            // using the storyboard identifier we set earlier
            
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(dashboard)                }
    else
        {
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginController)                }
   }

}
