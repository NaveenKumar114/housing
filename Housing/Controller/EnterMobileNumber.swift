//
//  EnterMobileNumber.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-11.
//
import FirebaseAuth
import Firebase
import UIKit
import CountryPickerView
class EnterMobileNumber: UIViewController {

    @IBOutlet weak var mobileNumberTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        mobileNumberTextField.leftView = cpv
        mobileNumberTextField.leftViewMode = .always
        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendButton(_ sender: Any) {
        if mobileNumberTextField.text != nil
        {
            sendotp()
        }
    }
    
    func sendotp()
    {
        Auth.auth().languageCode = "en"

        let phoneNumber = "+917904176492"
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { (verificationID, error) in
          if let error = error {
            print(error.localizedDescription)

            //self.showMessagePrompt(error.localizedDescription)
            return
          }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            self.performSegue(withIdentifier: "otp", sender: nil)

          // Sign in using the verificationID and the code sent to the user
          // ...
        }
    }

}
