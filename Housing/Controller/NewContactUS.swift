//
//  NewContactUS.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-10.
//

import UIKit

class NewContactUS: UIViewController {
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var profileImage: UIImageView!

    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var housingName: UILabel!
    @IBOutlet weak var contact: UILabel!
    @IBOutlet weak var identification: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard

        print("ss")
        housingName.text = defaults.string(forKey: userDefaultsKey.housingName)
        address.text = defaults.string(forKey: userDefaultsKey.housinAddress)
        email.text = defaults.string(forKey: userDefaultsKey.housingEmail)
        contact.text = defaults.string(forKey: userDefaultsKey.housingPhone)
        address.sizeToFit()
        identification.text = "\(defaults.string(forKey: userDefaultsKey.housingIdentType) ?? "") - \(defaults.string(forKey: userDefaultsKey.housingIdentNUmber) ?? "")"
        if let img = defaults.string(forKey: userDefaultsKey.housingImage)
        {
            let urlStr = ("https://e-visitor.my/uploads/housing/\(img)")
            print(urlStr)
            print("hou")
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                if   let image = UIImage(data: data!)
                {
                  
                  
                    profileImage.image = image
                    profileImage.layer.cornerRadius = 0.5 * profileImage.bounds.size.width
                    profileImage.clipsToBounds = true
                    profileImage.layer.borderWidth = 8
                    profileImage.layer.borderColor = UIColor.white.cgColor
                    viewForShadow.dropShadow()
                    viewForShadow.layer.cornerRadius = 0.5 * profileImage.bounds.size.width

                }
                }
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
