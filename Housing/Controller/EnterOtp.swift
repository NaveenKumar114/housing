//
//  EnterOtp.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-11.
//

import UIKit
import FirebaseAuth
class EnterOtp: UIViewController {
    @IBOutlet weak var otpTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func confirmButton(_ sender: Any) {
        if otpTextField.text != nil
        {
            verifyOtp()
        }
    }
   
    func verifyOtp()
    {
        let verificationCode = otpTextField.text!
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: verificationCode)
        Auth.auth().signIn(with: credential) { (authResult, error) in
          if let error = error {
            let authError = error as NSError
            print(authError.description)
            return
          }

            print("Corrects")
          // User has signed in successfully and currentUser object is valid
          let currentUserInstance = Auth.auth().currentUser
        }
    }

}
