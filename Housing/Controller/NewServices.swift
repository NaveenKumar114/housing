//
//  NewServices.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-05.
//

import UIKit
import CollapsibleTableSectionViewController
class NewServices: CollapsibleTableSectionViewController {
    var titleForSection = [String] ()
    var tableViewData = [[String]] ()
    var number = [[String]] ()
    var address = [[String]] ()
    let child = SpinnerViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        createSpinnerView()
        makePostCall()
        self.title = "Services"
        // Do any additional setup after loading the view.
    }
    
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_services")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(ServicesJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        titleForSection.removeAll()
                        tableViewData.removeAll()
                        number.removeAll()
                        address.removeAll()
                        removeSpinnerView()
                        if loginBaseResponse?.servicesList?.count == 0 || loginBaseResponse?.servicesList == nil
                                {
                                   let alert = UIAlertController(title: "Services", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                   alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                   self.present(alert, animated: true, completion: nil)
                                  }else
                               {
                                for n in 0 ... (loginBaseResponse?.servicesList!.count)! - 1
                                {
                                   // print(loginBaseResponse?.servicesList!.count)
                                    //titleForSection.append((loginBaseResponse?.faqList![n].question)!)
                                    if loginBaseResponse?.servicesList![n] != nil
                                    {
                                    titleForSection.append((loginBaseResponse?.servicesList![n].serviceName)!)
                                    var subService = [String] ()
                                    var subNumber = [String] ()
                                    var subAddress = [String] ()
                                    for m in 0 ... (loginBaseResponse?.servicesList![n].subService?.count)! - 1
                                    {
                                      //  print(loginBaseResponse?.servicesList![n].subService?.count)
                                        let name = (loginBaseResponse?.servicesList![n].subService![m].name)!
                                        let number = (loginBaseResponse?.servicesList![n].subService![m].contact)!
                                        let address = (loginBaseResponse?.servicesList![n].subService![m].address)!
                                        subService.append(name)
                                        subNumber.append(number)
                                        subAddress.append(address)
                                    }
                                    tableViewData.append(subService)
                                    number.append(subNumber)
                                    address.append(subAddress)
                                    print(tableViewData.count , number.count , address.count)
                                }
                                }
                               }
        
                        
                        self.reload()
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Service", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }

}

extension NewServices: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        return titleForSection.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData[section].count
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.servicesCellIdentifier) as! ServicesCell
        cell2.name.text = self.tableViewData[indexPath.section][indexPath.row]
        cell2.address.text = self.number[indexPath.section][indexPath.row]
        cell2.number.text = self.address[indexPath.section][indexPath.row]
      // cell2.numberButton.addTarget(self, action: #selector(makeCall), for: .touchUpInside)
        cell2.number.tag = indexPath.row
        cell2.selectionStyle = .none
        return cell2
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleForSection[section]
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return false
    }
   
    func collapsibleTableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let i = indexPath
        print(number[i.section][i.row])
        dialNumber(number: number[i.section][i.row])
        
    }
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
        let alert = UIAlertController(title: "Service", message: "Error making call", preferredStyle: UIAlertController.Style.alert)
        
        // add an action (button)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
        
        // show the alert
        self.present(alert, animated: true, completion: nil)
                // add error message here
       }
    }
}
