//
//  SignUpPassword.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-21.
//


import UIKit
import Alamofire
class SignUpPassword: UIViewController , UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    var userData : CheckUserJSON?
    
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var confirmPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        email.text = userData?.userLogindetails?.memberEmail ?? ""
        password.isSecureTextEntry = true
        confirmPassword.isSecureTextEntry = true
        email.delegate = self
        password.delegate = self
        confirmPassword.delegate = self
        email.font = UIFont(name: "Arial-Rounded", size: 15.0)
        password.font = UIFont(name: "Arial-Rounded", size: 15.0)

        confirmPassword.font = UIFont(name: "Arial-Rounded", size: 15.0)


    }

    @IBAction func confirmPressed(_ sender: Any) {
        if password.text == "" || confirmPassword.text == ""
        {
            let alert = UIAlertController(title: "SignUp", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if password.text != confirmPassword.text
        {
            let alert = UIAlertController(title: "SignUp", message: "Password and confirm password do not match", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if let safeData = userData?.userLogindetails
            {
                makePostCall(contact: safeData.memberContact!, email: safeData.memberEmail!, identificationType: safeData.memberIdentificationType!, identificationNumber: safeData.memberIndentificationNo!, name: safeData.memberName!)
            }
        }
    }
 

    func makePostCall(contact : String , email : String , identificationType : String , identificationNumber : String , name : String) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["member_contact":"\(contact)","member_email":"\(email)","member_identification_type":"\(identificationType)","member_indentification_no":"\(identificationNumber)","member_name":"\(name)","member_password":"\(password.text!)","member_role":"Family","member_status":"PENDING","user_role":"Family","verification_code":"","code":0,"create_by":"\(name)","modify_by":"\(name)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        print(json)
    //    makePostCallAddFavourites(to: URL(string: "\(ConstantsUsedInProject.baseUrl)check_usermembers")!, params: json)
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)check_usermembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                   
                   

                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(SignUPJson.self, from: data!)
                    print(loginBaseResponse)
                    let code_str = loginBaseResponse!.code
                    
                    DispatchQueue.main.async {
                        
                        if code_str != 204 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            print("success")
                            print(loginBaseResponse as Any)
                            let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if code_str == 204  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.performSegue(withIdentifier: segueIdentifiers.passwordToCode, sender: loginBaseResponse)
                                UserDefaults.standard.setValue(loginBaseResponse?.userLogindetails?.verificationCode!, forKey: "hcode")


                            }))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            

                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostCallAddFavourites(to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        multipart.append(data, withName: key)
                        print(key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                       
                        let decoder = JSONDecoder()


                        print(String(data: data, encoding: String.Encoding.utf8) as Any)

                        let loginBaseResponse = try? decoder.decode(CheckUserJSON.self, from: data)
                        print(loginBaseResponse)
                        let code_str = loginBaseResponse!.code
                        
                        DispatchQueue.main.async {
                            
                            if code_str != 204 {
                                
                                //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                                print("success")
                                print(loginBaseResponse as Any)
                                let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                
                            }else if code_str == 204  {
                                //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                                
                                let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                    self.performSegue(withIdentifier: segueIdentifiers.passwordToCode, sender: SignUp.loginData)
                                    UserDefaults.standard.setValue(loginBaseResponse?.userLogindetails?.verificationCode!, forKey: "hcode")


                                }))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                

                                
                                
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.passwordToCode
        {
            let data = SignUp.loginData
            let dest = segue.destination as! SignUpVerificationCode
            dest.userData = data
        dest.password = password.text!
        }
    }
    
}
