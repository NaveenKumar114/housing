//
//  FacilitiesPass.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-19.
//

import UIKit
import WebKit
import SnapshotKit
class FacilitiesPass: UIViewController {
    @IBOutlet weak var viewForBlur: UIView!
    @IBOutlet var viewForClose: UIView!
    @IBOutlet weak var passWebView: WKWebView!
    var passData : FacilitiesList?
    override func viewDidLoad() {
        super.viewDidLoad()

        if let safeData = passData
        {
            let housing = "\(UserDefaults.standard.string(forKey: userDefaultsKey.housingName)!)".uppercased()
            let passType = safeData.facilitiesName?.uppercased()
            let address = safeData.address
            
            let endDate = "\(convertDateFormater(safeData.facilitiesEnddate!)) \(safeData.facilitiesEndtime!)"
            let imageUrl = "\(ConstantsUsedInProject.baseImgUrl)facilities/qr_images/\(safeData.qrImage!)"
            passWebView.loadHTMLString("<div class=\"container-fluid printhide\" style=\"border:2px solid #315b69;width:100%;\">\n" +
                                        "    <div class=\"row\">\n" +
                                        "        <div class=\"\" style=\"width:100%;\">\n" +
                                        "                            <h1 class=\"font-weight-bold text-center font-size-35\" style=\"text-align:center;color:#315b69;font-size:100px\">\n" +
                                        "                                " + "\(housing)" + "\n" +
                                        "</h1>\n" +
                                        "                            <h5 class=\"font-weight-bold text-center font-size-35\" style=\"margin-top:-20px;text-align:center;color:#315b69;font-size:45px\">\n" +
                                        "                                " + "\(passType!)" + "\n" +
                                        "</h5>\n" +
                                        "            <div style=\"width:100%;\">\n" +
                                        "                <img style=\"height:150px;float:left;margin-left:5px;\" src=" + "\(imageUrl)" + " class=\"\" alt=\"avatar\">\n" +
                                        "                <img style=\"height:150px;float:right;margin-right:5px;\" src=" + "\(imageUrl)" + " class=\"\" alt=\"avatar\">\n" +
                                        "            </div>\n" +
                                        "            <div >\n" +
                                        "                <center>\n" +
                                        "                    <img style=\"height:570px;margin-top:150px;\" src=" + "\(imageUrl)" + "  class=\"\" alt=\"avatar\">\n" +
                                        "                </center>\n" +
                                        "            </div>\n" +
                                        "            <div style=\"width:100%;\">\n" +
                                        "                <img style=\"height:150px;float:left;margin-left:5px;\" src=" + "\(imageUrl)" + "  class=\"\" alt=\"avatar\">\n" +
                                        "                <img style=\"height:150px;float:right;margin-right:5px;\" src=" + "\(imageUrl)" + "  class=\"\" alt=\"avatar\">\n" +
                                        "            </div>\n" +
                                        "        </div>\n" +
                                        "        <center>\n" +
                                        "            <div style=\"width:100%;margin:10px;\">\n" +
                                        "                <p style=\"font-size: 40px;text-align:center;color:gold;margin-top: 250px;margin-right:15px;color:#315b69\">PLACE THE QR CODE A DISTANCE AWAY FROM THE READER</p>\n" +
                                        "                <p style=\" font-size: 40px;text-align:center;color:#315b69;margin:20px;\">" + "\(address!)" + "</p>\n" +
                                        "               <p style=\"font-size: 40px;text-align:center;color:gold;margin-right:15px;color:#315b69\"> QR Code expires by " + "\(endDate)" + "</p>\n" +
                                        "            </div>\n" +
                                        "        </center>\n" +
                                        "    </div>\n" +
                                        "</div>", baseURL: nil)
        }
        let gesture = UITapGestureRecognizer(target: self, action: #selector(dismissVC))
        viewForClose.addGestureRecognizer(gesture)
        let blurEffect = UIBlurEffect(style: .light)
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = viewForBlur.frame
        viewForBlur.addSubview(blurEffectView)
    }
    @objc func dismissVC()
    {
        self.dismiss(animated: true, completion: nil)

    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    @IBAction func shareButton(_ sender: Any) {
        self.passWebView.scrollView.asyncTakeSnapshotOfFullContent { (image) in
            self.viewForBlur.asyncTakeSnapshotOfFullContent { [self] (back) in
                self.combineImage(bottomImage: back! , topImage: image! , width: passWebView.frame.width , height: passWebView.frame.height)
                
            
            }
            
        }
    }
    func combineImage (bottomImage : UIImage , topImage : UIImage , width : CGFloat , height : CGFloat)
    {
        
           let newSize = CGSize(width: width, height: height)
        let newSize2 = CGSize(width: width + 80, height: height + 100)

           UIGraphicsBeginImageContextWithOptions(newSize2, false, 0.0)
           bottomImage.draw(in: CGRect(origin: CGPoint.zero, size: newSize2))
        let x : CGPoint = CGPoint(x: 40, y: 50)
           topImage.draw(in: CGRect(origin: x, size: newSize))
            
           let newImage = UIGraphicsGetImageFromCurrentImageContext()
           UIGraphicsEndImageContext()
        let imageToShare = [ newImage! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash

        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)
    }
}
