//
//  SignUpVerificationCode.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-21.
//

import UIKit
import FirebaseAuth

class SignUpVerificationCode: UIViewController , UITextFieldDelegate{
    var userData : CheckUserJSON?
    var password : String?
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var verificationCode: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        verificationCode.delegate = self
        verificationCode.font = UIFont(name: "Arial-Rounded", size: 15.0)

    }
    

    @IBAction func confirmPressed(_ sender: Any) {
        if verificationCode.text == ""
        {
            let alert = UIAlertController(title: "SignUp", message: "Please Enter Code", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            print("ss")
            print(userData?.userLogindetails)
         verifyOtp()
        }
        
    }
    func verifyOtp()
    {
        let verificati = verificationCode.text!
        let verificationID = UserDefaults.standard.string(forKey: "authVerificationID")
        let credential = PhoneAuthProvider.provider().credential(
            withVerificationID: verificationID!,
            verificationCode: verificati)
        Auth.auth().signIn(with: credential) { [self] (authResult, error) in
          if let error = error {
            let authError = error as NSError
            print(authError.description)
            let alert = UIAlertController(title: "Register", message: "\(error.localizedDescription)", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            return
          }

            print("Corrects")
          // User has signed in successfully and currentUser object is valid
            if let safeData = userData?.userLogindetails
            {
                print("ss")

                makePostCall(contact: safeData.memberContact!, email: safeData.memberEmail!, identificationType: safeData.memberIdentificationType!, identificationNumber: safeData.memberIndentificationNo!, name: safeData.memberName!)
            }        }
    }

    
    func makePostCall(contact : String , email : String , identificationType : String , identificationNumber : String , name : String) {
        let decoder = JSONDecoder()
        var v = UserDefaults.standard.string(forKey: "hcode")
        UserDefaults.standard.removeObject(forKey: "hcode")
        let json: [String: Any] = ["member_contact":"\(contact)","member_email":"\(email)","member_identification_type":"\(identificationType)","member_indentification_no":"\(identificationNumber)","member_name":"\(name)","member_password":"\(password!)","member_role":"Family","member_status":"PENDING","user_role":"Family","verification_code":"\(v!)","code":0,"create_by":"\(name)","modify_by":"\(name)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)check_usermembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(CheckUserJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            print("success")
                            print(loginBaseResponse as Any)
                            let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.navigationController?.popToRootViewController(animated: true)

                            }))
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))

                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)


                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}
