//
//  StatementOfAccount.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-08.
//

import UIKit

class StatementOfAccount: UIViewController {
    let child = SpinnerViewController()

    @IBOutlet weak var totalDueAmount: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var statementOfAccointTableView: UITableView!
    var addressList : AddressJSON?
    var ebillingData : EbillingListJSON?
    var totalAmount = 0.0
    var addressId : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        addressLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)

        let gesture = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressLabel.addGestureRecognizer(gesture)
        makePostCallAddress()
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            addressLabel.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            addressId = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            makePostCallStatement(addressID: addressId!)
            createSpinnerView()

        }
        statementOfAccointTableView.delegate = self
        statementOfAccointTableView.dataSource = self
        statementOfAccointTableView.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        statementOfAccointTableView.register(UINib(nibName: reuseNibIdentifier.StatementOfAccountCellNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.StatementOfAccountCellIdentifier)
        self.statementOfAccointTableView.tableFooterView = UIView()

    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }

    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressLabel.text = UIAlertAction.title
                        print(n)
                        addressId = listOfAllAddress[n].addressID!
                        makePostCallStatement(addressID: listOfAllAddress[n].addressID!)
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }

    func makePostCallStatement(addressID : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID)" , "status" : "Paid"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_billing_details")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(EbillingListJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                               print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                       // print(loginBaseResponse as Any)
                        self.totalAmount = 0
                        self.ebillingData = loginBaseResponse
                        self.statementOfAccointTableView.reloadData()
                        self.removeSpinnerView()
                        if loginBaseResponse?.billingDetailsList?.count == 0
                              {
                                 let alert = UIAlertController(title: "Statement of account", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                 alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                 self.present(alert, animated: true, completion: nil)
                                }
                        else{
                            totalAmount = 0
                            for n in 0 ... (loginBaseResponse?.billingDetailsList!.count)! - 1
                            {
                                 if let safeData = loginBaseResponse?.billingDetailsList?[n]
                                 {
                                    var a = Double(safeData.totalAmount ?? "0")
                                    if safeData.facilityID != "0"
                                    {
                                        a = Double(safeData.advanceAmount ?? "0")
                                    }
                                    
                                    totalAmount = totalAmount + (a ?? 0)
                                    totalDueAmount.text = "\(String(format: "%.2f", totalAmount)) MYR"
                                 }
                            }

                        }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    }



}

extension StatementOfAccount: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = statementOfAccointTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.StatementOfAccountCellIdentifier) as! StatementOfAccountCell
        if let safeData = ebillingData?.billingDetailsList?[indexPath.row]
        {

            cell.status.text = safeData.status?.uppercased()
         
            cell.dateAndTime.text = convertDateFormater(safeData.modifyDate!)
            //print(safeData)
            //cell.amount.text = "\(safeData.pendingAmount ?? "0") MYR"
            let number = Double(safeData.totalAmount ?? "0")
            print("\(String(format: "%.2f", number!)) MYR")
            cell.amount.text = "\(String(format: "%.2f", number!)) MYR"
            cell.name.text = safeData.billingName
            cell.paidBy.text = "Paid By : \(safeData.modifyBy!)"
            if safeData.status == "PENDING" || safeData.status == "Pending"
            {
                cell.status.textColor = .red
            }
            if safeData.facilityID != "0"
            {
                let number = Double(safeData.advanceAmount ?? "0")
                
                cell.amount.text = "\(String(format: "%.2f", number ?? "0")) MYR"
               
            }
            if safeData.attachmentImage != nil
            {
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/billing/attachments/\(safeData.attachmentImage!)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                    if   let image = UIImage(data: data!)
                    {
                       // cell.billAttachment.image = image
                        //cell.billAttachment.imageView?.image = image
                        cell.attachment.image = image
                        cell.attachment.backgroundColor = .white
                        cell.attachment.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)

                        

                    }
                    }
                }
            }
            //let a = Double(safeData.pendingAmount ?? "0")
            //totalAmount = totalAmount + (a ?? 0)
            //totalDueAmount.text = "\(String(format: "%.2f", totalAmount)) MYR"
        }
        return cell
    }
    
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy HH:mm:ss"
            return  dateFormatter.string(from: date!)

        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 140
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ebillingData?.billingDetailsList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let x = ebillingData?.billingDetailsList?[indexPath.row]
        statementOfAccointTableView.deselectRow(at: indexPath, animated: true)
        performSegue(withIdentifier: segueIdentifiers.statementToPDF, sender: x)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let data = sender as! BillingDetailsList
            let destinationVC = segue.destination as! ViewBillingPdf
        destinationVC.billingData = data
        
        
    }
}

extension String {

    init(withInt int: Int, leadingZeros: Int = 2) {
        self.init(format: "%0\(leadingZeros)d", int)
    }

    func leadingZeros(_ zeros: Int) -> String {
        if let int = Int(self) {
            return String(withInt: int, leadingZeros: zeros)
        }
        print("Warning: \(self) is not an Int")
        return ""
    }
    
}
