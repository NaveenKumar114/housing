//
//  AboutUs.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-17.
//

import UIKit

class AboutUs: UIViewController {

    @IBOutlet weak var forShadow: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        forShadow.dropShadow()
        // Do any additional setup after loading the view.
    }
    

    

}

extension UIView {
    func dropShadow(scale: Bool = true) {
        layer.masksToBounds = false
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOpacity = 0.2
        layer.shadowOffset = .zero
        layer.shadowRadius = 10
        layer.shouldRasterize = true
        layer.rasterizationScale = scale ? UIScreen.main.scale : 1
    }
}
