//
//  Address.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-19.
//

import UIKit

class Address: UIViewController {
    @IBOutlet weak var confirmButton: CurvedButton!
    @IBOutlet weak var addressCollectionView: UICollectionView!
    var addressData : AddressJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        makePostCall()
        addressCollectionView.dataSource = self
        addressCollectionView.delegate = self
        addressCollectionView.register(UINib(nibName: reuseCollectionNibIdentifier.addressNibIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCollectionIdentifier.addressCellIdentifier)
        addressCollectionView.collectionViewLayout = LeftAlignedFlowLayout()
    }
    
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        addressData = loginBaseResponse
                        addressCollectionView.reloadData()
                        if let safeData = loginBaseResponse?.addressList
                        {
                            if safeData.count > 1
                            {
                                
                               
                            }
                            else
                            if safeData.count == 1
                            {
                                self.confirmButton.isHidden = true

                            }
                        }

                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
   
    @IBAction func confirmButtonPressed(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
extension Address : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return addressData?.addressList?.count ?? 0
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = addressCollectionView.dequeueReusableCell(withReuseIdentifier: reuseCollectionIdentifier.addressCellIdentifier, for: indexPath) as! AddressCollectionCell
        cell.addressLabel.text = "\(addressData?.addressList?[indexPath.row].houseNo ?? ""),  \(addressData?.addressList?[indexPath.row].address ?? "")"
        cell.addressButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
        cell.addressButton.tag = indexPath.row
        let addressID = UserDefaults.standard.string(forKey: "addressID")
        if addressID! == addressData?.addressList?[indexPath.row].addressID!
        {
            cell.addressButton.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
            cell.addressButton.tintColor = .white
           // cell.addressImagr.image = image?.withTintColor(ConstantsUsedInProject.appThemeColor)
            cell.addressImagr.contentMode = .scaleToFill
            cell.addressButton.alpha = 0
            cell.addressImagr.alpha = 1
            cell.addressLabel.textColor = ConstantsUsedInProject.appThemeColor
            cell.addressImagr.tintColor = ConstantsUsedInProject.appThemeColor

        }
        if indexPath.row != 0
        {
            cell.layer.addBorder(edge: .left, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1), thickness: 0.5)

        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      //  return CGSize(width: view.frame.width / 3.3, height: 190)
        let approximateWidthOfContent = view.frame.width / 3.3
            // x is the width of the logo in the left

          let size = CGSize(width: approximateWidthOfContent, height: 1000)

          //1000 is the large arbitrary values which should be taken in case of very high amount of content
        var str = "a"
        if addressData != nil
        {
            for n in 0...addressData!.addressList!.count - 1
            {
                let adr = addressData?.addressList![n]
                    if n == 0
                    {
                        str = (adr?.address)!
                    }
                    else
                    {
                        if str.count < (adr?.address!.count)!
                        {
                            str = (adr?.address)!
                        }
                    }
            }
        }
        let attributes = [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 15)]
         let estimatedFrame = NSString(string: str).boundingRect(with: size, options: .usesLineFragmentOrigin, attributes: attributes, context: nil)
        return CGSize(width: view.frame.width / 3.3, height: estimatedFrame.height + 66)
        
    }
    
    @objc func addButtonPressed(sender : UIButton)
    {
        let x = sender.tag
        //print(x)
        for n in 0 ... addressCollectionView.numberOfItems(inSection: 0) - 1
        {
          //  print(n)
            let i = IndexPath(row: n, section: 0)
            if n == x
            {
                
                let cell = addressCollectionView.cellForItem(at: i) as! AddressCollectionCell
                cell.addressButton.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                cell.addressButton.tintColor = .white
                let addressId = addressData?.addressList?[n].addressID
                print(addressId!)
                let address = cell.addressLabel.text
                print(address!)
                UserDefaults.standard.setValue(addressId!, forKey: "addressID")
                UserDefaults.standard.setValue(address, forKey: "address")
                NotificationCenter.default.post(name: Notification.Name("navgation"), object: nil)
                let image = UIImage(named: "home")
                cell.addressButton.setImage(image?.withTintColor(ConstantsUsedInProject.appThemeColor), for: .normal)
                cell.addressImagr.image = image?.withTintColor(ConstantsUsedInProject.appThemeColor)
                cell.addressImagr.contentMode = .scaleToFill
                cell.addressButton.alpha = 0
                cell.addressImagr.alpha = 1
                cell.addressLabel.textColor = ConstantsUsedInProject.appThemeColor
                cell.addressImagr.tintColor = ConstantsUsedInProject.appThemeColor

            }
            else{
                let cell = addressCollectionView.cellForItem(at: i) as! AddressCollectionCell
                cell.addressButton.backgroundColor = .white
                let image = cell.addressButton.imageView?.image
                cell.addressButton.setImage(image?.withTintColor(.darkGray), for: .normal)
                cell.addressButton.tintColor = .darkGray
                
                cell.addressButton.alpha = 1
                cell.addressImagr.alpha = 0
                cell.addressLabel.textColor = .darkGray
            }
        }
    }
}

class LeftAlignedFlowLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let originalAttributes = super.layoutAttributesForElements(in: rect) else {
            return nil
        }
        var leftMargin: CGFloat = 0.0
        var lastY: Int = 0
        return originalAttributes.map {
            let changedAttribute = $0
            // Check if start of a new row.
            // Center Y should be equal for all items on the same row
            if Int(changedAttribute.center.y.rounded()) != lastY {
                leftMargin = sectionInset.left
            }
            changedAttribute.frame.origin.x = leftMargin
            lastY = Int(changedAttribute.center.y.rounded())
            leftMargin += changedAttribute.frame.width + minimumInteritemSpacing
            return changedAttribute
        }
    }
}
