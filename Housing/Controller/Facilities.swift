//
//  Facilities.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-04.
//

import UIKit
import DatePickerDialog

class Facilities: UIViewController  {
    @IBOutlet weak var address: UILabel!
    let expiryDatePicker = MonthYearPickerView()
    let toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 50))

    @IBOutlet weak var bookButtonVIew: UIView!
    @IBOutlet weak var calenderIcon: UIImageView!
    @IBOutlet weak var pendingWarningLabel: UILabel!
    @IBOutlet weak var facilitiesscrollView: UIScrollView!
    @IBOutlet weak var addressHistory: UILabel!
    @IBOutlet weak var historySegment: UISegmentedControl!
    var refreshControl = UIRefreshControl()
    @IBOutlet weak var historyStatusView: UIView!
    var selectedIndex : IndexPath?
    @IBOutlet weak var segmentControlk: UISegmentedControl!
    @IBOutlet weak var addressHistoryView: CurvedView!
    @IBOutlet weak var bookingFee: UILabel!
    @IBOutlet weak var deposit: UILabel!
    var facilitiesData : FacilitiesJSON?
    @IBOutlet weak var sixToElevenAm: CurvedButtonNoBorder!
    @IBOutlet weak var bookingDate: UILabel!
    @IBOutlet weak var sevenToTenPm: CurvedButtonNoBorder!
    @IBOutlet weak var threeTOSevenPm: CurvedButtonNoBorder!
    @IBOutlet weak var elevenToThreePm: CurvedButtonNoBorder!
    var timeButtons = [UIButton]()
    @IBOutlet weak var facilitiesCollectionView: UICollectionView!
    var addressID : String?
    @IBOutlet weak var historyTableView: UITableView!
    var addressList : AddressJSON?
    var facilityHistoryData : FacilitiesHistory?
    var selectedFacility : Int?
    let child = SpinnerViewController()
    @IBOutlet weak var confirmButton: CurvedButton!
    var addressIDHistory : String?
    var currentSelected : String?
    var startTimeStr : String?
    var endTimeStr : String?
    var facilitySlotData : CheckFacilitySlot?
    var historyStatusSelected : String?
    var startDate : String?
    var endDate : String?
    var selectedTimes = [false , false , false , false]
    var historyDate : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        historyTableView.separatorStyle = .singleLine
        segmentControlk.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControlk.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        historySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        historySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        facilitiesCollectionView.delegate = self
        facilitiesCollectionView.dataSource = self
        historyTableView.dataSource = self
        historyTableView.delegate = self
        facilitiesCollectionView.register(UINib(nibName: reuseCollectionNibIdentifier.addressNibIdentifier, bundle: nil), forCellWithReuseIdentifier: reuseCollectionIdentifier.addressCellIdentifier)
        historyTableView.register(UINib(nibName: reuseNibIdentifier.facilityHistoryNibIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.facilityHistoryCellIdentifier)
        makePostCall()
        
        createSpinnerView()
        timeButtons = [sixToElevenAm , elevenToThreePm , threeTOSevenPm , sevenToTenPm]
        makePostCallAddress()
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            address.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            addressID = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            addressIDHistory = addressID
            //makePostCallVisitorHistory(currentAddressID: addressIDHistory!)
            addressHistory.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
        }
        let gestureAddressSeason = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        address.addGestureRecognizer(gestureAddressSeason)
        bookingDate.isUserInteractionEnabled = true
        let gesture7 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
     bookingDate.addGestureRecognizer(gesture7)
     
        let gestureAddresshistory = UITapGestureRecognizer(target: self, action: #selector(showAddressHistory))
        addressHistory.addGestureRecognizer(gestureAddresshistory)
        address.font = UIFont(name: "Arial-Rounded", size: 15.0)
        addressHistory.font = UIFont(name: "Arial-Rounded", size: 15.0)
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
                   refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
                 historyTableView.addSubview(refreshControl)
        historySegment.apportionsSegmentWidthsByContent = true
        let gestureCalendar = UITapGestureRecognizer(target: self, action: #selector(dateAndYearPicker))
        calenderIcon.addGestureRecognizer(gestureCalendar)
        let month = Calendar.current.component(.month, from: Date())
        let year = Calendar.current.component(.year, from: Date())
        var m = "\(month)"
        if m.count == 1
        {
            print("1")
            m = "0\(month)"
        }
        historyDate = "\(year)-\(m)"
    }
 
    @objc func dateAndYearPicker()
    {
        let month = Calendar.current.component(.month, from: Date())
        let year = Calendar.current.component(.year, from: Date())
        var m = "\(month)"
        if m.count == 1
        {
            print("1")
            m = "0\(month)"
        }
        historyDate = "\(year)-\(m)"
        expiryDatePicker.backgroundColor = UIColor.white
           expiryDatePicker.setValue(UIColor.black, forKey: "textColor")
           expiryDatePicker.autoresizingMask = .flexibleWidth
           expiryDatePicker.contentMode = .center
           expiryDatePicker.frame = CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 300, width: UIScreen.main.bounds.size.width, height: 300)
           self.view.addSubview(expiryDatePicker)

           toolBar.barStyle = .default
           toolBar.isTranslucent = true
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.action))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: nil)

           toolBar.setItems([spaceButton, doneButton], animated: false)
           toolBar.isUserInteractionEnabled = true
           self.view.addSubview(toolBar)

           //DISABLE RIGHT ITEM & LEFT ITEM
          //        disableCancelAndSaveItems()

           //DISABLED SELECTION FOR ALL CELLS
          //        ableToSelectCellsAndButtons(isAble: false)

           //DISABLE RIGHT ITEM & LEFT ITEM
          // isEnableCancelAndSaveItems(isEnabled: false)

           //SHOW GREY BACK GROUND
          // showGreyOutView(isShow: true)
           expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
              // self.expiredDetailOutlet.text = string
            var m = "\(month)"
            if m.count == 1
            {
                print("1")
                m = "0\(month)"
            }
            self.historyDate = "\(year)-\(m)"

               
           }
        
    }
    
    @objc func action() {
        expiryDatePicker.removeFromSuperview()
        toolBar.removeFromSuperview()
        view.endEditing(true)
        makePostCallVisitorHistory(currentAddressID: addressID!, status: historyStatusSelected!, date: historyDate!)
    }
    @objc func refresh(_ sender: AnyObject) {
          print("refresh")
        if historyStatusSelected != nil
        {
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }
      }
/*    @objc func showStaus()
    {
        let alert = UIAlertController(title: "Choose status", message: nil, preferredStyle: .actionSheet)
        alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        alert.addAction(UIAlertAction(title: "APPROVED" , style: .default , handler:{ [self] (UIAlertAction)in
            historyStatusLabel.text = "APPROVED"
            historyStatusSelected = "APPROVED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }))
        alert.addAction(UIAlertAction(title: "PENDING" , style: .default , handler:{ [self] (UIAlertAction)in
            historyStatusLabel.text = "PENDING"
            historyStatusSelected = "PENDING"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }))
        alert.addAction(UIAlertAction(title: "REJECTED" , style: .default , handler:{ [self] (UIAlertAction)in
            historyStatusLabel.text = "REJECTED"
            historyStatusSelected = "REJECTED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }))
        alert.addAction(UIAlertAction(title: "EXPIRED" , style: .default , handler:{ [self] (UIAlertAction)in
            historyStatusLabel.text = "EXPIRED"
            historyStatusSelected = "EXPIRED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }))
        alert.addAction(UIAlertAction(title: "CANCELLED" , style: .default , handler:{ [self] (UIAlertAction)in
            historyStatusLabel.text = "CANCELLED"
            historyStatusSelected = "CANCELLED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }))
        alert.addAction(UIAlertAction(title: "ALL" , style: .default , handler:{ [self] (UIAlertAction)in
            historyStatusLabel.text = "ALL"
            historyStatusSelected = "ALL"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
            print("User click Dismiss button")
        }))
        self.present(alert, animated: true, completion: {
            print("completion block")
        })
    } */
    @objc func showAddressHistory()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressHistory.text = UIAlertAction.title
                        self.addressIDHistory = listOfAllAddress[n].addressID!
                        //print(listOfAllAddress[n])
                       // print(addressID)
                      //  print(n)
                        if historyStatusSelected != nil
                        {
                            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
                        }
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case bookingDate:
            datePickerTapped(sender: x!)
       
        default:
            print("error in time date")
        }
        
    }
    @IBAction func segmentChange(_ sender: UISegmentedControl) {
        pendingWarningLabel.isHidden = true
        if sender.selectedSegmentIndex == 0
        {
            facilitiesCollectionView.isHidden = false
            historyTableView.isHidden = true
            addressHistoryView.isHidden = true
            historySegment.isHidden = true
            historyStatusView.isHidden = true
            bookButtonVIew.isHidden = false
        }
        else
        {
            bookButtonVIew.isHidden = true
            facilitiesCollectionView.isHidden = true
            historyTableView.isHidden = false
            facilitiesscrollView.isHidden = true
            addressHistoryView.isHidden = false
            historySegment.isHidden = false
            historyStatusView.isHidden = false
            if historyStatusSelected == nil
            {
                historyStatusSelected = "ALL"
                makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
            }
            if historySegment.selectedSegmentIndex == 1
            {
                pendingWarningLabel.isHidden = false
            }
        }
    }
    func datePickerTapped(sender : UILabel) {
        let today = Date()
        let modifiedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
        DatePickerDialog().show("Start Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: modifiedDate , minimumDate:modifiedDate, maximumDate: nil, datePickerMode: .date) { [self] (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                makePostCallCheckFacilitySlot(date: convertDateFormater(formatter.string(from: dt)))
                // print(formatter.string(from: dt))
                datePickerTappedTodate(sender: sender , fromdate: dt)
                startDate = formatter.string(from: dt)
                for n in 0 ... selectedTimes.count - 1
                {
                    selectedTimes[n] = false
                }
            }
        }
    }
    func datePickerTappedTodate(sender : UILabel , fromdate : Date) {
        let today = Date()
        let modifiedDate = Calendar.current.date(byAdding: .day, value: 1, to: today)!
        DatePickerDialog().show("To Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: fromdate , minimumDate:fromdate, maximumDate: nil, datePickerMode: .date) { [self] (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
               // makePostCallCheckFacilitySlot(date: convertDateFormater(formatter.string(from: dt)))
                // print(formatter.string(from: dt))
                endDate = formatter.string(from: dt)
                sender.text = "\(startDate!) - \(endDate!)"
                for n in 0 ... selectedTimes.count - 1
                {
                    selectedTimes[n] = false
                }
            }
        }
        
    }
    func convertDateFormater(_ date: String) -> String
        {
            print(date)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return  dateFormatter.string(from: date!)

        }
    func timePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Time", doneButtonTitle: "done", cancelButtonTitle: "cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .time) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                //print(formatter.string(from: dt))
                sender.text = formatter.string(from: dt)
                
            }
        }
        
    }
    @IBAction func timeButtonPressed(_ sender: Any) {
        let button = sender as! UIButton
        for n in timeButtons
        {
            //checkSlot()
            if n == button
            {
                n.backgroundColor = ConstantsUsedInProject.appThemeColor
                switch n {
                case sixToElevenAm:
                        selectedTimes[0] = !selectedTimes[0]
                        if !selectedTimes[0]
                        {
                            n.backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                        }
                        startTimeStr = "06:00:00"
                        endTimeStr = "11:00:00"
                case elevenToThreePm:
                    selectedTimes[1] = !selectedTimes[1]
                    if !selectedTimes[1]
                    {
                        n.backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                    }
                        startTimeStr = "11:00:00"
                        endTimeStr = "15:00:00"
                case threeTOSevenPm:
                    selectedTimes[2] = !selectedTimes[2]
                    if !selectedTimes[2]
                    {
                        n.backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                    }
                        startTimeStr = "15:00:00"
                        endTimeStr = "19:00:00"
                case sevenToTenPm:
                    selectedTimes[3] = !selectedTimes[3]
                    if !selectedTimes[3]
                    {
                        n.backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                    }
                        startTimeStr = "19:00:00"
                        endTimeStr = "22:00:00"
               
                
                default:
                    print("error")
                }
            }
            else
            {
               // n.backgroundColor = #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
          
            }
            
            
        }
        print(selectedTimes)
    }
    
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
               // UILabel.appearance(whenContainedInInstancesOf: [UIAlertController.self]).numberOfLines = 0

                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.address.text = UIAlertAction.title
                        self.addressID = listOfAllAddress[n].addressID!
                        //print(listOfAllAddress[n])
                       // print(addressID)
                      //  print(n)
                    }))
                }
            
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @IBAction func confirmButtonPressed(_ sender: Any) {
        var item = 9
        for n in 0 ... selectedTimes.count - 1 {
            
            if selectedTimes[n] == true
            {
                item = n
                break
            }
        }
        if address.text == "" || startTimeStr == nil || address.text == "" || addressID == nil || endTimeStr == nil || bookingDate.text == "Booking Date *" || item == 9 || endDate == nil || startDate == nil
        {
            let alert = UIAlertController(title: "Facilities", message: "Please enter all details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            var times = [Int]()
            for n in 0 ... selectedTimes.count - 1 {
                if selectedTimes[n] == true
                {
                    times.append(n)
                }
            }
            print(times)
            prepareForAdding(item: times , current: 0)
        }
    }
    
    func prepareForAdding(item : [Int] , current : Int)
    {
        var jsonArray = [[String: Any]]()
        for i in 0 ... item.count - 1
        {
        switch item[i] {
        case 0:
                startTimeStr = "06:00:00"
                endTimeStr = "11:00:00"
        case 1:
                startTimeStr = "11:00:00"
                endTimeStr = "15:00:00"
        case 2:
                startTimeStr = "15:00:00"
                endTimeStr = "19:00:00"
        case 3:
                startTimeStr = "19:00:00"
                endTimeStr = "22:00:00"
        default:
            print("error")
        }
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let mbrID = defaults.string(forKey: userDefaultsKey.userMemberID)
   
        let name = defaults.string(forKey: userDefaultsKey.name)
        let snippet = address.text!
        let range = snippet.range(of: ",")
        let addressWithoutNumber = snippet[range!.upperBound...].trimmingCharacters(in: .whitespaces)
        let facilitySelected = (facilitiesData?.facilitiesCategoryList?[selectedFacility!])!

        let decoder = JSONDecoder()
        let json: [String: Any] = ["address":"\(address.text!)","address_id":"\(addressID!)","facilities_enddate":"\(convertDateFormater(endDate!))","facilities_endtime":"\(endTimeStr!)","facilities_name":"\(name!)","facilities_startdate":"\(convertDateFormater(startDate!))","facilities_starttime":"\(startTimeStr!)","facilities_type":"\(facilitySelected.facilityName!)","status":"PENDING","subscriber_id":"\(sub!)","userid":"\(usr!)","code":0,"create_by":"\(name!)","modify_by":"\(name!)" , "facility_category_id" : "\(facilitySelected.id!)" , "user_memberid" : "\(mbrID!)"]
            jsonArray.append(json)
    }
      //  print(jsonArray)
      
        makePostCallAddFacility(item: item, current: current , jsonArray: jsonArray)
    }
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)get_facilities_category")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(FacilitiesJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                              //     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.facilitiesData = loginBaseResponse
                            self.facilitiesCollectionView.reloadData()
                            self.removeSpinnerView()
                                    if loginBaseResponse?.facilitiesCategoryList?.count == 0
                                     {
                                        let alert = UIAlertController(title: "Facilities", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                        self.present(alert, animated: true, completion: nil)
                                       }
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Facilities", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    @IBAction func facilityHistorySegment(_ sender: Any) {
        pendingWarningLabel.isHidden = true
        let x = sender as! UISegmentedControl
        switch x.selectedSegmentIndex {
        case 0:
            print("ALL")
            historyStatusSelected = "ALL"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        case 1:
            print("pending")
            pendingWarningLabel.isHidden = false
            historyStatusSelected = "PENDING"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        case 2:
            print("approve")
            historyStatusSelected = "APPROVED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        case 3:
            print("reject")
            historyStatusSelected = "REJECTCED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        case 4:
            print("cancel")
            historyStatusSelected = "CANCELLED"
            makePostCallVisitorHistory(currentAddressID: addressIDHistory!, status: historyStatusSelected! )
        default:
            print("errro")
        }
    }
    func makePostCallVisitorHistory(currentAddressID : String , status : String , date: String) {
        
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(currentAddressID)" , "status" : "\(status)" , "monthyear" : "\(date)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)get_facilitieslist")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(FacilitiesHistory.self, from: data!)
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    if   let code_str = loginBaseResponse?.code
                 {
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.facilityHistoryData = loginBaseResponse
                            self.historyTableView.reloadData()
                            self.refreshControl.endRefreshing()
                            print(loginBaseResponse?.facilitiesList?.count)
                            if loginBaseResponse?.facilitiesList?.count == 0
                                      {
                                         let alert = UIAlertController(title: "Facility History", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                         alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                         self.present(alert, animated: true, completion: nil)
                                        }


                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Facilities", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                 }
                }
            }
            task.resume()
        }
    }
    func makePostCallVisitorHistory(currentAddressID : String , status : String) {
        
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(currentAddressID)" , "status" : "\(status)" , "monthyear": "\(historyDate!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)get_facilitieslist")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(FacilitiesHistory.self, from: data!)
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    if   let code_str = loginBaseResponse?.code
                 {
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            print(loginBaseResponse as Any)
                            self.facilityHistoryData = loginBaseResponse
                            self.historyTableView.reloadData()
                            self.refreshControl.endRefreshing()
                            print(loginBaseResponse?.facilitiesList?.count)
                            if loginBaseResponse?.facilitiesList?.count == 0
                                      {
                                         let alert = UIAlertController(title: "Facility History", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                         alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                         self.present(alert, animated: true, completion: nil)
                                        }


                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Facilities", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                 }
                }
            }
            task.resume()
        }
    }
    func makePostCallCheckFacilitySlot(date : String) {
        let facilitySelected = (facilitiesData?.facilitiesCategoryList?[selectedFacility!])!

        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID!)" , "user_memberid" : "\(defaults.string(forKey: userDefaultsKey.userMemberID)!)" , "facilities_startdate" : "\(date)" , "facility_category_id" : "\(facilitySelected.id!)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)get_slot_available")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(CheckFacilitySlot.self, from: data!)
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    if   let code_str = loginBaseResponse?.code
                 {
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                                   print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            // print("success")
                            //print(loginBaseResponse as Any)
                            self.facilitySlotData = loginBaseResponse
                            self.checkSlot()
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Facilities", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                 }
                }
            }
            task.resume()
        }
    }
    func makePostCallAddFacility(item : [Int] , current : Int , jsonArray : [[String: Any]]) {
       

        let decoder = JSONDecoder()
 
        
        let jsonData = try? JSONSerialization.data(withJSONObject: jsonArray)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)add_facilities_array")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)

                    let loginBaseResponse = try? decoder.decode(AddFacilities.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            if true
                            {
                                let alert = UIAlertController(title: "Facilities", message: "\(loginBaseResponse?.response ?? "Successfully Booked")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.refreshTextfields()
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            else
                            {
                                self.prepareForAdding(item: item, current: current + 1)
                            }
                           
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Facilities", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    func refreshTextfields()
    {
        sixToElevenAm.setTitle("6AM - 11AM", for: .normal)
        elevenToThreePm.setTitle("11AM - 3PM", for: .normal)
        threeTOSevenPm.setTitle("3PM - 7PM", for: .normal)
        sevenToTenPm.setTitle("7PM - 10PM", for: .normal)
        sixToElevenAm.setImage(nil, for: .normal)
        elevenToThreePm.setImage(nil, for: .normal)
        threeTOSevenPm.setImage(nil, for: .normal)
        sevenToTenPm.setImage(nil, for: .normal)
       startTimeStr = nil
        endTimeStr = nil
        bookingDate.text = "Booking Date *"
        confirmButton.isHidden = false
        for n in timeButtons
        {
            n.backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
        }
    }
   func checkSlot()
   {
    sixToElevenAm.setTitle("6AM - 11AM", for: .normal)
    elevenToThreePm.setTitle("11AM - 3PM", for: .normal)
    threeTOSevenPm.setTitle("3PM - 7PM", for: .normal)
    sevenToTenPm.setTitle("7PM - 10PM", for: .normal)
    sixToElevenAm.setImage(nil, for: .normal)
    elevenToThreePm.setImage(nil, for: .normal)
    threeTOSevenPm.setImage(nil, for: .normal)
    sevenToTenPm.setImage(nil, for: .normal)
    confirmButton.isHidden = false
    var check = [false , false , false , false]
        for n in timeButtons
        {
            n.backgroundColor = #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
            n.isUserInteractionEnabled = true
            n.alpha = 1
        }
        if let safeData = facilitySlotData
        {
            if facilitySlotData?.facilities != nil{
            for n in 0 ... 3
            {
                var slot = ""
                switch n {
                case 0:
                    slot = safeData.facilities?.slot1 ?? ""
                case 1:
                    slot = safeData.facilities?.slot2 ?? ""
                case 2:
                    slot = safeData.facilities?.slot3 ?? ""
                case 3:
                    slot = safeData.facilities?.slot4 ?? ""
                default:
                    break
                }
                let facilitySelected = (facilitiesData?.facilitiesCategoryList?[selectedFacility!])!
                if slot == facilitySelected.capacity
                {
                    timeButtons[n].backgroundColor = #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                    timeButtons[n].setImage(UIImage(named: "checkmark")?.withTintColor(.white), for: .normal)
                    timeButtons[n].setTitle("", for: .normal)
                    timeButtons[n].isUserInteractionEnabled = false
                    check[n] = true
                }
                else
                {
                    timeButtons[n].backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                    timeButtons[n].isUserInteractionEnabled = true
                }
               /* switch slot {
                case "1" , "0":
                    timeButtons[n].backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)

                    timeButtons[n].isUserInteractionEnabled = true
                case "2" , "3":
                    timeButtons[n].backgroundColor = .systemOrange
                    timeButtons[n].isUserInteractionEnabled = true
                case "4":
                    timeButtons[n].backgroundColor = .systemRed
                    timeButtons[n].isUserInteractionEnabled = false
                    check[n] = true
                default:
                    print("error")
                } */

            }
            for n in 0 ... 3
            {
                var slot = ""
                switch n {
                case 0:
                    slot = safeData.facilities?.uslot1 ?? ""
                case 1:
                    slot = safeData.facilities?.uslot2 ?? ""
                case 2:
                    slot = safeData.facilities?.uslot3 ?? ""
                case 3:
                    slot = safeData.facilities?.uslot4 ?? ""
                default:
                    break
                }
                
                switch slot {
                case "1":
                    timeButtons[n].backgroundColor = #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
                    timeButtons[n].setTitle("", for: .normal)
                    timeButtons[n].setImage(UIImage(named: "checkmark")?.withTintColor(.white), for: .normal)

                    timeButtons[n].isUserInteractionEnabled = false
                    check[n] = true
                   
                case "0":
                    print("0")
                    //timeButtons[n].backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)

                    //timeButtons[n].isUserInteractionEnabled = true
                   
                default:
                    print("ss")
                    
                }

            }
                for n in 0 ... 3
                {
                    var slot = ""
                    switch n {
                    case 0:
                        slot = safeData.facilities?.aslot1 ?? ""
                    case 1:
                        slot = safeData.facilities?.aslot2 ?? ""
                    case 2:
                        slot = safeData.facilities?.aslot3 ?? ""
                    case 3:
                        slot = safeData.facilities?.aslot4 ?? ""
                    default:
                        break
                    }
                    
                    switch slot {
                    case "1":
                        let i  = IndexPath(row: selectedFacility!, section: 0)
                        let cell = facilitiesCollectionView.cellForItem(at: i) as!AddressCollectionCell
                        let image = cell.addressButton.image(for: .normal)
                      //  timeButtons[n].contentMode = .scaleAspectFit

                        
                        let x = facilitiesData?.facilitiesCategoryList?[i.row]

                        if x?.icon != nil
                        {
                            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)facilities_category/\(x!.icon!)")
                            print(urlStr)
                            let url = URL(string: urlStr)
                            
                            DispatchQueue.global().async {
                                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                                DispatchQueue.main.async { [self] in
                                    let image = UIImage(data: data!)
                                    
                                    timeButtons[n].setImage(image?.withTintColor(.white), for: .normal)
                                    timeButtons[n].setTitle("", for: .normal)
                                    timeButtons[n].backgroundColor = .systemRed
                                    timeButtons[n].isUserInteractionEnabled = false
                                    timeButtons[n].alpha = 1

                                }
                            }
                        }
                        check[n] = true
                       
                    case "0":
                        print("0")
                        //timeButtons[n].backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)

                        //timeButtons[n].isUserInteractionEnabled = true
                       
                    default:
                        print("ss")
                        
                    }

                }

                var count = 0
                for n in 0 ... check.count - 1
                {
                    if check[n] == true
                    {
                        count = count + 1
                    }
                }
                if count == check.count
                {
                    confirmButton.isHidden = true
                }
            }
        }
   }
}

extension Facilities : UICollectionViewDataSource , UICollectionViewDelegate , UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return facilitiesData?.facilitiesCategoryList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = facilitiesCollectionView.dequeueReusableCell(withReuseIdentifier: reuseCollectionIdentifier.addressCellIdentifier, for: indexPath) as! AddressCollectionCell
        let x = facilitiesData?.facilitiesCategoryList?[indexPath.row]
        cell.addressLabel.text = x?.facilityName!
        cell.addressLabel.textColor = .darkGray
       cell.addressButton.addTarget(self, action: #selector(addButtonPressed), for: .touchUpInside)
        cell.addressButton.tag = indexPath.row
        print(x?.facilityName)
        cell.addressImagr.alpha = 0
        cell.addressButton.alpha = 1

        if x?.icon != nil
        {
            let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)facilities_category/\(x!.icon!)")
            print(urlStr)
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                    let image = UIImage(data: data!)
                    
                    cell.addressButton.setImage(image?.withTintColor(.darkGray), for: .normal)
                    cell.addressImagr.image = image
                    cell.addressImagr.alpha = 0
                    if selectedIndex != nil
                    {
                    if selectedIndex == indexPath
                    {
                        cell.addressImagr.alpha = 1
                        cell.addressButton.alpha = 0
                        cell.addressImagr.image = image?.withTintColor(ConstantsUsedInProject.appThemeColor)
                        cell.addressLabel.textColor = ConstantsUsedInProject.appThemeColor

                    }
                    else
                    {
                        cell.addressImagr.alpha = 0
                        cell.addressButton.alpha = 1
                    }
                    }

                }
            }
        }
        if indexPath.row != 0
        {
            cell.layer.addBorder(edge: .left, color: #colorLiteral(red: 0.7843137255, green: 0.7843137255, blue: 0.7843137255, alpha: 1), thickness: 0.5)

        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      
        return CGSize(width: 70, height: 120)
        
    }
    @objc func addButtonPressed(sender : UIButton)
    {
       // refreshTextfields()
        print("add")
        facilitiesscrollView.isHidden = false
        let x = sender.tag
        //print(x)
        for n in 0 ... facilitiesCollectionView.numberOfItems(inSection: 0) - 1
        {
            print(facilitiesCollectionView.numberOfItems(inSection: 0))
          //  print(n)
            let i = IndexPath(row: n, section: 0)
            print(i)
            if n == x
            {
                selectedIndex = i
                let cell = facilitiesCollectionView.cellForItem(at: i) as! AddressCollectionCell
               // cell.addressButton.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                let image = cell.addressButton.imageView?.image
                cell.addressButton.setImage(image?.withTintColor(ConstantsUsedInProject.appThemeColor), for: .normal)
                cell.addressButton.tintColor = .white
                let d = facilitiesData?.facilitiesCategoryList?[n].advanceAmount
                var number = Double(d ?? "0")
                
                deposit.text = "\(String(format: "%.2f", number!)) MYR"
                let b = facilitiesData?.facilitiesCategoryList?[n].bookingAmount ?? ""
                number = Double(b ?? "0")
                bookingFee.text = "\(String(format: "%.2f", number!)) MYR"
                selectedFacility = n
              
               // cell.addressLabel.frame = CGRect(x: x, y: y, width: w, height: h)
                cell.addressImagr.image = image?.withTintColor(ConstantsUsedInProject.appThemeColor)
                cell.addressImagr.contentMode = .scaleToFill
                cell.addressButton.alpha = 0
                cell.addressImagr.alpha = 1
                cell.addressLabel.textColor = ConstantsUsedInProject.appThemeColor
                
             //  facilitiesCollectionView.performBatchUpdates(nil, completion: nil)
            }
            else{
              if  let cell = facilitiesCollectionView.cellForItem(at: i) as? AddressCollectionCell
              {
                cell.addressButton.backgroundColor = .white
                let image = cell.addressButton.imageView?.image
                cell.addressButton.setImage(image?.withTintColor(.darkGray), for: .normal)
                cell.addressButton.tintColor = .darkGray
                
                cell.addressButton.alpha = 1
                cell.addressImagr.alpha = 0
                cell.addressLabel.textColor = .darkGray
              }
            }
        }
        startTimeStr = nil
         endTimeStr = nil
         //bookingDate.text = "Booking Date *"
         confirmButton.isHidden = false
         for n in timeButtons
         {
             n.backgroundColor =  #colorLiteral(red: 0.3221354783, green: 0.627459228, blue: 0.06978648156, alpha: 1)
         }
        if bookingDate.text != "Booking Date *"
        {
            
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
            makePostCallCheckFacilitySlot(date: convertDateFormater(startDate!))
                // print(formatter.string(from: dt))
                
                for n in 0 ... selectedTimes.count - 1
                {
                    selectedTimes[n] = false
                }
                
        }
    }
}

extension Facilities : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return facilityHistoryData?.facilitiesList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.facilityHistoryCellIdentifier) as! FacilityHistoryCell
        if let x = facilityHistoryData?.facilitiesList?[indexPath.row]
        {
            print(x)
            cell.dateAndTime.text = "\(reverseConvertDateFormater(x.facilitiesStartdate!)) - (\(convertTimeFormater(x.facilitiesStarttime!)) - \(convertTimeFormater(x.facilitiesEndtime!)))"
            
            cell.active.text = x.status
            if x.status == "APPROVED"
            {
                cell.active.textColor = .systemGreen
            }
            else
            {
                cell.active.textColor = .systemRed
            }
            cell.facilityName.text = x.facilitiesType
        }
        return cell
    }
    func convertTimeFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "HH:mm:ss"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "h:mm a"
        if date != nil
        {
            return  dateFormatter.string(from: date!)

        }
    else
        {
            return ""
        }

        }
    func reverseConvertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            if date != nil
            {
                return  dateFormatter.string(from: date!)

            }
        else
            {
                return ""
            }

        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       openPass(index: indexPath)
    }

    @objc func openPass(index : IndexPath)
    {
        let storyboard = UIStoryboard(name: "FacilitiesPass", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "facilitiesPass") as! FacilitiesPass
        myAlert.passData = facilityHistoryData?.facilitiesList?[index.row]
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
}
