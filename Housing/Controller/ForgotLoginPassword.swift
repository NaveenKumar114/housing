//
//  ForgotLoginPassword.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-11.
//

import UIKit

class ForgotLoginPassword: UIViewController, UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var confirmPassword: UITextField!
    @IBOutlet weak var hcodeLabel: UITextField!
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var profileImage: UIImageView!
    @IBOutlet weak var password: UITextField!
    var hcode : String?
    var userDetails : ChangePasswordKcodeJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard

        confirmPassword.delegate = self
        password.font = UIFont(name: "Arial-Rounded", size: 15.0)
        confirmPassword.font = UIFont(name: "Arial-Rounded", size: 15.0)
        password.delegate = self
        confirmPassword.isSecureTextEntry = true
        password.isSecureTextEntry = true
     

    }
    

    @IBAction func confirmPressed(_ sender: Any) {
        if hcode == nil || confirmPassword.text == "" || password.text == "" || hcodeLabel.text == "" || userDetails == nil
        {
            let alert = UIAlertController(title: "Reset Password", message: "Please Enter Details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if hcode != hcodeLabel.text {
            let alert = UIAlertController(title: "Reset Password", message: "Hcode doesnt match", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        if confirmPassword.text != password.text {
            let alert = UIAlertController(title: "Reset Password", message: "Password and confirm password doesnt match", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            makePostCall()
        }
        
    }
    
    @IBAction func backPressed(_ sender: Any) {
        navigationController?.popViewController(animated: true)

    }
    func makePostCall() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)updateUsersPassword")! as URL)
        request.httpMethod = "POST"
        let postString = "member_password=\(password.text!)&user_memberid=\(userDetails!.userLogindetails!.userMemberid!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(ChangePasswordKcodeJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        let alert = UIAlertController(title: "Reset Password", message: "\(loginBaseResponse?.response ?? "Password Changed")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                            self.navigationController?.popToRootViewController(animated: true)
                        }))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Reset Password", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
            }
        }
        task.resume()
    }
}
