//
//  ContactUS.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-17.
//

import UIKit

class ContactUS: UIViewController {
    @IBOutlet weak var dataTabelView: UITableView!
    @IBOutlet weak var addressImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressImageView.layer.cornerRadius = 10
        dataTabelView.dataSource = self
        dataTabelView.delegate = self
    }
    

}
extension ContactUS : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 6
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        var heading = ""

            switch indexPath.section {
            case 0:
                heading = "Block A, Floor A-28-09, Trefoil@Setia City, Jalan Setia Dagang AH U13/AH,Seksyen U13, 40170 Setia Alam, Selangor Darul Ehsan, Malaysia"
                
            case 1:
                heading = "(1-800-88-0098)"
            case 2:
                heading = "+603-3362 6028"
            case 3:
                heading = "03-3362 5027"
            case 4:
                heading = "info@thefollo.com"
            case 5:
                heading = "Mon - Sun: 9:00am - 5:00pm"
            default:
                print("error")
            }
        
        cell.textLabel?.attributedText = NSAttributedString(string: "\(heading)", attributes:
                                                                    [.underlineStyle: NSUnderlineStyle.single.rawValue])
        cell.textLabel?.textColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 15.0)
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = UITableViewCell()
        var heading = ""
        switch section {
        case 0:
            heading = "Address"
        case 1:
            heading = "Hot Line"
        case 2:
            heading = "Phone"
        case 3:
            heading = "Fax"
        case 4:
            heading = "Email Address"
        case 5:
            heading = "Working Time"
        default:
            print("error")
        }
        cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 15.0)
        cell.textLabel?.text = heading
        //cell.textLabel?.textColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)

        return cell
    }
    
    
}
