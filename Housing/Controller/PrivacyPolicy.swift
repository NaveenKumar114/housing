//
//  PrivacyPolicy.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-17.
//

import UIKit
import CollapsibleTableSectionViewController
class PrivacyPolicy: CollapsibleTableSectionViewController {

    var titleForSection = ["1. Introduction" , "2. Information We Process" , "3. The Purpose Of Processing Your Data" , "4. How Long We Store Your Information" , "5. Privacy Policy Changes" , "6. EU Consent Policy and Personalised Experience"]
    var tableViewData = [
        ["We manage our business with the principles indicated in this Privacy Policy in order to make sure that the confidentiality of personal information is secured and managed. Users’ access to the Mobile Application and use of the Services offered on the Housing application is subject to this Privacy Policy. By accessing the Mobile Application and by sustaining to use the Services provided, you accept this Privacy Policy, and in particular, you are deemed to have consented to our use and disclosure of your personal information in the manner described hereby. We reserve the right to make changes to this Privacy Policy at any time. If you do not agree with any part of this Privacy Policy, you must immediately discontinue accessing the Mobile Application and using the Services."],
        ["""
        As part of the regular functionality of our Services, we collect, use information about you. This Privacy Policy has been developed in a way for you to understand how we collect, use, communicate and disclose and make use of your personal information when the Services on the Mobile Application are used
        We will not sell, use, or disclose any data for any purpose with third parties.
        We will describe the reasons for which information is being collected, before or at the time of collecting personal information from you:
        Registration and Profile Information. When you create an account, we may collect your personal information such as your username, first and last name, email address, mobile phone number and photo, if you chose to have a photo associated with your account. We may collect billing information when you sign up for Premium service. The data we receive is dependent upon your privacy settings with the social network.
"""] ,
        ["""
        To make our service available. We use information that you submit and information that is processed automatically to provide you with all requested services, such as: create your account and identify you as a user.
            To improve, test and monitor the effectiveness of our App. We use information that is processed automatically to better understand user behaviour and trends, detect potential outages and technical issues, to operate, protect, improve, and optimise our App.
        """] ,
        ["We will only keep personal information as long as needed to fulfil the purposes identified. We will protect personal information by reasonable security protections against loss or theft, unauthorised access, disclosure, copying, or modification."] ,
        ["Because we're always looking for new and innovative ways to help you enjoy our services, this policy may change over time. We will notify you of any material changes so that you have time to review the changes."] ,
        ["Depending on your Privacy Settings, our partners may collect and process personal data such as device identifiers, language, location data, geolocation and other demographic and interest data about you to provide you with the most relevant experience when using this app."]
    ]
     var hiddenSections = Set<Int>()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.title = "Privacy Policy"
        // Do any additional setup after loading the view.
    }

}

extension PrivacyPolicy: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        return titleForSection.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData[section].count
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        cell.textLabel?.text = tableViewData[indexPath.section][indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 12.0)
        return cell
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleForSection[section]
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }
    
}
