//
//  ChangePasswordEmail.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-15.
//

import UIKit

class ChangePasswordEmail: UIViewController , UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    var hcode : String?
    static var hcodePassword : String?
    static var userdetails : ChangePasswordKcodeJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard

        emailLabel.delegate = self
        emailLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)
        if let img = defaults.string(forKey: userDefaultsKey.housingImage)
        {
            let urlStr = ("https://e-visitor.my/uploads/housing/\(img)")
            print(urlStr)
            print("hou")
            let url = URL(string: urlStr)
            
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                DispatchQueue.main.async { [self] in
                if   let image = UIImage(data: data!)
                {
                  
                  
                    profileImage.image = image
                    profileImage.layer.cornerRadius = 0.5 * profileImage.bounds.size.width
                    profileImage.clipsToBounds = true
                    profileImage.layer.borderWidth = 8
                    profileImage.layer.borderColor = UIColor.white.cgColor
                    viewForShadow.dropShadow()
                    viewForShadow.layer.cornerRadius = 0.5 * profileImage.bounds.size.width

                }
                }
            }
        }
    }


    @IBAction func endEmailPressed(_ sender: Any) {
        if emailLabel.text != ""
        {
            let number = arc4random_uniform(900000) + 100000;
            print(number)
            hcode = String(number)
          //  UserDefaults.standard.set(number, forKey: "hcode")
            makePostCall()
        }
        else
        {
            let alert = UIAlertController(title: "Reset Password", message: "Please enter a email", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func cancelPressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func makePostCall() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)changePassswordRequest")! as URL)
        request.httpMethod = "POST"
        ChangePasswordNewPassword.hcodePass = hcode
        let postString = "email=\(emailLabel.text!)&hcode=\(hcode!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(ChangePasswordKcodeJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                       // self.performSegue(withIdentifier: segueIdentifiers.confirmPasswordEmailToPassword, sender: loginBaseResponse)
                        ChangePasswordEmail.userdetails = loginBaseResponse
                        NotificationCenter.default.post(name: Notification.Name("confirmEmail"), object: nil)
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Reset Password", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
                
            }
        }
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.confirmPasswordEmailToPassword
        {
            let a = sender as! ChangePasswordKcodeJSON
            let destVC = segue.destination as! ChangePasswordNewPassword
            destVC.hcode = self.hcode
            destVC.userDetails = a
        }
     
    }

}
