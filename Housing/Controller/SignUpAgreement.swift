//
//  SignUpAgreement.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-21.
//

import UIKit

class SignUpAgreement: UIViewController {
    var userData : CheckUserJSON?
    @IBOutlet weak var agreeSwitch: UISwitch!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func agreeButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: segueIdentifiers.agreeToPassword, sender: userData)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.agreeToPassword
        {
            let data = sender as! CheckUserJSON
            let dest = segue.destination as! SignUpPassword
            dest.userData = data
            
        }
    }
}
