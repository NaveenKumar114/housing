//
//  RegisterNewUser.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-11.
//

import UIKit

class RegisterNewUser: UIViewController, bulkSeaseonDelegate {
    func toBulk() {
        performSegue(withIdentifier: segueIdentifiers.addVisitorToBulkSeaseon, sender: nil)
    }
    

    @IBOutlet weak var segmentController: UISegmentedControl!
    @IBOutlet weak var favouritesCollection: UIView!
    @IBOutlet weak var historyCollection: UIView!
    @IBOutlet weak var newCollection: UIView!
    override func viewDidLoad() {
        
        super.viewDidLoad()
        segmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentController.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        self.navigationItem.title = "New Visitor"
        let textAttributes = [NSAttributedString.Key.foregroundColor:UIColor.darkGray]
        navigationController?.navigationBar.titleTextAttributes = textAttributes
        segmentController.layer.cornerRadius = segmentController.frame.width / 2
        segmentController.layer.borderWidth = 2
        segmentController.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor

        segmentController.layer.masksToBounds = true
        NotificationCenter.default.addObserver(self, selector: #selector(applyFavourite(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.applyFavChangeView)"),object: nil)

    }

    @objc func applyFavourite(notfication: NSNotification) {
        newCollection.isHidden = false
        historyCollection.isHidden = true
        favouritesCollection.isHidden = true
        segmentController.selectedSegmentIndex = 0
    }
    
    @IBAction func segmentChanged(_ sender: UISegmentedControl) {
        print(sender.selectedSegmentIndex)
        switch sender.selectedSegmentIndex {
        case 0:
            newCollection.isHidden = false
            historyCollection.isHidden = true
            favouritesCollection.isHidden = true
        case 1:
            newCollection.isHidden = true
            historyCollection.isHidden = true
            favouritesCollection.isHidden = false
        case 2:
            newCollection.isHidden = true
            historyCollection.isHidden = false
            favouritesCollection.isHidden = true
        default:
            print("error")
        }
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == segueIdentifiers.newVisitorContainer) {
            let controller = segue.destination as! NewVisitor
            controller.delegate = self
        }
    }
}
