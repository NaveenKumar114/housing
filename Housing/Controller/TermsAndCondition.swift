//
//  TermsAndCondition.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-17.
//

import UIKit
import CollapsibleTableSectionViewController

class TermsAndCondition:  CollapsibleTableSectionViewController{

    var titleForSection = ["1. Intellectual Property" , "2. Disclaimer/ Limitation of Liability" , "3. Indemnity" , "4. Links to Other Websites" , "5. Variation/ Changes" , "6. Governing Law and Jurisdiction"]
    var tableViewData = [
        ["This website belongs to Housing. The copyright to the contents of this website is owned by or licensed to Housing. This website is intended for personal use, quick reference, illustration and information purposes only and may not be copied, redistributed or published in any manner without the written permission of Housing. Any unauthorised use of any part of this website is strictly prohibited. The trademarks, logos, characters and service marks (collectively “Trademarks”) displayed on this website belong to Housing. Nothing contained on this website should be construed as granting any license or right to use any Trademark displayed on this website. Any use/misuse of the Trademarks displayed on this website, or any other content on this website, except as provided in these Terms and Conditions, is strictly prohibited. Housing reserves the rights to bring any action arising from the improper or unauthorised use of this website, including any action for infringement of its trademarks and other intellectual property rights."],
        ["Housing does not provide any warranty or guarantee as to the accuracy, timeliness, performance, completeness or suitability of the information and materials found or offered on this website for any particular purpose. You acknowledge that such information and materials may contain inaccuracies or errors and Housing expressly exclude liability for any such inaccuracies or errors to the fullest extent permitted by law. Any use of any information or materials on this website is entirely at your own risk, for which Housing shall not be liable. It shall be your own responsibility to ensure that any materials or information available through this website meet your specific requirements. Neither Housing nor any other party involved in creating, producing or delivering this website is liable for any direct, incidental, consequential, indirect or punitive damages arising out of your access to, or use of, this website. Without limiting the foregoing, everything on this website is provided to you “AS IS” WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED. Housing SHALL NOT BE LIABLE FOR ANY DAMAGES, LOSS OF PROFITS, INJURIES, SAVINGS OR GOODWILL RESULTING FROM YOUR USE OR INABILITY TO USE INFORMATION PROVIDED FROM THIS WEBSITE."] ,
        ["You shall indemnify and keep Housing indemnified against all claims, damages, actions and proceedings made or brought against Housing arising from your use of this website and/or any breach of terms in relation thereto by you."] ,
        ["The linkage to other websites provided herein is merely for your convenience and does not signify that Housing endorses such websites. Housing bears no responsibility for the contents of such other websites and shall not be held liable for any damages or injury howsoever arising therefrom. You shall view any of the linked websites at your own risk."] ,
        ["Housing reserves the rights to change, vary or modify any of the information and terms contained herein without notice."] ,
        ["The terms and conditions herein shall be governed by and construed in accordance with the laws of Malaysia and you shall submit to the jurisdiction of the courts of Malaysia."]
    ]
     var hiddenSections = Set<Int>()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.title = "Terms & Conditions"
        // Do any additional setup after loading the view.
    }

}

extension TermsAndCondition: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        return titleForSection.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData[section].count
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        cell.textLabel?.text = tableViewData[indexPath.section][indexPath.row]
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 12.0)
        return cell
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleForSection[section]
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return true
    }
    
}
