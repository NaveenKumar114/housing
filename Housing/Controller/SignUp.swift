

import FirebaseAuth
import Firebase
import UIKit
import CountryPickerView
class SignUp: UIViewController , UITextFieldDelegate, CountryPickerViewDelegate{
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        print(country)
        countryCode = country.phoneCode
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
        self.navigationController?.isNavigationBarHidden = false

    }

    @IBOutlet weak var notMemberName: UITextField!
    @IBOutlet weak var notMemberMobile: UITextField!
    @IBOutlet weak var notMemberEmail: UITextField!
    @IBOutlet weak var notmenberswitch: UISwitch!
    @IBOutlet weak var notMemberOfHousing: UIScrollView!
    static var loginData : CheckUserJSON?
    var countryCode : String?
    @IBOutlet weak var memberIdentificationType: UITextField!
    @IBOutlet weak var memberIdentificationNumber: UITextField!
    @IBOutlet weak var memberMobile: UITextField!
    @IBOutlet weak var memberEmail: UITextField!
    @IBOutlet weak var memberName: UITextField!
    @IBOutlet weak var memberSwitch: UISwitch!
    @IBOutlet weak var memberOfHousing: UIScrollView!
    var memberTextFieldsArray = [UITextField]()
    var notMemberTextFieldsArray = [UITextField]()
 
    var typeOptions = ["NRIC" , "PASSPORT"]
    var pickerViewArray = [UITextField]()
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        print("Signup")
        //navigationController?.setNavigationBarHidden(false, animated: true)
        let cpv = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        memberMobile.leftView = cpv
        cpv.delegate = self
        countryCode = cpv.selectedCountry.phoneCode

        memberMobile.leftViewMode = .always
        let cpv2 = CountryPickerView(frame: CGRect(x: 0, y: 0, width: 120, height: 20))
        cpv2.delegate = self
        notMemberMobile.leftView = cpv2
        notMemberMobile.leftViewMode = .always
        memberTextFieldsArray = [memberEmail , memberMobile , memberMobile , memberIdentificationType , memberIdentificationNumber]
        notMemberTextFieldsArray = [notMemberEmail , notMemberMobile , notMemberName]
        memberOfHousing.isHidden = true
        notMemberOfHousing.isHidden = false
        pickerViewArray = [memberIdentificationType]
       // navigationController?.navigationBar.barTintColor = #colorLiteral(red: 0.8090034127, green: 0.6006121635, blue: 0.1609272361, alpha: 1)
        for textField in 0 ... pickerViewArray.count - 1
        {
            pickerViewArray[textField].delegate = self
            createPickerView(textField: pickerViewArray[textField], tag: textField)
            
        }
        for n in memberTextFieldsArray
        {
            n.delegate = self
            n.font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        for n in notMemberTextFieldsArray
        {
            n.delegate = self
            n.font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
        
    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.tag = tag
        textField.inputView = pickerView
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in pickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    @IBAction func memberOrNotMEnber(_ sender: UISwitch) {
        
        if sender.isOn
        {
            memberSwitch.setOn(true, animated: true)
            notmenberswitch.setOn(true, animated: true)
            memberOfHousing.isHidden = false
            notMemberOfHousing.isHidden = true
            memberName.text = notMemberName.text
            memberMobile.text = notMemberMobile.text
            memberEmail.text = notMemberEmail.text
        }
        else
        {
            memberSwitch.setOn(false, animated: true)
            notmenberswitch.setOn(false, animated: true)
            memberOfHousing.isHidden = true
            notMemberOfHousing.isHidden = false
            
            notMemberName.text = memberName.text
            notMemberMobile.text = memberMobile.text
            notMemberEmail.text = memberEmail.text
        }
    }

    @IBAction func nextPressedMember(_ sender: Any) {
        var empty = 0
        for textfield in memberTextFieldsArray
        {
            if textfield.text == ""
            {
                empty = 1
            }
            
        }
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            sendotp()
         
        }
        
    }
    @IBAction func nextPressedNotMember(_ sender: Any) {
        var empty = 0
        for textfield in notMemberTextFieldsArray
        {
            if textfield.text == ""
            {
                empty = 1
            }
            
        }
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            makePostCall(contact: notMemberMobile.text!, email: notMemberEmail.text!, identificationType: "", identificationNumber: "", name: notMemberName.text!)
        }
        
    }
    func sendotp()
    {
        Auth.auth().languageCode = "en"

        let phoneNumber = "\(countryCode!)\(memberMobile.text!)"
        print(phoneNumber)
        PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber, uiDelegate: nil) { [self] (verificationID, error) in
          if let error = error {
            print(error.localizedDescription)
            let alert = UIAlertController(title: "Register", message: "\(error.localizedDescription)", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
            //self.showMessagePrompt(error.localizedDescription)
            return
          }
            UserDefaults.standard.set(verificationID, forKey: "authVerificationID")
            //self.performSegue(withIdentifier: "otp", sender: nil)
            makePostCall(contact: memberMobile.text!, email: memberEmail.text!, identificationType: memberIdentificationType.text!, identificationNumber: memberIdentificationNumber.text!, name: memberName.text!)
          // Sign in using the verificationID and the code sent to the user
          // ...
        }
    }

    func makePostCall(contact : String , email : String , identificationType : String , identificationNumber : String , name : String) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["member_contact":"\(contact)","member_email":"\(email)","member_identification_type":"\(identificationType)","member_indentification_no":"\(identificationNumber)","member_name":"\(name)","member_password":"","member_role":"Family","member_status":"PENDING","user_role":"Family","verification_code":"","code":0,"create_by":"\(name)","modify_by":"\(name)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string: "\(ConstantsUsedInProject.baseUrl)check_usermembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                   
                    let loginBaseResponse = try? decoder.decode(CheckUserJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code

                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            
                            //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            print("success")
                            //print(loginBaseResponse as Any)
                            let alert = UIAlertController(title: "Register", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        }else if code_str == 203  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            SignUp.loginData = loginBaseResponse
                            print("sign \(SignUp.loginData)")
                            self.performSegue(withIdentifier: segueIdentifiers.signUpToAgree, sender: loginBaseResponse)
                            

                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.signUpToAgree
        {
            let data = sender as! CheckUserJSON
            let dest = segue.destination as! SignUpAgreement
            dest.userData = data
        }
    }
}

extension SignUp : UIPickerViewDelegate , UIPickerViewDataSource 
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            pickerViewArray[0].text = typeOptions[0]

        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return typeOptions.count
        case 1:
            return 2
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return typeOptions[row]
        case 1:
            return typeOptions[row]
    
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            pickerViewArray[0].text = typeOptions[row]

        default:
            print("err")
        }
    }
   
}


