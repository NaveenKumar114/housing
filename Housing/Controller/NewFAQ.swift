//
//  NewFAQ.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-05.
//

import UIKit
import CollapsibleTableSectionViewController
class NewFAQ:  CollapsibleTableSectionViewController {
    var titleForSection = ["1. Example"]
    var tableViewData = [
        ["No Data"]
    ]
    let child = SpinnerViewController()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        createSpinnerView()
        makePostCall()
        self.title = "FAQ"
        // Do any additional setup after loading the view.
    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_faqlist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(FAQJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        titleForSection.removeAll()
                        tableViewData.removeAll()
                        removeSpinnerView()
                            if loginBaseResponse?.faqList?.count == 0
                             {
                                let alert = UIAlertController(title: "FAQ", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                               }else
                            {
                                for n in 0 ... (loginBaseResponse?.faqList!.count)! - 1
                                {
                                    titleForSection.append((loginBaseResponse?.faqList![n].question)!)
                                    
                                    tableViewData.append([(loginBaseResponse?.faqList![n].answer)!])
                                }
                            }
                     
                        //servicesTabelView.reloadData()
                        self.reload()
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }


 
}
extension NewFAQ: CollapsibleTableSectionDelegate {
    
    func numberOfSections(_ tableView: UITableView) -> Int {
        return titleForSection.count
    }
    
    func collapsibleTableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableViewData[section].count
    }
    
    func collapsibleTableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let cell2 = tableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.faqCellIdentifier) as! FAQCell
     //   cell2.dataLabel.text = self.tableViewData[indexPath.section][indexPath.row]
        cell.textLabel?.text = self.tableViewData[indexPath.section][indexPath.row]
        cell2.dataLabel?.text = self.tableViewData[indexPath.section][indexPath.row]
        return cell2
    }
    
    func collapsibleTableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }
    
    func collapsibleTableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return titleForSection[section]
    }
    
    func shouldCollapseByDefault(_ tableView: UITableView) -> Bool {
        return false
    }
    
}
