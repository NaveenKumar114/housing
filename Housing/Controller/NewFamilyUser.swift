//
//  NewFamilyUser.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-12.
//

import UIKit

class NewFamilyUser: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var familyUserTitle: UILabel!
    @IBOutlet weak var familyOrTenant: UITextField!
    @IBOutlet weak var identificationType: UITextField!
    @IBOutlet weak var identificationNumber: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var checkingScroll: UIScrollView!
    
    @IBOutlet weak var addUserUserName: UITextField!
    @IBOutlet weak var addUserEmail: UITextField!
    @IBOutlet weak var addUserContact: UITextField!
    @IBOutlet weak var addUserIdentificationNumber: UITextField!
    @IBOutlet weak var addUserScroll: UIScrollView!
    @IBOutlet weak var addUserFamilyOrTentant: UITextField!
    @IBOutlet weak var addUserIdetificationType: UITextField!
    var newFamilyTextFields = [UITextField]()
    var visitoOrFamily = ["Family" , "Tenant" , "Admin"]
    var identificationTypes = ["NRIC" , "Passport"]
    var address : String?
    var addresId : String?
    var verified = false
    var editProfile = false
    var editUserData : UsermembersList?
    var editUserTextFileds = [UITextField]()
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        email.delegate = self
        email.tag = 12 // Dont delete this
        identificationNumber.delegate = self
        identificationNumber.tag = 10 // Dont deleter this
        newFamilyTextFields = [identificationType , familyOrTenant]
        editUserTextFileds = [addUserEmail , addUserIdetificationType , addUserIdentificationNumber , addUserFamilyOrTentant , addUserUserName , addUserContact]
        createPickerView(textField: addUserIdetificationType, tag: 2)
        createPickerView(textField: addUserFamilyOrTentant, tag: 3)
        
        for n in editUserTextFileds
        {
            n.delegate = self
            n.font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        address = UserDefaults.standard.string(forKey: userDefaultsKey.address)
        addresId = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
        self.title = "New User"
        if let safeData = editUserData
        {
            self.title = "Update User"
            addUserEmail.text = safeData.memberEmail
            addUserIdetificationType.text = safeData.memberIdentificationType
            addUserIdentificationNumber.text = safeData.memberIndentificationNo
            addUserFamilyOrTentant.text = safeData.memberType
            verified = true
            addUserScroll.isHidden = false
            checkingScroll.isHidden = true
            addUserEmail.isUserInteractionEnabled = false
            addUserUserName.text = safeData.memberName
            addUserContact.text = safeData.memberContact
        }
        if editProfile
        {
            let defaults = UserDefaults.standard

            addUserEmail.isUserInteractionEnabled = false
            self.title = "Update Profile"
            addUserEmail.text = defaults.string(forKey: userDefaultsKey.email) ?? ""
            addUserIdetificationType.text = defaults.string(forKey: userDefaultsKey.identifiactionType) ?? ""
            addUserIdentificationNumber.text = defaults.string(forKey: userDefaultsKey.identificationNumber) ?? ""
            addUserFamilyOrTentant.text = defaults.string(forKey: userDefaultsKey.memberRoleText) ?? ""
            verified = true
            addUserScroll.isHidden = false
            checkingScroll.isHidden = true
            addUserUserName.text = defaults.string(forKey: userDefaultsKey.name) ?? ""
            addUserContact.text = defaults.string(forKey: userDefaultsKey.phoneNumber) ?? ""
        }
        for textField in 0 ... newFamilyTextFields.count - 1
        {
            newFamilyTextFields[textField].delegate = self
            createPickerView(textField: newFamilyTextFields[textField], tag: textField)
            newFamilyTextFields[textField].font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        dismissPickerView()
   
    }
    
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in newFamilyTextFields
        {
            textField.inputAccessoryView = toolBar
        }
        addUserIdetificationType.inputAccessoryView = toolBar
        addUserFamilyOrTentant.inputAccessoryView = toolBar
        identificationType.inputAccessoryView = toolBar
        familyOrTenant.inputAccessoryView = toolBar
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    @IBAction func confirmPressed(_ sender: Any) {
        var empty = 0
        if editUserData != nil || editProfile
        {
            for n in editUserTextFileds
            {
                empty = n.text == "" ? 1 : 0
            }
            if empty == 1
            {
                let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                var memberRole = ""
                if editUserData != nil{
                    memberRole = (editUserData?.memberRole!)!
                }
                if editProfile
                {
                    memberRole = UserDefaults.standard.string(forKey: userDefaultsKey.memberRole)!
                }
                makePostCallUpdateUsers(memberRole: memberRole)
            }
        }
        else
        {
        for n in newFamilyTextFields
        {
            empty = n.text == "" ? 1 : 0
        }
        if identificationNumber.text == "" || email.text == ""
        {
            empty = 1
        }
        }
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            if verified
            {
                var memberRole = ""
                switch addUserFamilyOrTentant.text {
                case "Family":
                    memberRole = "2"
                case "Tenant":
                    memberRole = "3"
                case "Admin":
                    memberRole = "1"
                default:
                    print("ss")
                }
                makePostCallAddUsers(memberRole: memberRole)
                print("verify")
            }
            else
            {
               
                makePostCallUsers()

            }
        }
    }
    func makePostCallUsers() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let name = defaults.string(forKey: userDefaultsKey.name)
        let housing = defaults.string(forKey: userDefaultsKey.housingName)
        
        let snippet = address
        let range = snippet!.range(of: ",")
        let addressWithoutNumber = snippet![range!.upperBound...].trimmingCharacters(in: .whitespaces)
        let decoder = JSONDecoder()
        var memberRole = 0
        switch familyOrTenant.text! {
        case "Family":
            memberRole = 2
        case "Tenant":
            memberRole = 3
        case "Admin":
            memberRole = 1
        default:
            memberRole = 1
        }
        let json: [String: Any] = ["member_address":"\(addressWithoutNumber)","member_address_id":"\(addresId!)","member_contact":"","member_email":"\(email.text!)","member_housingname":"\(housing!)","member_identification_type":"\(identificationType.text!)","member_indentification_no":"\(identificationNumber.text!)","member_name":"","member_role":"\(memberRole)","member_status":"PENDING","member_type":"\(familyOrTenant.text!)","subscriber_id":"\(sub!)","user_role":"\(familyOrTenant.text!)","userid":"\(usr!)","code":0,"create_by":"\(name!)","modify_by":"\(name!)"]
        print(memberRole)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)add_usermembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(UserListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                           
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)

                            //print(self.userList?.usermembersList)
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                          
                            if loginBaseResponse?.response == "Verified"
                            {
                                addUserEmail.text = email.text
                                addUserIdetificationType.text = identificationType.text
                                addUserIdentificationNumber.text = identificationNumber.text
                                addUserFamilyOrTentant.text = familyOrTenant.text
                                verified = true
                                addUserScroll.isHidden = false
                                checkingScroll.isHidden = true
                            
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            // add an action (button)
                           
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostCallAddUsers(memberRole : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let name = defaults.string(forKey: userDefaultsKey.name)
        let housing = defaults.string(forKey: userDefaultsKey.housingName)
        let snippet = address
        let range = snippet!.range(of: ",")
        let addressWithoutNumber = snippet![range!.upperBound...].trimmingCharacters(in: .whitespaces)
        let decoder = JSONDecoder()
       // let json: [String: Any] = ["address":"\(addressWithoutNumber)","address_id":"\(addressID!)","subscriber_id":"\(sub!)","userid":"\(usr!)" , "roletype":"\(roleType)"]
        let json: [String: Any] = ["member_address":"\(addressWithoutNumber)","member_address_id":"\(addresId!)","member_contact":"\(addUserContact.text!)","member_email":"\(addUserEmail.text!)","member_housingname":"\(housing!)","member_identification_type":"\(addUserIdetificationType.text!)","member_indentification_no":"\(addUserIdentificationNumber.text!)","member_name":"\(addUserUserName.text!)","member_role":"\(memberRole)","member_status":"PENDING","member_type":"\(addUserFamilyOrTentant.text!)","subscriber_id":"\(sub!)","user_role":"\(addUserFamilyOrTentant.text!)","userid":"\(usr!)","code":0,"create_by":"\(name!)","modify_by":"\(name!)"]
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)add_usermembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(AddUserJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            NotificationCenter.default.post(name: Notification.Name("\(notificationNames.userAddedORUpdated)"), object: nil)
                            //print(self.userList?.usermembersList)
                        }else  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                          
                          
                                let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            
                            // add an action (button)
                           
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostCallUpdateUsers(memberRole : String) {
        let defaults = UserDefaults.standard
        var sub = defaults.string(forKey: "subID")
        var usr = defaults.string(forKey: "userID")
        var usrMberID = defaults.string(forKey: userDefaultsKey.userMemberID)
        if let safeDate = editUserData
        {
            sub = safeDate.subscriberID
            usr = safeDate.userid
            usrMberID = safeDate.userMemberid
        }
        let name = defaults.string(forKey: userDefaultsKey.name)
        let housing = defaults.string(forKey: userDefaultsKey.housingName)
        let snippet = address
        let range = snippet!.range(of: ",")
        let addressWithoutNumber = snippet![range!.upperBound...].trimmingCharacters(in: .whitespaces)
        let decoder = JSONDecoder()
       // let json: [String: Any] = ["address":"\(addressWithoutNumber)","address_id":"\(addressID!)","subscriber_id":"\(sub!)","userid":"\(usr!)" , "roletype":"\(roleType)"]
        let json: [String: Any] = ["member_address":"\(addressWithoutNumber)","member_address_id":"\(addresId!)","member_contact":"\(addUserContact.text!)","member_email":"\(addUserEmail.text!)","member_housingname":"\(housing!)","member_identification_type":"\(addUserIdetificationType.text!)","member_indentification_no":"\(addUserIdentificationNumber.text!)","member_name":"\(addUserUserName.text!)","member_role":"\(memberRole)","member_status":"ACTIVE","member_type":"\(addUserFamilyOrTentant.text!)","subscriber_id":"\(sub!)","user_role":"\(addUserFamilyOrTentant.text!)","user_memberid":"\(usrMberID!)","code":0,"create_by":"\(name!)","modify_by":"\(name!)" , "userid":"\(usr!)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)update_usermembers")
        {
            print("Update user")
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                   // print(String(data: data!, encoding: String.Encoding.utf8))
                    let loginBaseResponse = try? decoder.decode(UpdateUserJSON.self, from: data!)
                    //print(loginBaseResponse)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                                self.backTwo()
                            }))
                            NotificationCenter.default.post(name: Notification.Name("\(notificationNames.userAddedORUpdated)"), object: nil)
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            //print(self.userList?.usermembersList)
                        }else  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                          
                          
                                let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            
                            // add an action (button)
                           
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    func backTwo() {
        let viewControllers: [UIViewController] = self.navigationController!.viewControllers as [UIViewController]
        self.navigationController!.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }

}

extension NewFamilyUser : UIPickerViewDelegate , UIPickerViewDataSource
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            newFamilyTextFields[0].text = identificationTypes[0]
        case 1:
            newFamilyTextFields[1].text = visitoOrFamily[0]
        case 2 :
            addUserIdetificationType.text = identificationTypes[0]
        case 3 :
            addUserFamilyOrTentant.text = visitoOrFamily[0]
        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return 2
        case 1:
            return 3
        case 2:
            return 2
        case 3:
            return 3
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return identificationTypes[row]
        case 1:
            return visitoOrFamily[row]
        case 2:
            return identificationTypes[row]
        case 3:
            return visitoOrFamily[row]
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            newFamilyTextFields[0].text = identificationTypes[row]
        case 1:
            newFamilyTextFields[1].text = visitoOrFamily[row]
        case 2 :
            addUserIdetificationType.text = identificationTypes[row]
        case 3 :
            addUserFamilyOrTentant.text = visitoOrFamily[row]

        default:
            print("err")
            print(pickerView.tag)
        }
    }
}






