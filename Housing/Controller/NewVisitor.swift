//
//  NewVisitor.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-10.
//

import UIKit
import DatePicker
import Alamofire
import DatePickerDialog
import ContactsUI
class NewVisitor: UIViewController , CNContactPickerDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var visitorPassText: UILabel!
    @IBOutlet weak var dropofButtonView: UIView!
    @IBOutlet weak var seasonPassText: UILabel!
    @IBOutlet weak var dropOffText: UILabel!
    @IBOutlet weak var overnightText: UILabel!
    @IBOutlet weak var seasonPassImage: UIImageView!
    @IBOutlet weak var overNightImage: UIImageView!
    @IBOutlet weak var visitorPassImage: UIImageView!
    @IBOutlet weak var dropOffimage: UIImageView!
    @IBOutlet weak var openContactsIcon: UIImageView!
    var delegate : bulkSeaseonDelegate?
    @IBOutlet weak var addressDropOff: UILabel!
    @IBOutlet weak var visitorOrFamilyDropOff: UITextField!
    @IBOutlet weak var nameDropOff: UITextField!
    @IBOutlet weak var phoneNumberDropOff: UITextField!
    @IBOutlet weak var seaseonPasscontact: UIImageView!
    @IBOutlet weak var vehicleNumberDropOff: UITextField!
    @IBOutlet weak var dateDropOff: UILabel!
    @IBOutlet weak var timeDropOff: UILabel!
    @IBOutlet weak var startDateOvernight: UILabel!
    @IBOutlet weak var endDateOvernight: UILabel!
    @IBOutlet weak var timeOvernight: UILabel!
    var dropOffVisitorsPassTextFields = [UITextField]()
    var selected = 0
    var addressID : String?
    var addressList : AddressJSON?
    var visitoOrFamily = ["Family" , "Visitor"]
    var identificationTypes = ["NRIC" , "Passport"]
    @IBOutlet weak var seaseonView: UIScrollView!
    @IBOutlet weak var overnightStayView: UIView!
    @IBOutlet weak var dropOffVisitorOvernightScrollView: UIScrollView!
    @IBOutlet weak var visitorsPassButton: CircularButton!
    @IBOutlet weak var dropOffVisitorView: UIView!
    @IBOutlet weak var seaseonPassButton: CircularButton!
    @IBOutlet weak var overnightButton: CircularButton!
    @IBOutlet weak var dropOffButton: CircularButton!
    var buttonArray = [CircularButton]()
    var dropOffPickerViewArray = [UITextField]()
    var textfieldOpenFirstTime = [true , true , true]

    @IBOutlet weak var seasenPassSegment: UISegmentedControl!
    
    
    @IBOutlet weak var vehicleModelSeaseonPass: UITextField!
    
   
    @IBOutlet weak var familyOrVisitorSeasonPass: UITextField!
    @IBOutlet weak var nameSeasonPass: UITextField!
    @IBOutlet weak var phoneNoSeasonPass: UITextField!
    @IBOutlet weak var vehicleNoSeasonPass: UITextField!
    @IBOutlet weak var identificationType: UITextField!
    @IBOutlet weak var identificationNoSeasonPass: UITextField!
    @IBOutlet weak var purposeSeasonPass: UITextField!
    @IBOutlet weak var startDateSeasonPass: UILabel!
    @IBOutlet weak var endDateSeasonPass: UILabel!
    @IBOutlet weak var timeSeasonPass: UILabel!
    @IBOutlet weak var identificationPhotoSeasonPass: UIImageView!
    var seasonPassTextFields = [UITextField]()
    var selectedImageView : UIImageView?
    var identificationImage : UIImage?
    var faceImage : UIImage?
    var imageViewArray  = [UIImageView]()
    var textArray = [UILabel]()
    @IBOutlet weak var addressSeasonPass: UILabel!
    @IBOutlet weak var facePhotoSeasonPass: UIImageView!
    
    static var screenSize = UIScreen.main.bounds.size.height
    static var screenWidth = UIScreen.main.bounds.size.width

    
    override func viewDidLoad() {
        super.viewDidLoad()
        addressSeasonPass.font = UIFont(name: "Arial-Rounded", size: 15.0)
        addressDropOff.font = UIFont(name: "Arial-Rounded", size: 15.0)

        seasenPassSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        seasenPassSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        dropOffButton.tag = 0
        visitorsPassButton.tag = 1
        overnightButton.tag = 2
        seaseonPassButton.tag = 3
        imageViewArray = [dropOffimage , visitorPassImage , overNightImage , seasonPassImage]
        textArray = [dropOffText , visitorPassText , overnightText , seasonPassText]
        buttonArray = [dropOffButton , visitorsPassButton , overnightButton , seaseonPassButton]
        dropOffVisitorsPassTextFields = [visitorOrFamilyDropOff , nameDropOff , phoneNumberDropOff , vehicleNumberDropOff]  //these are common for dropoff ,visitor pass and overnightstay . overnight stay only time and date label changes all other are same
        seasonPassTextFields = [familyOrVisitorSeasonPass , nameSeasonPass , phoneNoSeasonPass , vehicleNoSeasonPass , identificationType , identificationNoSeasonPass , purposeSeasonPass , vehicleModelSeaseonPass]
        for n in dropOffVisitorsPassTextFields
        {
            n.delegate = self
            n.font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        for n in seasonPassTextFields
        {
            n.delegate = self
            n.font = UIFont(name: "Arial-Rounded", size: 15.0)

        }
        makePostCallAddress()
        dropOffPickerViewArray = [visitorOrFamilyDropOff ,familyOrVisitorSeasonPass , identificationType]
        for textField in 0 ... dropOffPickerViewArray.count - 1
        {
            dropOffPickerViewArray[textField].delegate = self
            createPickerView(textField: dropOffPickerViewArray[textField], tag: textField)
            
        }
        let gestureAddress = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressDropOff.addGestureRecognizer(gestureAddress)
        let gesture1 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        dateDropOff.addGestureRecognizer(gesture1)
        let gesture2 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        timeDropOff.addGestureRecognizer(gesture2)
        let gesture3 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        timeOvernight.addGestureRecognizer(gesture3)
        let gesture4 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        startDateOvernight.addGestureRecognizer(gesture4)
        let gesture5 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        endDateOvernight.addGestureRecognizer(gesture5)
        visitorOrFamilyDropOff.text = "Family"
        familyOrVisitorSeasonPass.text = "Family"
        identificationType.placeholder = "Identification Type"
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            addressDropOff.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            addressID = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            addressSeasonPass.text = addressDropOff.text
           // addressSeasonPass.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
        }
        dismissPickerView()
        let gestureAddressSeason = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressSeasonPass.addGestureRecognizer(gestureAddressSeason)
        identificationPhotoSeasonPass.isUserInteractionEnabled = true
        facePhotoSeasonPass.isUserInteractionEnabled = true
        let gestureident = UITapGestureRecognizer(target: self, action: #selector(toImage(sender:)))
        identificationPhotoSeasonPass.addGestureRecognizer(gestureident)
        let gestureface = UITapGestureRecognizer(target: self, action: #selector(toImage(sender:)))
        facePhotoSeasonPass.addGestureRecognizer(gestureface)
        
        
        let gesture6 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        endDateSeasonPass.addGestureRecognizer(gesture6)
        let gesture7 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        startDateSeasonPass.addGestureRecognizer(gesture7)
        let gesture8 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        timeSeasonPass.addGestureRecognizer(gesture8)
        
        let gesture9 = UITapGestureRecognizer(target: self, action: #selector(openContacts))
        openContactsIcon.addGestureRecognizer(gesture9)
        let gesture10 = UITapGestureRecognizer(target: self, action: #selector(openContacts))
        seaseonPasscontact.addGestureRecognizer(gesture10)
        NotificationCenter.default.addObserver(self, selector: #selector(applyFavourite(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.applyFavoutites)"),object: nil)
    }
    @objc func applyFavourite(notfication: NSNotification) {
        if let pass = notfication.userInfo!["pass"] as? FavList
        {
           refreshTextfields()
            var passType = 0
            if pass.seasonType == "Single"
            {
                passType = 3
                print(pass)
                familyOrVisitorSeasonPass.text = pass.category
                nameSeasonPass.text = pass.name
                phoneNoSeasonPass.text = pass.contactNo
                vehicleNoSeasonPass.text = pass.vehicleNo
                vehicleModelSeaseonPass.text = pass.vehicleType
                identificationType.text = pass.identificationType
                identificationNoSeasonPass.text = pass.identificationNo
                purposeSeasonPass.text = pass.purpose
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                timeSeasonPass.text = formatter.string(from: Date())
                timeOvernight.text = formatter.string(from: Date())
                formatter.dateFormat = "dd-MM-yyyy"
                startDateSeasonPass.text = formatter.string(from: Date())
                endDateSeasonPass.text = formatter.string(from: Date())
                
                
            }
            else
            {
                switch pass.visitorType {
                case "Drop Off Pick Up":
                    passType = 0
                case "Visitor Pass":
                    passType = 1
                case "Overnight Stay":
                    passType = 2
                default:
                    passType = 0
                }
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                timeDropOff.text = formatter.string(from: Date())
                timeOvernight.text = formatter.string(from: Date())
                formatter.dateFormat = "dd-MM-yyyy"
                dateDropOff.text = formatter.string(from: Date())
                startDateOvernight.text = formatter.string(from: Date())
                endDateOvernight.text = formatter.string(from: Date())
                visitorOrFamilyDropOff.text = pass.category
                nameDropOff.text = pass.name
                phoneNumberDropOff.text = pass.contactNo
                vehicleNumberDropOff.text = pass.vehicleNo
            }
            
            switch passType {
            case 0:
                //Dropoff
                selected = 0
                dropOffVisitorOvernightScrollView.isHidden = false
                seaseonView.isHidden = true
                dropOffVisitorView.isHidden = false
                overnightStayView.isHidden = true
                dropofButtonView.isHidden = false
                
                break
            case 1:
                //visitorspass
                selected = 1
                dropOffVisitorOvernightScrollView.isHidden = false
                seaseonView.isHidden = true
                dropOffVisitorView.isHidden = false
                overnightStayView.isHidden = true
                break
            case 2:
                //overnightstay
                selected = 2
                dropOffVisitorOvernightScrollView.isHidden = false
                seaseonView.isHidden = true
                dropOffVisitorView.isHidden = true
                dropofButtonView.isHidden = false

                overnightStayView.isHidden = false
                break
            case 3:
                //seasonpass
                selected = 3
                dropOffVisitorOvernightScrollView.isHidden = true
                seaseonView.isHidden = false
                dropofButtonView.isHidden = true
                break
            default:
                selected = 4
            //error
            }
            for n in 0...3
            {
                if n == selected
                {
                    //buttonArray[n].backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                    //buttonArray[n].tintColor = .white
                    buttonArray[n].alpha = 0
                    imageViewArray[n].alpha = 1
                    textArray[n].textColor = ConstantsUsedInProject.appThemeColor
                }
                else
                {
                    buttonArray[n].alpha = 1
                    imageViewArray[n].alpha = 0
                    textArray[n].textColor = .darkGray
                }
            }

        }
    }
    @objc func openContacts()
    {
        let contacVC = CNContactPickerViewController()
               contacVC.delegate = self
               self.present(contacVC, animated: true, completion: nil)
    }
    func contactPicker(_ picker: CNContactPickerViewController, didSelect contact: CNContact) {
          print(contact.phoneNumbers)
          let numbers = contact.phoneNumbers.first
          print((numbers?.value)?.stringValue ?? "")
        print(contact)
        self.nameDropOff.text = "\(contact.givenName)  \(contact.familyName)"
        self.phoneNumberDropOff.text = contact.phoneNumbers.first?.value.stringValue
        self.nameSeasonPass.text = "\(contact.givenName)  \(contact.familyName)"
        self.phoneNoSeasonPass.text = contact.phoneNumbers.first?.value.stringValue
     
      }

      func contactPickerDidCancel(_ picker: CNContactPickerViewController) {
          self.dismiss(animated: true, completion: nil)
      }
    @IBAction func singlrOrBulkSeaseonPass(_ sender: Any) {
        let x = sender as! UISegmentedControl
        if x.selectedSegmentIndex == 1
        {
            delegate?.toBulk()
        }
    }
    @objc func toImage(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UIImageView
        selectedImageView = x
        let chooseImageActionMenu = UIAlertController(title: "Choose an option", message: nil, preferredStyle: .actionSheet)
        let galleryButton = UIAlertAction(title: "Gallery", style: .default) { (_) in
            self.openGallery()
        }
        let cameraButton = UIAlertAction(title: "Camera", style: .default) { (_) in
            self.openCamera()
        }
        let cancelButton = UIAlertAction(title: "Cancel", style: .cancel)
        chooseImageActionMenu.addAction(galleryButton)
        chooseImageActionMenu.addAction(cameraButton)
        chooseImageActionMenu.addAction(cancelButton)
        self.present(chooseImageActionMenu, animated: true, completion: nil)
    }
    
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case dateDropOff , startDateOvernight , endDateOvernight , startDateSeasonPass , endDateSeasonPass:
            datePickerTapped(sender: x!)
        case timeDropOff , timeOvernight , timeSeasonPass:
            timePickerTapped(sender: x!)
        default:
            print("error in time date")
        }
        
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                // print(formatter.string(from: dt))
            }
        }
        
    }
    func timePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .time) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                //print(formatter.string(from: dt))
                sender.text = formatter.string(from: dt)
                
            }
        }
        
    }
    func createPickerView(textField : UITextField , tag : Int) {
        
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag + 100
        textField.tintColor = UIColor.clear
        pickerView.tag = tag + 100
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in dropOffPickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    @IBAction func addToFavourotesSeasonPass(_ sender: Any) {
        seasonPassFavourites()
    }
    func seasonPassFavourites()
    {
        var passType : String!
        var time : String!
        var startDate : String!
        var endDate : String!
        var empty = 0
        passType = "Season Pass"
        for n in seasonPassTextFields
        {
            empty = n.text == "" ? 1 : 0
        }
        if addressSeasonPass.text == "" || addressID == nil || purposeSeasonPass.text == ""
        {
            empty = 1
        }
     
            if startDateSeasonPass.text == "Start Date" || endDateSeasonPass.text == "End Date" || timeSeasonPass.text == "Time"
            {
                empty = 1
            }
            else
            {
                startDate = convertDateFormater(startDateSeasonPass.text!)
                endDate = convertDateFormater(endDateSeasonPass.text!)
            time = timeSeasonPass.text!
            }
        if faceImage == nil || identificationImage == nil
        {
            empty = 1
        }
        
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let imageDataFace = faceImage!.jpegData(compressionQuality: 0.50)!
            let imageDataIdent = identificationImage!.jpegData(compressionQuality: 0.50)!
            let defaults = UserDefaults.standard
            let subID = defaults.string(forKey: "subID")
            let usrID = defaults.string(forKey: "userID")
            let name = defaults.string(forKey: userDefaultsKey.name)
            let email = defaults.string(forKey: userDefaultsKey.email)
            let housing = defaults.string(forKey: userDefaultsKey.housingName)
            let usrmbrID = defaults.string(forKey: userDefaultsKey.userMemberID)
            let json: [String: Any] = ["category" : "\(familyOrVisitorSeasonPass.text!)", "visitor_id" : "0", "name" : "\(nameSeasonPass!.text!)", "housing_name" : "\(housing!)",
                                       "email" : "\(email!)", "contact_no" : "\(phoneNoSeasonPass.text!)", "vehicle_type" : "\(vehicleModelSeaseonPass.text!)",
                                       "visitor_type" : "\(passType!)", "season_type" : "Single", "identification_type" : "\(identificationType.text!)",
                                       "identification_no" : "\(identificationNoSeasonPass.text!)", "purpose" : "\(purposeSeasonPass.text!)",
                                       "validity_start" : "\(startDate!)", "validity_end" : "\(endDate!)",
                                       "start_time" : "\(time!)", "end_time" : "\(time!)" ,
                                       "vehicle_no" : "\(vehicleNoSeasonPass.text!)" ,
                                       "userid" : "\(usrID!)", "subscriber_id" : "\(subID!)","address" : "\(addressSeasonPass.text!)" , "address_id" : "\(addressID!)",
                                       "status" : "ACTIVE", "upload_type" : "1" , "create_by" : "\(name!)",
                                       "modify_by" : "\(name!)" , "user_memberid" : "\(usrmbrID!)"]
            uploadImageFavourites(identificationImageDate: imageDataIdent, faceImageData: imageDataFace, to: URL(string: "\(ConstantsUsedInProject.baseUrl)add_visitors_fav")!, params: json)
        }
    }
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressDropOff.text = UIAlertAction.title
                        self.addressID = listOfAllAddress[n].addressID!
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    
    @IBAction func dropOffPressed(_ sender: CircularButton) {
        switch sender.tag {
        case 0:
            //Dropoff
            selected = 0
            dropOffVisitorOvernightScrollView.isHidden = false
            seaseonView.isHidden = true
            dropOffVisitorView.isHidden = false
            overnightStayView.isHidden = true
            dropofButtonView.isHidden = false

            break
        case 1:
            //visitorspass
            selected = 1
            dropOffVisitorOvernightScrollView.isHidden = false
            seaseonView.isHidden = true
            dropOffVisitorView.isHidden = false
            dropofButtonView.isHidden = false

            overnightStayView.isHidden = true
            break
        case 2:
            //overnightstay
            selected = 2
            dropOffVisitorOvernightScrollView.isHidden = false
            seaseonView.isHidden = true
            dropOffVisitorView.isHidden = true
            overnightStayView.isHidden = false
            dropofButtonView.isHidden = false

            break
        case 3:
            //seasonpass
            selected = 3
            dropOffVisitorOvernightScrollView.isHidden = true
            seaseonView.isHidden = false
            dropofButtonView.isHidden = true
            break
        default:
            selected = 4
        //error
        }
        for n in 0...3
        {
            if n == selected
            {
                //buttonArray[n].backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                //buttonArray[n].tintColor = .white
                buttonArray[n].alpha = 0
                imageViewArray[n].alpha = 1
                textArray[n].textColor = ConstantsUsedInProject.appThemeColor
            }
            else
            {
                buttonArray[n].alpha = 1
                imageViewArray[n].alpha = 0
                textArray[n].textColor = .darkGray
            }
        }
    }

    @IBAction func confirmDropOff(_ sender: Any) {
        switch selected {
        case 0:
            //dropoff
            dropOffVisitorsPassOvernightConfirm(type: .dropoff)
        case 1:
            //Visitors pass
            dropOffVisitorsPassOvernightConfirm(type: .visitorpass)
        case 2 :
            dropOffVisitorsPassOvernightConfirm(type: .overnightStay)
        default:
            print("error")
        }
    }
    
    @IBAction func addToFavouritesDropOff(_ sender: Any) {
        switch selected {
        case 0:
            //dropoff
            dropOffVisitorsPassOvernightConfirmFavourites(type: .dropoff)
        case 1:
            //Visitors pass
            dropOffVisitorsPassOvernightConfirmFavourites(type: .visitorpass)
        case 2 :
            dropOffVisitorsPassOvernightConfirmFavourites(type: .overnightStay)
        default:
            print("error")
        }
    }
    @IBAction func confirmSeasonPass(_ sender: Any) {
        var passType : String!
        var time : String!
        var startDate : String!
        var endDate : String!
        var empty = 0
        passType = "Season Pass"
        for n in seasonPassTextFields
        {
            empty = n.text == "" ? 1 : 0
        }
        if addressSeasonPass.text == "" || addressID == nil || purposeSeasonPass.text == ""
        {
            empty = 1
        }
     
            if startDateSeasonPass.text == "Start Date" || endDateSeasonPass.text == "End Date" || timeSeasonPass.text == "Time"
            {
                empty = 1
            }
            else
            {
                startDate = convertDateFormater(startDateSeasonPass.text!)
                endDate = convertDateFormater(endDateSeasonPass.text!)
            time = timeSeasonPass.text!
            }
        if faceImage == nil || identificationImage == nil
        {
            empty = 1
        }
        
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let imageDataFace = faceImage!.jpegData(compressionQuality: 0.50)!
            let imageDataIdent = identificationImage!.jpegData(compressionQuality: 0.50)!
            let defaults = UserDefaults.standard
            let subID = defaults.string(forKey: "subID")
            let usrID = defaults.string(forKey: "userID")
            let name = defaults.string(forKey: userDefaultsKey.name)
            let email = defaults.string(forKey: userDefaultsKey.email)
            let housing = defaults.string(forKey: userDefaultsKey.housingName)
            let usrmbrID = defaults.string(forKey: userDefaultsKey.userMemberID)

            let json: [String: Any] = ["category" : "\(familyOrVisitorSeasonPass.text!)", "visitor_id" : "0", "name" : "\(nameSeasonPass!.text!)", "housing_name" : "\(housing!)",
                                       "email" : "\(email!)", "contact_no" : "\(phoneNoSeasonPass.text!)", "vehicle_type" : "\(vehicleModelSeaseonPass.text!)",
                                       "visitor_type" : "\(passType!)", "season_type" : "Single", "identification_type" : "\(identificationType.text!)",
                                       "identification_no" : "\(identificationNoSeasonPass.text!)", "purpose" : "\(purposeSeasonPass.text!)",
                                       "validity_start" : "\(startDate!)", "validity_end" : "\(endDate!)",
                                       "start_time" : "\(time!)", "end_time" : "\(time!)" ,
                                       "vehicle_no" : "\(vehicleNoSeasonPass.text!)" ,
                                       "userid" : "\(usrID!)", "subscriber_id" : "\(subID!)","address" : "\(addressSeasonPass.text!)" , "address_id" : "\(addressID!)",
                                       "status" : "ACTIVE", "upload_type" : "1" , "create_by" : "\(name!)",
                                       "modify_by" : "\(name!)" , "user_memberid" : "\(usrmbrID!)"]
            uploadImage(identificationImageDate: imageDataIdent, faceImageData: imageDataFace, to: URL(string: "\(ConstantsUsedInProject.baseUrl)add_visitors")!, params: json)
        }
    }
    func dropOffVisitorsPassOvernightConfirmFavourites(type : newVisitorTypeEnum)
    {
        var passType : String!
        var time : String!
        var startDate : String!
        var endDate : String!
        switch type {
        case .dropoff:
            passType = "Drop Off Pick Up"
        case .overnightStay:
            passType = "Overnight Stay"
        case .visitorpass:
            passType = "Visitor Pass"
            
        }
        var empty = 0
        for n in dropOffVisitorsPassTextFields
        {
            empty = n.text == "" ? 1 : 0
        }
        if addressDropOff.text == "" || addressID == nil
        {
            empty = 1
        }
        if type == .overnightStay
        {
            if startDateOvernight.text == "Start Date" || endDateOvernight.text == "End Date" || timeOvernight.text == "Time"
            {
                empty = 1
            }
            else
            {
                startDate = convertDateFormater(startDateOvernight.text!)
                endDate = convertDateFormater(endDateOvernight.text!)
            time = timeOvernight.text!
            }
        }
        else
        {
            if dateDropOff.text == "Date" || timeDropOff.text == "Time"
            {
                empty = 1
            }
            else
            {
                startDate = convertDateFormater(dateDropOff.text!)
                endDate = convertDateFormater(dateDropOff.text!)
                time = timeDropOff.text!
            }
        }
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let defaults = UserDefaults.standard
            let subID = defaults.string(forKey: "subID")
            let usrID = defaults.string(forKey: "userID")
            let name = defaults.string(forKey: userDefaultsKey.name)
            let usrmbrID = defaults.string(forKey: userDefaultsKey.userMemberID)

            let email = defaults.string(forKey: userDefaultsKey.email)
            let housing = defaults.string(forKey: userDefaultsKey.housingName)
            let json: [String: Any] = ["category" : "\(visitorOrFamilyDropOff.text!)", "visitor_id" : "0", "name" : "\(nameDropOff.text!)", "housing_name" : "\(housing!)",
                                       "email" : "\(email!)", "contact_no" : "\(phoneNumberDropOff.text!)", "vehicle_type" : "0",
                                       "visitor_type" : "\(passType!)", "season_type" : "None", "identification_type" : "0",
                                       "identification_no" : "0", "purpose" : "\(passType!)",
                                       "validity_start" : "\(startDate!)", "validity_end" : "\(endDate!)",
                                       "start_time" : "\(time!)", "end_time" : "\(time!)" ,
                                       "vehicle_no" : "\(vehicleNumberDropOff.text!)" ,
                                       "userid" : "\(usrID!)", "subscriber_id" : "\(subID!)","address" : "\(addressDropOff.text!)" , "address_id" : "\(addressID!)",
                                       "status" : "ACTIVE", "upload_type" : "0" , "create_by" : "\(name!)",
                                       "modify_by" : "\(name!)", "user_memberid" : "\(usrmbrID!)"]
            makePostCallAddFavourites(to: URL(string: "\(ConstantsUsedInProject.baseUrl)add_visitors_fav")!, params: json)
        }
    }

    func dropOffVisitorsPassOvernightConfirm(type : newVisitorTypeEnum)
    {
        var passType : String!
        var time : String!
        var startDate : String!
        var endDate : String!
        switch type {
        case .dropoff:
            passType = "Drop Off Pick Up"
        case .overnightStay:
            passType = "Overnight Stay"
        case .visitorpass:
            passType = "Visitors Pass"
            
        }
        var empty = 0
        for n in dropOffVisitorsPassTextFields
        {
            empty = n.text == "" ? 1 : 0
        }
        if addressDropOff.text == "" || addressID == nil
        {
            empty = 1
        }
        if type == .overnightStay
        {
            if startDateOvernight.text == "Start Date" || endDateOvernight.text == "End Date" || timeOvernight.text == "Time"
            {
                empty = 1
            }
            else
            {
                startDate = convertDateFormater(startDateOvernight.text!)
                endDate = convertDateFormater(endDateOvernight.text!)
            time = timeOvernight.text!
            }
        }
        else
        {
            if dateDropOff.text == "Date" || timeDropOff.text == "Time"
            {
                empty = 1
            }
            else
            {
                startDate = convertDateFormater(dateDropOff.text!) 
                endDate = convertDateFormater(dateDropOff.text!)
                time = timeDropOff.text!
            }
        }
        if empty == 1
        {
            let alert = UIAlertController(title: "Add visitor", message: "Please enter all the details", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
        else
        {
            let defaults = UserDefaults.standard
            let subID = defaults.string(forKey: "subID")
            let usrmbrID = defaults.string(forKey: userDefaultsKey.userMemberID)

            let usrID = defaults.string(forKey: "userID")
            let name = defaults.string(forKey: userDefaultsKey.name)
            let email = defaults.string(forKey: userDefaultsKey.email)
            let housing = defaults.string(forKey: userDefaultsKey.housingName)
            let json: [String: Any] = ["category" : "\(visitorOrFamilyDropOff.text!)", "visitor_id" : "0", "name" : "\(nameDropOff.text!)", "housing_name" : "\(housing!)",
                                       "email" : "\(email!)", "contact_no" : "\(phoneNumberDropOff.text!)", "vehicle_type" : "0",
                                       "visitor_type" : "\(passType!)", "season_type" : "None", "identification_type" : "0",
                                       "identification_no" : "0", "purpose" : "\(passType!)",
                                       "validity_start" : "\(startDate!)", "validity_end" : "\(endDate!)",
                                       "start_time" : "\(time!)", "end_time" : "\(time!)" ,
                                       "vehicle_no" : "\(vehicleNumberDropOff.text!)" ,
                                       "userid" : "\(usrID!)", "subscriber_id" : "\(subID!)","address" : "\(addressDropOff.text!)" , "address_id" : "\(addressID!)",
                                       "status" : "ACTIVE", "upload_type" : "0" , "create_by" : "\(name!)",
                                       "modify_by" : "\(name!)", "user_memberid" : "\(usrmbrID!)"]
            makePostCallAddVisitors(to: URL(string: "\(ConstantsUsedInProject.baseUrl)add_visitors")!, params: json)
        }
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return  dateFormatter.string(from: date!)

        }
    func uploadImage(identificationImageDate: Data, faceImageData: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        multipart.append(data, withName: key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
            multipart.append(identificationImageDate, withName: "identification_photo", fileName: "identification_photo", mimeType: "image/png")
            multipart.append(faceImageData, withName: "face_photo", fileName: "face_photo", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddVisitorsJSON.self, from: data)

                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                self.refreshTextfields()
                                self.openPass(data: (loginBaseResponse?.visitor)!)
                                NotificationCenter.default.post(name: Notification.Name("\(notificationNames.passAddedOrUpdated)"), object: nil)
                            }
                            else
                            {
                                let alert = UIAlertController(title: "New User", message: "Unable To Add", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    
     func openPass(data : Visitor)
    {
        let storyboard = UIStoryboard(name: "VisitorPass", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "visitorPass") as! VisitorPass
        myAlert.newVisitorData = data
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
    func makePostCallAddFavourites(to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        multipart.append(data, withName: key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddVisitorsJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                self.refreshTextfields()
                                let alert = UIAlertController(title: "New Visitor", message: "Favoutire added ", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                self.dateDropOff.text = "Date"
                                self.timeDropOff.text = "Time"
                                self.startDateOvernight.text = "Start Date"
                                self.endDateOvernight.text = "End Date"
                                self.timeOvernight.text = "Time"
                                NotificationCenter.default.post(name: Notification.Name("\(notificationNames.favouriteAddedOrUpdated)"), object: nil)
                            }
                            else
                            {
                                let alert = UIAlertController(title: "New Visitor", message: "\(loginBaseResponse?.response ?? "Cant add")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

    func makePostCallAddVisitors(to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        multipart.append(data, withName: key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddVisitorsJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                self.openPass(data: (loginBaseResponse?.visitor)!)
                                NotificationCenter.default.post(name: Notification.Name("\(notificationNames.passAddedOrUpdated)"), object: nil)
                                self.dateDropOff.text = "Date"
                                self.timeDropOff.text = "Time"
                                self.startDateOvernight.text = "Start Date"
                                self.endDateOvernight.text = "End Date"
                                self.timeOvernight.text = "Time"
                                self.refreshTextfields()
                            }
                            else
                            {
                                let alert = UIAlertController(title: "New Visitor", message: "Unable To Add", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    func refreshTextfields()
    {
        for n in dropOffVisitorsPassTextFields
        {
            n.text = ""
        }
        for n in seasonPassTextFields
        {
            n.text = ""
        }
        self.dateDropOff.text = "Date"
        self.timeDropOff.text = "Time"
        self.startDateOvernight.text = "Start Date"
        self.endDateOvernight.text = "End Date"
        self.timeOvernight.text = "Time"
        startDateSeasonPass.text = "Start Date"
        endDateSeasonPass.text = "End Date"
        timeSeasonPass.text = "Time"
    }
    
    func uploadImageFavourites(identificationImageDate: Data, faceImageData: Data, to url: URL, params: [String: Any]) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        multipart.append(data, withName: key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
            multipart.append(identificationImageDate, withName: "identification_photo", fileName: "identification_photo", mimeType: "image/png")
            multipart.append(faceImageData, withName: "face_photo", fileName: "face_photo", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddVisitorsJSON.self, from: data)

                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async {
                            
                            if code_str == 200 {
                                self.refreshTextfields()
                                let alert = UIAlertController(title: "New Visitor", message: "Favoutire added ", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                                //self.openPass(data: (loginBaseResponse?.visitor)!)
                                //NotificationCenter.default.post(name: Notification.Name("\(notificationNames.passAddedOrUpdated)"), object: nil)
                            }
                            else
                            {
                                let alert = UIAlertController(title: "New Visitor", message: "\(loginBaseResponse?.response ?? "Cant add")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
}


extension NewVisitor : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""
        {
        switch textField.tag {
        case 100:
            dropOffPickerViewArray[0].text = visitoOrFamily[0]
        case 101:
            dropOffPickerViewArray[1].text = visitoOrFamily[0]
        case 102:
            dropOffPickerViewArray[2].text = identificationTypes[0]
        default:
            print("errTextDidBegin")
        }
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
    
        print("change")
        var isPickerView = false
        if textField.text != ""
        {
        switch textField.tag {
        case 100:
            isPickerView = true
        case 101:
            isPickerView = true
        case 102:
            isPickerView = true
        default:
            print("errTextDidBegin")
        }
        }
        if textField.text != "" && isPickerView && textfieldOpenFirstTime[textField.tag - 100] == false
        {
            view.endEditing(true)

        }
        else
        {
            if isPickerView
            {
            textfieldOpenFirstTime[textField.tag - 100] = false
            }
        }

    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 100:
            return 2
        case 101:
            return 2
        case 102:
            return 2
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 100:
            return visitoOrFamily[row]
        case 101:
            return visitoOrFamily[row]
        case 102:
            return identificationTypes[row]
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 100:
            dropOffPickerViewArray[0].text = visitoOrFamily[row]
        case 101:
            dropOffPickerViewArray[1].text = visitoOrFamily[row]
        case 102:
            dropOffPickerViewArray[2].text = identificationTypes[row]
        default:
            print("err")
        }
    }
}














enum newVisitorTypeEnum {
    case dropoff
    case visitorpass
    case overnightStay
    
}
extension URLEncoding {
    public func queryParameters(_ parameters: [String: Any]) -> [(String, String)] {
        var components: [(String, String)] = []
        
        for key in parameters.keys.sorted(by: <) {
            let value = parameters[key]!
            components += queryComponents(fromKey: key, value: value)
        }
        return components
    }
}


extension NewVisitor : UINavigationControllerDelegate , UIImagePickerControllerDelegate
{
    func openCamera()
    {
        
        let imagePicker = UIImagePickerController()
        
        
        imagePicker.sourceType = .camera
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        present(imagePicker, animated: true)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        guard let pickedImage = info[.originalImage] as? UIImage else {
            // imageViewPic.contentMode = .scaleToFill
            print("No image found")
            return
        }
        if selectedImageView == identificationPhotoSeasonPass
        {
            identificationImage = pickedImage
            
            print("identification")
        }
        else
        {
            if selectedImageView == facePhotoSeasonPass
            {
                faceImage = pickedImage
                print("face")
            }
        }
        selectedImageView?.image = pickedImage
    }
    func openGallery()
    {
        let imagePicker = UIImagePickerController()
        imagePicker.allowsEditing = true

        imagePicker.sourceType = .photoLibrary
        imagePicker.delegate = self
        present(imagePicker, animated: true, completion: nil)
    }
}

protocol bulkSeaseonDelegate {
func toBulk()
}
