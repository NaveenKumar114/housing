//
//  DashboardTab.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-18.
//

import UIKit

class DashboardTab: UITabBarController  , UITabBarControllerDelegate{

    @IBOutlet weak var dashTab: UITabBar!
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
      
        self.navigationController?.navigationBar.isTranslucent = false
   
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default) //UIImage.init(named: "transparent.png")
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear
      //  self.navigationController?.isNavigationBarHidden = true

    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
       // navigationController?.navigationBar.prefersLargeTitles = true
        let housing = UserDefaults.standard.string(forKey: userDefaultsKey.housingName)
        let address = UserDefaults.standard.string(forKey: userDefaultsKey.address)
        let label = UILabel()
            label.backgroundColor = .clear
            label.numberOfLines = 2
            label.textAlignment = .left
            label.textColor = .white
        label.font = UIFont(name: "Arial-Rounded", size: 12.0)
        var titleString = ""
        if address == nil
        {
            NotificationCenter.default.addObserver(self, selector: #selector(updateUser(notfication:)), name: NSNotification.Name(rawValue: "navgation"), object: nil)
            titleString = "\(housing!.uppercased())"

        }
        else
        {
            titleString = "\(housing!.uppercased())" + " \n" + "\(address!)"

        }
        let amountText = NSMutableAttributedString.init(string: titleString)

        // set the custom font and color for the 0,1 range in string
        amountText.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20),
                                  NSAttributedString.Key.foregroundColor: UIColor.white],
                                 range: NSMakeRange(0, housing!.count))
        label.attributedText = amountText
            self.navigationItem.titleView = label

       // self.navigationController?.isNavigationBarHidden = true
    }
    @objc func updateUser(notfication: NSNotification) {
        let housing = UserDefaults.standard.string(forKey: userDefaultsKey.housingName)
        let address = UserDefaults.standard.string(forKey: userDefaultsKey.address)
        let label = UILabel()
            label.backgroundColor = .clear
            label.numberOfLines = 2
            label.textAlignment = .left
            label.textColor = .white
        label.font = label.font.withSize(12)
        var titleString = ""
        if address == nil
        {
            titleString = "\(housing!.uppercased())"

        }
        else
        {
            titleString = "\(housing!.uppercased())" + " \n" + "\(address!)"

        }
        let amountText = NSMutableAttributedString.init(string: titleString)

        // set the custom font and color for the 0,1 range in string
        amountText.setAttributes([NSAttributedString.Key.font: UIFont.systemFont(ofSize: 20),
                                  NSAttributedString.Key.foregroundColor: UIColor.white],
                                 range: NSMakeRange(0, housing!.count))
        label.attributedText = amountText
            self.navigationItem.titleView = label
    }
    @IBAction func logoutPressed(_ sender: Any) {
       
        let alert = UIAlertController(title: "LogOut", message: "Are You Sure You Want To Sign Out", preferredStyle: .alert)
        
        let ok = UIAlertAction(title: "OK", style: .default, handler: { action in
            let defaults = UserDefaults.standard
 
            defaults.removeObject(forKey: "isLoggedIn")
            defaults.removeObject(forKey: "subID")
            defaults.removeObject(forKey: "userID")
            defaults.removeObject(forKey: "name")
            defaults.removeObject(forKey: userDefaultsKey.addressId)
            defaults.removeObject(forKey: userDefaultsKey.address)
            defaults.removeObject(forKey: userDefaultsKey.email)
            defaults.removeObject(forKey: userDefaultsKey.housingName)
            defaults.removeObject(forKey: userDefaultsKey.phoneNumber)
            defaults.removeObject(forKey: userDefaultsKey.memberRole)
            defaults.removeObject(forKey: userDefaultsKey.identifiactionType)
            defaults.removeObject(forKey: userDefaultsKey.identificationNumber)
            defaults.removeObject(forKey: userDefaultsKey.memberRoleText)
            defaults.removeObject(forKey: userDefaultsKey.userMemberID)
            defaults.removeObject(forKey: userDefaultsKey.profileImg)








            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            // if user is logged in before
            
            // instantiate the main tab bar controller and set it as root view controller
            // using the storyboard identifier we set earlier
            let loginController = storyboard.instantiateViewController(identifier: "login")
            (UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate)?.changeRootViewController(loginController)
        })
        alert.addAction(ok)
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: { action in
        })
        alert.addAction(cancel)
        DispatchQueue.main.async(execute: {
            self.present(alert, animated: true)
        })
    }
}
