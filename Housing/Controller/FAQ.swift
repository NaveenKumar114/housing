//
//  Services.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-12.
//

import UIKit

class FAQ: UIViewController {
    let child = SpinnerViewController()

    @IBOutlet weak var servicesTabelView: UITableView!
    @IBOutlet weak var tableViewHeight: NSLayoutConstraint!
    var titleForSection = ["1. Example"]
    var tableViewData = [
        ["No Data"]
    ]
     var hiddenSections = Set<Int>()
    override func viewDidLoad() {
        super.viewDidLoad()
        servicesTabelView.dataSource = self
        servicesTabelView.delegate = self
        servicesTabelView.register(UINib(nibName: reuseNibIdentifier.faqNibIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.faqCellIdentifier)
        let headerNib = UINib.init(nibName: "RefundPolicyView", bundle: Bundle.main)
        servicesTabelView.register(headerNib, forHeaderFooterViewReuseIdentifier: "RefundPolicyView")
        makePostCall()
        createSpinnerView()
        self.servicesTabelView.tableFooterView = UIView()


    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_faqlist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(FAQJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        titleForSection.removeAll()
                        tableViewData.removeAll()
                        removeSpinnerView()
                            if loginBaseResponse?.faqList?.count == 0
                             {
                                let alert = UIAlertController(title: "FAQ", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                self.present(alert, animated: true, completion: nil)
                               }else
                            {
                                for n in 0 ... (loginBaseResponse?.faqList!.count)! - 1
                                {
                                    titleForSection.append((loginBaseResponse?.faqList![n].question)!)
                                    
                                    tableViewData.append([(loginBaseResponse?.faqList![n].answer)!])
                                }
                            }
                     
                        servicesTabelView.reloadData()
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.servicesTabelView.addObserver(self, forKeyPath: "contentSize", options: .new, context: nil)
        self.servicesTabelView.reloadData()
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.servicesTabelView.removeObserver(self, forKeyPath: "contentSize")
    }
  
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == "contentSize"
        {
          if let newValue = change?[.newKey]
            {
                let newSize  = newValue as! CGSize
                self.tableViewHeight.constant = newSize.height
            }
            
        }
    }

}

extension FAQ : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewData.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let cell2 = servicesTabelView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.faqCellIdentifier) as! FAQCell
     //   cell2.dataLabel.text = self.tableViewData[indexPath.section][indexPath.row]
        cell.textLabel?.text = self.tableViewData[indexPath.section][indexPath.row]
        cell2.dataLabel?.text = self.tableViewData[indexPath.section][indexPath.row]
        return cell2
    }
   func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return CGFloat.init(0)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // 1
        if self.hiddenSections.contains(section) {
            return 0
        }
        
        // 2
        return self.tableViewData[section].count
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        // 1
        let sectionButton = UIButton()
        
        // 2
        sectionButton.setTitle(titleForSection[section],
                               for: .normal)
        
        // 3
        //sectionButton.titleLabel?.font =  UIFont(name: "Arial-Rounded", size: 14)!

        sectionButton.setImage(#imageLiteral(resourceName: "drop-down-arrow"), for: .normal)
        sectionButton.imageView?.tintColor = .white
        sectionButton.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        sectionButton.layer.cornerRadius = 5
        sectionButton.contentHorizontalAlignment = .left
    //sectionButton.semanticContentAttribute = .forceRightToLeft
        // 4
        let left = tableView.frame.width - 25
        sectionButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: left, bottom: 10, right: 10);

        sectionButton.tag = section
        
        // 5
        sectionButton.addTarget(self,
                                action: #selector(self.hideSection(sender:)),
                                for: .touchUpInside)
        return sectionButton
    }
  
    @objc
    private func hideSection(sender: UIButton) {
        let section = sender.tag
        
        func indexPathsForSection() -> [IndexPath] {
            var indexPaths = [IndexPath]()
            
            for row in 0..<self.tableViewData[section].count {
                indexPaths.append(IndexPath(row: row,
                                            section: section))
            }
            
            return indexPaths
        }
        
        if self.hiddenSections.contains(section) {
            self.hiddenSections.remove(section)
            self.servicesTabelView.insertRows(at: indexPathsForSection(),
                                      with: .fade)
        } else {
            self.hiddenSections.insert(section)
            self.servicesTabelView.deleteRows(at: indexPathsForSection(),
                                      with: .fade)
        }
    }
   
}

