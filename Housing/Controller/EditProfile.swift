//
//  EditProfile.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-18.
//

import UIKit

class EditProfile: UIViewController {

    @IBOutlet weak var role: UITextField!
    @IBOutlet weak var identificationNumber: UITextField!
    @IBOutlet weak var identificationType: UITextField!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var contactNumber: UITextField!
    var newFamilyTextFields = [UITextField]()
    var visitoOrFamily = ["Family" , "Tenant" , "Admin"]
    var identificationTypes = ["NRIC" , "Passport"]
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "My Profile"
        let defaults = UserDefaults.standard

        
        email.text = defaults.string(forKey: userDefaultsKey.email) ?? ""
        identificationType.text = defaults.string(forKey: userDefaultsKey.identifiactionType) ?? ""
        identificationNumber.text = defaults.string(forKey: userDefaultsKey.identificationNumber) ?? ""
            role.text = defaults.string(forKey: userDefaultsKey.memberRoleText) ?? ""
        
        
        
        userName.text = defaults.string(forKey: userDefaultsKey.name) ?? ""
        contactNumber.text = defaults.string(forKey: userDefaultsKey.phoneNumber) ?? ""
        
        for textField in 0 ... newFamilyTextFields.count - 1
        {
            newFamilyTextFields[textField].delegate = self
            createPickerView(textField: newFamilyTextFields[textField], tag: textField)
            
        }
        dismissPickerView()
    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag
        textField.tintColor = UIColor.clear
        pickerView.tag = tag
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in newFamilyTextFields
        {
            textField.inputAccessoryView = toolBar
        }
    }
    
    @objc func action() {
        view.endEditing(true)
    }
   /* func makePostCallUpdateUsers() {
        let defaults = UserDefaults.standard
        var sub = defaults.string(forKey: "subID")
        var usr = defaults.string(forKey: "userID")
        var usrMberID = defaults.string(forKey: userDefaultsKey.userMemberID)
        if let safeDate = editUserData
        {
            sub = safeDate.subscriberID
            usr = safeDate.userid
            usrMberID = safeDate.userMemberid
        }
        let name = defaults.string(forKey: userDefaultsKey.name)
        let housing = defaults.string(forKey: userDefaultsKey.housingName)
        let snippet = address
        let range = snippet!.range(of: ",")
        let addressWithoutNumber = snippet![range!.upperBound...].trimmingCharacters(in: .whitespaces)
        let decoder = JSONDecoder()
       // let json: [String: Any] = ["address":"\(addressWithoutNumber)","address_id":"\(addressID!)","subscriber_id":"\(sub!)","userid":"\(usr!)" , "roletype":"\(roleType)"]
        let json: [String: Any] = ["member_address":"\(addressWithoutNumber)","member_address_id":"\(addresId!)","member_contact":"\(addUserContact.text!)","member_email":"\(addUserEmail.text!)","member_housingname":"\(housing!)","member_identification_type":"\(addUserIdetificationType.text!)","member_indentification_no":"\(addUserIdentificationNumber.text!)","member_name":"\(addUserUserName.text!)","member_role":"\(UserDefaults.standard.string(forKey: userDefaultsKey.memberRole)!)","member_status":"ACTIVE","member_type":"\(addUserFamilyOrTentant.text!)","subscriber_id":"\(sub!)","user_role":"\(addUserFamilyOrTentant.text!)","user_memberid":"\(usrMberID!)","code":0,"create_by":"\(name!)","modify_by":"\(name!)" , "userid":"\(usr!)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)update_usermembers")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                   // print(String(data: data!, encoding: String.Encoding.utf8))
                    let loginBaseResponse = try? decoder.decode(UpdateUserJSON.self, from: data!)
                    //print(loginBaseResponse)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            NotificationCenter.default.post(name: Notification.Name("\(notificationNames.userAddedORUpdated)"), object: nil)
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            //print(self.userList?.usermembersList)
                        }else  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                          
                          
                                let alert = UIAlertController(title: "Add Users", message: "\(loginBaseResponse?.response ?? "")", preferredStyle: UIAlertController.Style.alert)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            
                            // add an action (button)
                           
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    } */
}

extension EditProfile : UIPickerViewDelegate , UIPickerViewDataSource , UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        switch textField.tag {
        case 0:
            newFamilyTextFields[0].text = identificationTypes[0]
        case 1:
            newFamilyTextFields[1].text = visitoOrFamily[0]
        default:
            print("err")
        }
    }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 0:
            return 2
        case 1:
            return 3
        case 2:
            return 2
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 0:
            return identificationTypes[row]
        case 1:
            return visitoOrFamily[row]
    
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 0:
            newFamilyTextFields[0].text = identificationTypes[row]
        case 1:
            newFamilyTextFields[1].text = visitoOrFamily[row]
        default:
            print("err")
        }
    }
}
