//
//  Users.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-08.
//

import UIKit

class Users: UIViewController {
    var addressID : String?
    var addressList : AddressJSON?
    var userList : UserListJSON?
    var currentRoleType = "Family"
    let child = SpinnerViewController()

    @IBOutlet weak var segmentControllerk: UISegmentedControl!
    @IBOutlet weak var address: UILabel!
    @IBOutlet weak var usersListTableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Users"
        address.font = UIFont(name: "Arial-Rounded", size: 15.0)

        segmentControllerk.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        segmentControllerk.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        usersListTableView.delegate = self
        usersListTableView.dataSource = self
        self.usersListTableView.tableFooterView = UIView()

        makePostCallAddress()
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            address.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            addressID = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            makePostCallUsers(roleType: currentRoleType)
            createSpinnerView()

        }
        let gestureAddressSeason = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        address.addGestureRecognizer(gestureAddressSeason)
        usersListTableView.register(UINib(nibName: reuseNibIdentifier.UsersListCellNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.UsersListCellIdentifier)
        NotificationCenter.default.addObserver(self, selector: #selector(updateUser(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.userAddedORUpdated)"), object: nil)


    }
    func createSpinnerView() {
            addChild(child)
            child.view.frame = view.frame
            view.addSubview(child.view)
            child.didMove(toParent: self)
        }
        func removeSpinnerView()
        {
            child.willMove(toParent: nil)
            child.view.removeFromSuperview()
            child.removeFromParent()
        }
    @objc func updateUser(notfication: NSNotification) {
        userList = nil
        makePostCallUsers(roleType: currentRoleType)
    }
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.address.text = UIAlertAction.title
                        self.addressID = listOfAllAddress[n].addressID!
                        makePostCallUsers(roleType: currentRoleType)
                        //print(listOfAllAddress[n])
                       // print(addressID)
                      //  print(n)
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    
    @IBAction func userOrFamilySegment(_ sender: UISegmentedControl) {
        switch sender.selectedSegmentIndex
        {
        case 0:
            print("0")
            currentRoleType = "Family"
            makePostCallUsers(roleType: currentRoleType)
        case 1:
            currentRoleType = "Tenant"
            makePostCallUsers(roleType: currentRoleType)
        default:
            print("error")
        }
    }
    
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Users", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    
    func makePostCallUsers(roleType : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let role = defaults.string(forKey: "\(userDefaultsKey.memberRole)")

        let snippet = address.text!
        let range = snippet.range(of: ",")
        let addressWithoutNumber = snippet[range!.upperBound...].trimmingCharacters(in: .whitespaces)
        let decoder = JSONDecoder()
        let json: [String: Any] = ["address":"\(addressWithoutNumber)","address_id":"\(addressID!)","subscriber_id":"\(sub!)","userid":"\(usr!)" , "roletype":"\(roleType)" , "role":"\(role!)"]
        print(json)
        
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)get_usermemberslist")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(UserListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {
                            self.userList = nil
                            self.usersListTableView.reloadData()

                            self.userList = loginBaseResponse!
                            
                            self.usersListTableView.reloadData()
                            //print(self.userList?.usermembersList)
                            self.removeSpinnerView()
                            if loginBaseResponse?.usermembersList?.count == 0
                                    {
                                       let alert = UIAlertController(title: "Users", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                       alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                       self.present(alert, animated: true, completion: nil)
                                      }
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Users", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    
}

extension Users : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return userList?.usermembersList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell2 = usersListTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.UsersListCellIdentifier) as! UsersListCell
        cell2.nameLabel.text = userList?.usermembersList?[indexPath.row].memberName ?? ""
        cell2.userImageView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        cell2.dummyView.cropAsCircleWithBorder(borderColor: .white, strokeWidth: 0)
        cell2.dummyView.isHidden = false
        cell2.userImageView.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
       // cell2.imageView?.image = UIImage(named: "loading_profile")
        print(cell2.imageView?.image)
        if userList?.usermembersList?[indexPath.row].memberStatus ?? "" == "BLOCK"
        {
            cell2.userImageView.backgroundColor = .red
            cell2.dummyView.backgroundColor = .red
        }
        else
        {
            cell2.userImageView.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
            cell2.dummyView.backgroundColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        }
        if userList?.usermembersList?[indexPath.row].profileImg != nil
        {
            if userList?.usermembersList?[indexPath.row].profileImg != ""
            {
                print("out1")
            if let img = userList?.usermembersList?[indexPath.row].profileImg
            {
                print("out2")
                print("imagelod")
                print(userList?.usermembersList?[indexPath.row].profileImg)
                let urlStr = ("\(ConstantsUsedInProject.baseImgUrl)users/profile/\(img)")
                print(urlStr)
                let url = URL(string: urlStr)
                
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async { [self] in
                    if   let image = UIImage(data: data!)
                    {
           //             print(userList?.usermembersList?[indexPath.row].memberName)
                      
                        //profileButton.setImage(image, for: .normal)
                        cell2.userImageView.image = image
                        cell2.userImageView.layer.cornerRadius = 0.5 * cell2.userImageView.bounds.size.width
                        cell2.userImageView.clipsToBounds = true
                        cell2.userImageView.layer.borderWidth = 1
                        cell2.userImageView.contentMode = .scaleToFill
                        cell2.dummyView.isHidden = true
                        if userList?.usermembersList?.count != indexPath.row
                       {
                        if userList?.usermembersList?[indexPath.row].memberStatus ?? "" == "BLOCK"
                        {
                            cell2.userImageView.layer.borderColor =  UIColor.systemRed.cgColor

                        }
                        else
                        {
                            cell2.userImageView.layer.borderColor =  UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor

                        }
                       }
                    }
                    }
                }
            }
        }
        }
        cell2.accessoryType = .disclosureIndicator

        return cell2
    }

        
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
        if userList?.usermembersList?[indexPath.row].memberRole != "1"
        {
        let status = userList?.usermembersList?[indexPath.row].memberStatus ?? ""
        let memberID = userList?.usermembersList?[indexPath.row].userMemberid ?? ""
        var titleForSwipe = "BLOCK"
        var colorForTitle : UIColor = .red
        if status == "BLOCK"
        {
            titleForSwipe = "ACTIVE"
            colorForTitle = UIColor(named: ConstantsUsedInProject.appThemeColorName)!
        }
        let action = UIContextualAction(style: .normal, title:  titleForSwipe, handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                print("Update action ...")
                success(true)
            print(self.userList?.usermembersList?[indexPath.row].memberName ?? "")
            print(self.userList?.usermembersList?[indexPath.row].userMemberid ?? "")
            if titleForSwipe == "BLOCK"
            {
                
                self.makePostCallBlockOrAllow(status: "BLOCK", memberID: memberID)
            }
            else
            {
                self.makePostCallBlockOrAllow(status: "ACTIVE", memberID: memberID)
            }
            
            })
            action.backgroundColor = colorForTitle

            return UISwipeActionsConfiguration(actions: [action])
        }
        else
        {
            return nil
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: segueIdentifiers.userToShowUser, sender: indexPath)
        usersListTableView.deselectRow(at: indexPath, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.userToShowUser
        {
            let indexPath = sender as! IndexPath
            let data = userList?.usermembersList?[indexPath.row]
            let vc = segue.destination as! ShowUser
            vc.userDetail = data
            print("showUser")
            
        }
        if segue.identifier == segueIdentifiers.userToAddUser
        {
            let vc = segue.destination as! NewFamilyUser
            vc.address = address.text
            vc.addresId = addressID
        }
    }
    func makePostCallBlockOrAllow(status : String , memberID : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["address_id":"\(addressID!)","subscriber_id":"\(sub!)","userid":"\(usr!)" , "roletype":"\(currentRoleType)" ,"status" : "\(status)" , "user_memberid":"\(memberID)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)update_tenant_status")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(UserListJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async {
                        
                        if code_str == 200 {

                            self.makePostCallUsers(roleType: self.currentRoleType)
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Users", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
}

extension UIView
{
    func cropAsCircleWithBorder(borderColor : UIColor, strokeWidth: CGFloat)
    {
        var radius = min(self.bounds.width, self.bounds.height)
        var drawingRect : CGRect = self.bounds
        drawingRect.size.width = radius
        drawingRect.origin.x = (self.bounds.size.width - radius) / 2
        drawingRect.size.height = radius
        drawingRect.origin.y = (self.bounds.size.height - radius) / 2
        
        radius /= 2
        
        var path = UIBezierPath(roundedRect: drawingRect.insetBy(dx: strokeWidth/2, dy: strokeWidth/2), cornerRadius: radius)
        let border = CAShapeLayer()
        border.fillColor = UIColor.clear.cgColor
        border.path = path.cgPath
        border.strokeColor = borderColor.cgColor
        border.lineWidth = strokeWidth
        self.layer.addSublayer(border)
        
        path = UIBezierPath(roundedRect: drawingRect, cornerRadius: radius)
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
    }
}
