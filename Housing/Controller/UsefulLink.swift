//
//  UsefulLink.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-21.
//

import UIKit

class UsefulLink: UIViewController {
    var usefulLinkData : UsefulLinkJSON?
    var refreshControl = UIRefreshControl()
    let child = SpinnerViewController()

    @IBOutlet weak var usefulLinkTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()

        makePostCall()
        usefulLinkTable.delegate = self
        usefulLinkTable.dataSource = self
        createSpinnerView()
        self.usefulLinkTable.tableFooterView = UIView()



    }
    
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }

    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_usefullinks")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(UsefulLinkJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        usefulLinkData = loginBaseResponse
                        usefulLinkTable.reloadData()
                        removeSpinnerView()
                         if loginBaseResponse?.usefullinkList?.count == 0
                          {
                             let alert = UIAlertController(title: "Useful Links", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                             alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                             self.present(alert, animated: true, completion: nil)
                            }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }

}

extension UsefulLink : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return usefulLinkData?.usefullinkList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        if let data = usefulLinkData?.usefullinkList
        {
            cell.textLabel?.text = data[indexPath.row].name ?? ""
            cell.textLabel?.font = UIFont(name: "Arial-Rounded", size: 15.0)

            cell.textLabel?.textColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let x = usefulLinkData?.usefullinkList?[indexPath.row]
        performSegue(withIdentifier: segueIdentifiers.usefulInfoSingle, sender: x)
        usefulLinkTable.deselectRow(at: indexPath, animated: true)

    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
            let data = sender as! UsefullinkList
            let destinationVC = segue.destination as! ViewParticularUsefulInfo
            destinationVC.usefulData = data
        
        
    }
}

