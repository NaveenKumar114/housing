//
//  SeasonPass.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-15.
//

import UIKit

class SeasonPass: UIViewController {

    @IBOutlet weak var singleOrSeasonSegment: UISegmentedControl!
    override func viewDidLoad() {
        super.viewDidLoad()
        singleOrSeasonSegment.layer.cornerRadius = singleOrSeasonSegment.frame.height / 2
        singleOrSeasonSegment.layer.borderWidth = 2
        singleOrSeasonSegment.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
        singleOrSeasonSegment.clipsToBounds = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
