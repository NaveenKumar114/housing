//
//  BulkSeaseonPass.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-30.
//

import UIKit
import Alamofire
import DatePickerDialog

class BulkSeaseonPass: UIViewController, addBulkUserProtocol , UITextFieldDelegate {
    func addUser(data: seaseonBulkDataType) {
        print(data)
        seasonPassEntries.append(data)
        seaseonPassTableView.reloadData()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
  
    var typeOptions = ["Contractor" , "Renovator" , "Mover" , "Delivery" , "Teacher" , "Family Member" , "Staff" , "House Agent" , "Driver" , "Doctor" , "Part Time Maid" , "Live-In Maid" , "Babysitter" , "Care Taker" , "Other"]
    var addressList : AddressJSON?
    var textfieldOpenFirstTime = [true]

 
    @IBOutlet weak var seaseonPassTableView: UITableView!
    @IBOutlet weak var companyName: UITextField!
    @IBOutlet weak var email: UITextField!
    @IBOutlet weak var type: UITextField!
    @IBOutlet weak var startDate: UILabel!
    @IBOutlet weak var startTime: UILabel!
    @IBOutlet weak var endTime: UILabel!
    
    @IBOutlet weak var endDate: UILabel!
    @IBOutlet weak var phoneNumber: UITextField!
    @IBOutlet weak var purpose: UITextField!
    @IBOutlet weak var name: UITextField!
    @IBOutlet weak var address: UILabel!
    var pickerViewArray = [UITextField]()

    var addressID : String?
    var seasonPassEntries = [seaseonBulkDataType]()
    var progressAlert : UIAlertController!
    var progressViewAlert : UIProgressView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        purpose.delegate = self
        purpose.tag = 10
        purpose.font = UIFont(name: "Arial-Rounded", size: 15.0)
        makePostCallAddress()
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            address.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            addressID = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
        }
        let defaults = UserDefaults.standard
        companyName.text = defaults.string(forKey: userDefaultsKey.housingName)
        companyName.font = UIFont(name: "Arial-Rounded", size: 15.0)
        email.text = defaults.string(forKey: userDefaultsKey.email)
        email.font = UIFont(name: "Arial-Rounded", size: 15.0)
        phoneNumber.text = defaults.string(forKey: userDefaultsKey.phoneNumber)
        phoneNumber.font = UIFont(name: "Arial-Rounded", size: 15.0)
        name.text = defaults.string(forKey: userDefaultsKey.name)
        name.font = UIFont(name: "Arial-Rounded", size: 15.0)
        seaseonPassTableView.delegate = self
        seaseonPassTableView.dataSource = self
        seaseonPassTableView.register(UINib(nibName: reuseNibIdentifier.bulkSeasonListNibIdentifier, bundle: nil), forCellReuseIdentifier: reuseCellIdentifier.bulkSeasonListCellIdentifier)
        progressAlert = UIAlertController(title: "Uploading Images", message: "1/2", preferredStyle: .alert)

        progressViewAlert = UIProgressView(progressViewStyle: .default)

        progressViewAlert.setProgress(1.0/10.0, animated: true)
        progressViewAlert.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

        progressAlert.view.addSubview(progressViewAlert)

        //progressAlert.dismiss(animated: true, completion: nil)
        pickerViewArray = [type]
        type.font = UIFont(name: "Arial-Rounded", size: 15.0)
        for textField in 0 ... pickerViewArray.count - 1
        {
            pickerViewArray[textField].delegate = self
            createPickerView(textField: pickerViewArray[textField], tag: textField)
            
        }
        dismissPickerView()
        let gestureAddressSeason = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        address.addGestureRecognizer(gestureAddressSeason)
        let gesture6 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        endDate.addGestureRecognizer(gesture6)
        let gesture7 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        startDate.addGestureRecognizer(gesture7)
        let gesture8 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        endTime.addGestureRecognizer(gesture8)
        let gesture9 = UITapGestureRecognizer(target: self, action: #selector(showDateOrTime(sender:)))
        startTime.addGestureRecognizer(gesture9)
        self.seaseonPassTableView.tableFooterView = UIView()

    }
    func createPickerView(textField : UITextField , tag : Int) {
        let pickerView = UIPickerView()
        pickerView.delegate = self
        textField.inputView = pickerView
        textField.tag = tag + 100
        textField.tintColor = UIColor.clear
        pickerView.tag = tag + 100
    }
    func dismissPickerView() {
        let toolBar = UIToolbar()
        toolBar.sizeToFit()
        let button = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.action))
        toolBar.setItems([button], animated: true)
        toolBar.isUserInteractionEnabled = true
        for textField in pickerViewArray
        {
            textField.inputAccessoryView = toolBar
        }
    }
    
    @objc func action() {
        view.endEditing(true)
    }
    @objc func showDateOrTime(sender: UIGestureRecognizer)
    {
        let x = sender.view as? UILabel
        switch x {
        case startDate , endDate:
            datePickerTapped(sender: x!)
        case startTime , endTime:
            timePickerTapped(sender: x!)
        default:
            print("error in time date")
        }
        
    }
    func datePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Date", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .date) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                sender.text = formatter.string(from: dt)
                // print(formatter.string(from: dt))
            }
        }
        
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "yyyy-MM-dd"
            return  dateFormatter.string(from: date!)

        }
    func timePickerTapped(sender : UILabel) {
        DatePickerDialog().show("Time", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", defaultDate: Date(), minimumDate: nil, maximumDate: nil, datePickerMode: .time) { (date) in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "H:mm"
                //print(formatter.string(from: dt))
                sender.text = formatter.string(from: dt)
                
            }
        }
        
    }
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.address.text = UIAlertAction.title
                        self.addressID = listOfAllAddress[n].addressID!

                        print(n)
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    
    @IBAction func addNewPass(_ sender: Any) {
        openAlert()
        
    }
    @IBAction func submitForApprovalPressed(_ sender: Any) {
        if seasonPassEntries.count != 0
        {
            if type.text == "" || purpose.text == "" || startDate.text == "Start Date" || startTime.text == "Time" || endTime.text == "Time" || endDate.text == "End Date"
            {
                let alert = UIAlertController(title: "Season Pass", message: "Please enter all details", preferredStyle: UIAlertController.Style.alert)
                
                // add an action (button)
                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                
                // show the alert
                self.present(alert, animated: true, completion: nil)
            }
            else
            {
                prepareForUpload(index: 0, alertDisplayed: false)
            }
        }
        else
        {
            let alert = UIAlertController(title: "Season Pass", message: "Please add some pass holders", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    func prepareForUpload(index : Int , alertDisplayed : Bool)
    {
        let x = seasonPassEntries[index]
        let imageDataFace = x.facePhoto.jpegData(compressionQuality: 0.50)!
        let imageDataIdent = x.proofPhoto.jpegData(compressionQuality: 0.50)!
        let defaults = UserDefaults.standard
        let subID = defaults.string(forKey: "subID")
        let usrID = defaults.string(forKey: "userID")
        let name = defaults.string(forKey: userDefaultsKey.name)
        let email = defaults.string(forKey: userDefaultsKey.email)
        let housing = defaults.string(forKey: userDefaultsKey.housingName)
        let vehicleType = x.vehicleModel
        let identType = x.identifiationType
        let usrmbrID = defaults.string(forKey: userDefaultsKey.userMemberID)

        let json: [String: Any] = ["category" : "\(type.text!)", "visitor_id" : "0", "name" : "\(x.name)", "housing_name" : "\(housing!)",
                                   "email" : "\(email!)", "contact_no" : "\(x.phoneNumber)", "vehicle_type" : "\(vehicleType)",
                                   "visitor_type" : "Season Pass", "season_type" : "Single", "identification_type" : "\(identType)",
                                   "identification_no" : "\(x.icOrPassport)", "purpose" : "Season Pass",
                                   "validity_start" : "\(convertDateFormater(startDate.text!))", "validity_end" : "\(convertDateFormater(endDate.text!))",
                                   "start_time" : "\(startTime.text!)", "end_time" : "\(endTime.text!)" ,
                                   "vehicle_no" : "\(x.vehicleNumber)" ,
                                   "userid" : "\(usrID!)", "subscriber_id" : "\(subID!)","address" : "\(address.text!)" , "address_id" : "\(addressID!)",
                                   "status" : "ACTIVE", "upload_type" : "1" , "create_by" : "\(name!)",
                                   "modify_by" : "\(name!)" , "user_memberid" : "\(usrmbrID!)"]
        progressAlert.message  = "\(index + 1) of \(seasonPassEntries.count)"

        if !alertDisplayed
        {
            present(progressAlert, animated: true, completion: nil)
        }

        uploadImage(identificationImageDate: imageDataIdent, faceImageData: imageDataFace, to: URL(string: "\(ConstantsUsedInProject.baseUrl)add_visitors")!, params: json, index: index)
    }
    func downloadAlert() {
        let alertController = UIAlertController(title: "Title", message: "Loading...", preferredStyle: .alert)

        let progressDownload : UIProgressView = UIProgressView(progressViewStyle: .default)

            progressDownload.setProgress(3.0/10.0, animated: true)
            progressDownload.frame = CGRect(x: 10, y: 70, width: 250, height: 0)

        alertController.view.addSubview(progressDownload)
        present(alertController, animated: true, completion: nil)
        
        
    }
    @objc func openAlert()
    {
        let storyboard = UIStoryboard(name: "NewVisitor", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "bulk")  as! NewBulkPassHolder
        myAlert.delegate = self
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
    func uploadImage(identificationImageDate: Data, faceImageData: Data, to url: URL, params: [String: Any] , index : Int) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if let data = value.data(using: .utf8) {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        multipart.append(data, withName: key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
            multipart.append(identificationImageDate, withName: "identification_photo", fileName: "identification_photo", mimeType: "image/png")
            multipart.append(faceImageData, withName: "face_photo", fileName: "face_photo", mimeType: "image/png")
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                    self.progressViewAlert.progress = Float(progress.fractionCompleted)
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddVisitorsJSON.self, from: data)

                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8))
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                                print("success")
                                if index == seasonPassEntries.count - 1
                                {
                                    progressAlert.dismiss(animated: true, completion: nil)
                                    let alert = UIAlertController(title: "Season Pass", message: "Successfully Added", preferredStyle: UIAlertController.Style.alert)
                                    
                                    // add an action (button)
                                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                    
                                    // show the alert
                                    self.present(alert, animated: true, completion: nil)
                                    NotificationCenter.default.post(name: Notification.Name("\(notificationNames.passAddedOrUpdated)"), object: nil)
                                }
                                else
                                {
                                    prepareForUpload(index: index + 1, alertDisplayed: true)
                                }
                                
                            }
                            else
                            {
                                let alert = UIAlertController(title: "Season Pass", message: "Unable To Update", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }

}

extension BulkSeaseonPass : UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return seasonPassEntries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = seaseonPassTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.bulkSeasonListCellIdentifier) as! BulkPassListCell
        cell.mainView.layer.borderWidth = 1
        cell.mainView.layer.borderColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)?.cgColor
        cell.mainView.layer.cornerRadius = 20
        let entryData = seasonPassEntries[indexPath.row]
        cell.name.text = entryData.name
        cell.email.text = entryData.email
        cell.phoneNumber.text = entryData.phoneNumber
        cell.facePhoto.image = entryData.facePhoto
        cell.icOrPassport.text = entryData.icOrPassport
        cell.proofPhoto.image = entryData.proofPhoto
        cell.vehicle.text = "\(entryData.vehicleModel) - \(entryData.vehicleNumber)"
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
    
    
}


extension BulkSeaseonPass : UIPickerViewDelegate , UIPickerViewDataSource 
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField.text == ""
        {
        switch textField.tag {
        case 100:
            pickerViewArray[0].text = typeOptions[0]

        default:
            print("err")
        }
        }
    }
    func textFieldDidChangeSelection(_ textField: UITextField) {
       
           print("change")
           var isPickerView = false
           if textField.text != ""
           {
           switch textField.tag {
           case 100:
               isPickerView = true
           default:
               print("errTextDidBegin")
           }
           }
           if textField.text != "" && isPickerView && textfieldOpenFirstTime[textField.tag - 100] == false
           {
               view.endEditing(true)

           }
           else
           {
               if isPickerView
               {
               textfieldOpenFirstTime[textField.tag - 100] = false
               }
           }

       }
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch pickerView.tag {
        case 100:
            return typeOptions.count
        case 101:
            return 2
        default:
            return 0
        }
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 100:
            return typeOptions[row]
        case 1:
            return typeOptions[row]
    
        default:
            return ""
        }
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch pickerView.tag {
        case 100:
            pickerViewArray[0].text = typeOptions[row]

        default:
            print("err")
        }
    }
}
