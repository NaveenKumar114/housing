//
//  VisitorRecordSecurity.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-06.
//

import UIKit

class VisitorRecordSecurity: UIViewController {

    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var visitorRecordTableView: UITableView!
    var addressList : AddressJSON?
    var visitorRecordData : VisitorRecordUserJSON?
    override func viewDidLoad() {
        super.viewDidLoad()
        visitorRecordTableView.delegate = self
        visitorRecordTableView.dataSource = self
        visitorRecordTableView.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        visitorRecordTableView.register(UINib(nibName: reuseNibIdentifier.visitorRecordNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.visitorRecordCellIdentifier)
        makePostCallVisitorRecord(addressID: "0")
        self.visitorRecordTableView.tableFooterView = UIView()


    }
    
    func makePostCallVisitorRecord(addressID : String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_visitors")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(VisitorRecordUserJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.visitorRecordData = loginBaseResponse
                        self.visitorRecordTableView.reloadData()
                        if loginBaseResponse?.visitorList?.count == 0
                         {
                            let alert = UIAlertController(title: "Visitor Record", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                           }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    }
 

}

extension VisitorRecordSecurity: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = visitorRecordTableView.dequeueReusableCell(withIdentifier: reuseCellIdentifier.visitorRecordCellIdentifier) as! VisitorRecordCell
        if let safeData = visitorRecordData?.visitorList?[indexPath.row]
        {
            cell.name.text = safeData.name
            cell.date.text = safeData.validityStart
            cell.status.text = safeData.status
            cell.type.text = safeData.purpose
            cell.vehicleNo.text = safeData.vehicleNo
            cell.inDate.text = safeData.validityStart
            cell.outDate.text = safeData.validityEnd
            cell.inTime.text = safeData.inTime
            cell.outTime.text = safeData.outTime
            if safeData.status == "PENDING"
            {
                cell.status.textColor = .red
            }
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return visitorRecordData?.visitorList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        openPass(index: indexPath)
    }
    @objc func openPass(index : IndexPath)
    {
        let storyboard = UIStoryboard(name: "VisitorPass", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "visitorPass") as! VisitorPass
        myAlert.visitorHistoryData = visitorRecordData?.visitorList?[index.row]
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }

    
    
}
