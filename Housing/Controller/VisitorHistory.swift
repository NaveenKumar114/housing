//
//  VisitorHistory.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-12.
//

import UIKit
import Alamofire
class VisitorHistory: UIViewController  {


    let child = SpinnerViewController()
    var refreshControl = UIRefreshControl()

    @IBOutlet weak var historySegment: UISegmentedControl!
    @IBOutlet weak var calendarIcon: UIImageView!
    @IBOutlet weak var addressLabel: UILabel!
    var addressList : AddressJSON?
    var visitorRecordData : VisitorRecordUserJSON?
    var addressIdSelected : String?
    var historyDate : String?
    var historyStatusSelected  = "ALL"
    let expiryDatePicker = MonthYearPickerView()
    let toolBar = UIToolbar.init(frame: CGRect.init(x: 0.0, y: UIScreen.main.bounds.size.height - 400, width: UIScreen.main.bounds.size.width, height: 50))
    @IBOutlet weak var historyTabel: UITableView!
    var activeRecord = [VisitorList]()
    var reachedRecord = [VisitorList]()
    var expiredRecord = [VisitorList]()

    override func viewDidLoad() {
        super.viewDidLoad()
        createSpinnerView()

        historyTabel.delegate = self
        historyTabel.dataSource = self
        historyTabel.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        historyTabel.register(UINib(nibName: reuseNibIdentifier.historyNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.historyCellIdentifier)
        makePostCallAddress()
        historySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: UIColor.white], for: .selected)
        historySegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: ConstantsUsedInProject.appThemeColor], for: .normal)
        let gesture = UITapGestureRecognizer(target: self, action: #selector(showAddress))
        addressLabel.addGestureRecognizer(gesture)
        addressLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)
        let month = Calendar.current.component(.month, from: Date())
        let year = Calendar.current.component(.year, from: Date())
        var m = "\(month)"
        if m.count == 1
        {
            print("1")
            m = "0\(month)"
        }
        historyDate = "\(year)-\(m)"
        if UserDefaults.standard.string(forKey: userDefaultsKey.addressId) != nil
        {
            addressLabel.text = UserDefaults.standard.string(forKey: userDefaultsKey.address)
            let addressId = UserDefaults.standard.string(forKey: userDefaultsKey.addressId)
            addressIdSelected = addressId!

            makePostCallVisitorRecord(addressID: addressIdSelected! , status: historyStatusSelected , date: historyDate!)
            
        }
        makePostCallAddress()
        NotificationCenter.default.addObserver(self, selector: #selector(updateUser(notfication:)), name: NSNotification.Name(rawValue: "\(notificationNames.passAddedOrUpdated)"), object: nil)
        self.historyTabel.tableFooterView = UIView()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
           refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
         historyTabel.addSubview(refreshControl)
        let gestureCalendar = UITapGestureRecognizer(target: self, action: #selector(dateAndYearPicker))
        calendarIcon.addGestureRecognizer(gestureCalendar)


    }
    @objc func dateAndYearPicker()
    {
        let month = Calendar.current.component(.month, from: Date())
        let year = Calendar.current.component(.year, from: Date())
        var m = "\(month)"
        if m.count == 1
        {
            print("1")
            m = "0\(month)"
        }
        historyDate = "\(year)-\(m)"
        expiryDatePicker.backgroundColor = UIColor.white
           expiryDatePicker.setValue(UIColor.black, forKey: "textColor")
           expiryDatePicker.autoresizingMask = .flexibleWidth
           expiryDatePicker.contentMode = .center
        expiryDatePicker.frame = CGRect.init(x: 0.0, y: NewVisitor.screenSize - 500, width: UIScreen.main.bounds.size.width, height: 500)
           self.view.addSubview(expiryDatePicker)

           toolBar.barStyle = .default
           toolBar.isTranslucent = true
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(self.action))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: nil)

           toolBar.setItems([spaceButton, doneButton], animated: false)
           toolBar.isUserInteractionEnabled = true
           self.view.addSubview(toolBar)

           //DISABLE RIGHT ITEM & LEFT ITEM
          //        disableCancelAndSaveItems()

           //DISABLED SELECTION FOR ALL CELLS
          //        ableToSelectCellsAndButtons(isAble: false)

           //DISABLE RIGHT ITEM & LEFT ITEM
          // isEnableCancelAndSaveItems(isEnabled: false)

           //SHOW GREY BACK GROUND
          // showGreyOutView(isShow: true)
           expiryDatePicker.onDateSelected = { (month: Int, year: Int) in
              // self.expiredDetailOutlet.text = string
            var m = "\(month)"
            if m.count == 1
            {
                print("1")
                m = "0\(month)"
            }
            self.historyDate = "\(year)-\(m)"

               
           }
        
    }
    func checkStatus()
    {
        activeRecord.removeAll()
        expiredRecord.removeAll()
        reachedRecord.removeAll()
        if visitorRecordData?.visitorList!.count != 0
        {
        for n in 0 ... (visitorRecordData?.visitorList!.count)! - 1
        {
            if let safeData = visitorRecordData?.visitorList?[n]
            {
                if safeData.status == "ACTIVE"
                {
                    activeRecord.append(safeData)
                }
                if safeData.status == "REJECTED"
                {
                    expiredRecord.append(safeData)
                }
                if safeData.status == "REACHED"
                {
                    reachedRecord.append(safeData)

                }
                if safeData.status == "PENDING"
                {
                    let formatterForTime = DateFormatter()

                    let date = Date()
                    let currentTimeFormat2 = formatterForTime.string(from: date.addingTimeInterval(15 * 60) as Date )
                    _ = formatterForTime.date(from: currentTimeFormat2)!
                    let pass = "\(safeData.validityStart!) \(safeData.startTime!)"
                    formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
                    let passDate = formatterForTime.date(from: pass)!
                    let currentDateF = formatterForTime.string(from: date as Date)
                    let curr = formatterForTime.date(from: currentDateF) // curretn time
                    let passPlusGrace = passDate.addingTimeInterval(15*60) // pass plus 15 grace
                    
                    if curr! > passDate && curr! < passPlusGrace
                    {
                        activeRecord.append(safeData)
                    }
                    else
                    {
                        if curr! > passDate
                        {
                            expiredRecord.append(safeData)
                        }
                    }

                }
            }
        }
        }
        historyTabel.reloadData()
    }
    @objc func action() {
        expiryDatePicker.removeFromSuperview()
        toolBar.removeFromSuperview()
        view.endEditing(true)
        makePostCallVisitorRecord(addressID: addressIdSelected! , status: historyStatusSelected , date: historyDate!)
    }

    @objc func refresh(_ sender: AnyObject) {
        print("refresh")
        makePostCallVisitorRecord(addressID: addressIdSelected! , status: historyStatusSelected , date: historyDate!)
    }

    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }

    @IBAction func statusSegmentCHange(_ sender: Any) {
        let x = sender as! UISegmentedControl
        switch x.selectedSegmentIndex {
        case 0:
            print("ALL")
            historyStatusSelected = "ALL"
            historyTabel.reloadData()
        case 1:
            print("pending")
            historyStatusSelected = "ACTIVE"
            historyTabel.reloadData()

        case 2:
            print("approve")
            historyStatusSelected = "REACHED"
            historyTabel.reloadData()
        case 3:
            print("reject")
            historyStatusSelected = "EXPIRED"
            historyTabel.reloadData()
        default:
            print("errro")
        }
    }
    @objc func updateUser(notfication: NSNotification) {
    visitorRecordData = nil
        makePostCallVisitorRecord(addressID: addressIdSelected!  , status: historyStatusSelected , date: historyDate!)
}
    func makePostCallAddress() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_user_addresslist")!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(AddressJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.addressList = loginBaseResponse
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
  
    @objc func showAddress()
    {
        if let address = addressList
        {
            if let listOfAllAddress = address.addressList
            {
                let alert = UIAlertController(title: "Choose Address", message: nil, preferredStyle: .actionSheet)
                alert.view.tintColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
                for n in 0...listOfAllAddress.count - 1 {
                    alert.addAction(UIAlertAction(title: "\(listOfAllAddress[n].houseNo ?? ""), \(listOfAllAddress[n].address ?? "")" , style: .default , handler:{ [self] (UIAlertAction)in
                        self.addressLabel.text = UIAlertAction.title
                        print(n)

                        makePostCallVisitorRecord(addressID: listOfAllAddress[n].addressID!  , status: historyStatusSelected , date: historyDate!)
                        createSpinnerView()
                        addressIdSelected = listOfAllAddress[n].addressID!
                    }))
                }
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler:{ (UIAlertAction)in
                    print("User click Dismiss button")
                }))
                self.present(alert, animated: true, completion: {
                    print("completion block")
                })
            }
        }
    }
    
    func makePostCallVisitorRecord(addressID : String , status : String , date: String) {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)" , "address_id" : "\(addressID)"  , "status" : "\(status)" , "monthyear" : "\(date)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_visitorsbystatus")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(VisitorRecordUserJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                               print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        //print(loginBaseResponse as Any)
                        
                        self.visitorRecordData = loginBaseResponse
                        self.checkStatus()
                        self.historyTabel.reloadData()
                        self.removeSpinnerView()
                        self.refreshControl.endRefreshing()

                        if loginBaseResponse?.visitorList?.count == 0
                                  {
                                     let alert = UIAlertController(title: "History", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                                     alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                     //self.present(alert, animated: true, completion: nil)
                                    }
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        task.resume()
    }
    }
}

extension VisitorHistory: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = historyTabel.dequeueReusableCell(withIdentifier: reuseCellIdentifier.historyCellIdentifier) as! HistoryCell
        var s : VisitorList?
        switch historyStatusSelected {
        case "ALL":
            s = visitorRecordData?.visitorList?[indexPath.row]
        case "ACTIVE":
            s = activeRecord[indexPath.row]
        case "EXPIRED":
            s = expiredRecord[indexPath.row]
        case "REACHED" :
            s = reachedRecord[indexPath.row]
        default:
            print("err")
        }

        if let safeData = s        {
            cell.name.text = safeData.name
            cell.date.text = convertDateFormater(safeData.validityStart!)
            cell.status.text = safeData.status
            cell.type.text = safeData.purpose
            cell.vehicleNo.text = safeData.vehicleNo
            cell.status.textColor = .systemGreen
            if safeData.status == "PENDING" || safeData.status == "REJECTED"
            {
                cell.status.textColor = .red
            }
            if safeData.status == "REACHED" || safeData.status == "VISITED"
            {
                cell.status.textColor = .systemGreen

            }
            if safeData.status != "PENDING" && safeData.status != "REACHED"
            {
              //  cell.status.text = "EXPIRED"
               // cell.status.textColor = .red
            }
            if safeData.status == "PENDING"
            {
                let formatterForTime = DateFormatter()

                let date = Date()
                let currentTimeFormat2 = formatterForTime.string(from: date.addingTimeInterval(15 * 60) as Date )
                _ = formatterForTime.date(from: currentTimeFormat2)!
                let pass = "\(safeData.validityStart!) \(safeData.startTime!)"
                formatterForTime.dateFormat = "yyyy-MM-dd HH:mm:ss"
                let passDate = formatterForTime.date(from: pass)!
                let currentDateF = formatterForTime.string(from: date as Date)
                let curr = formatterForTime.date(from: currentDateF) // curretn time
                let passPlusGrace = passDate.addingTimeInterval(15*60) // pass plus 15 grace
                
                if curr! > passDate && curr! < passPlusGrace
                {
                    
                }
                else
                {
                    if curr! > passDate
                    {
                        cell.status.text = "EXPIRED"
                    }
                }

            }
            cell.favButton.isUserInteractionEnabled = true
         
            cell.heathImage.image = UIImage(named: "like")?.withTintColor(.systemRed)
             cell.favButton.addTarget(self, action: #selector(addFav), for: .touchUpInside)
              cell.favButton.tag = indexPath.row
            if safeData.favId != nil && safeData.favId != "0"
            {
                cell.heathImage.image = UIImage(named: "like")?.withTintColor(.systemRed)

            }
            else
            {
                cell.heathImage.image = UIImage(named: "LikeEmpty")?.withTintColor(.systemRed)

            }

        }
        return cell
    }
    @objc func addFav(sender : UIButton)
    {
        let x = sender.tag
        let i = IndexPath(row: x, section: 0)
        print(visitorRecordData?.visitorList?[i.row] as Any)
        if let safeData = visitorRecordData?.visitorList?[i.row]
        {
        let defaults = UserDefaults.standard
        let subID = defaults.string(forKey: "subID")
        let usrID = defaults.string(forKey: "userID")
            let usrmbrID = defaults.string(forKey: userDefaultsKey.userMemberID)

        let name = defaults.string(forKey: userDefaultsKey.name)
        let email = defaults.string(forKey: userDefaultsKey.email)
        let housing = defaults.string(forKey: userDefaultsKey.housingName)
            let json: [String: Any] = ["category" : "\(safeData.category!)", "visitor_id" : "0", "name" : "\(safeData.name!)", "housing_name" : "\(housing!)",
                                       "email" : "\(email!)", "contact_no" : "\(safeData.contactNo!)", "vehicle_type" : "0",
                                       "visitor_type" : "\(safeData.visitorType!)", "season_type" : "None", "identification_type" : "0",
                                       "identification_no" : "0", "purpose" : "\(safeData.purpose!)",
                                       "validity_start" : "\(safeData.validityStart!)   ", "validity_end" : "\(safeData.validityEnd!)",
                                       "start_time" : "\(safeData.startTime!)", "end_time" : "\(safeData.endTime!)" ,
                                       "vehicle_no" : "\(safeData.vehicleNo!)" ,
                                       "userid" : "\(usrID!)", "subscriber_id" : "\(subID!)","address" : "\(safeData.address!)" , "address_id" : "\(safeData.addressID!)",
                                   "status" : "PENDING", "upload_type" : "0" , "create_by" : "\(name!)",
                                   "modify_by" : "\(name!)" , "user_memberid" : "\(usrmbrID!)"]
            makePostCallAddFavourites(to: URL(string: "\(ConstantsUsedInProject.baseUrl)add_visitors_fav")!, params: json, row: i)
        }
        
    }
    func makePostCallAddFavourites(to url: URL, params: [String: Any] , row : IndexPath) {
        let block = { (multipart: MultipartFormData) in
            URLEncoding.default.queryParameters(params).forEach { (key, value) in
                if value.data(using: .utf8) != nil {
                    if let data = value.removingPercentEncoding?.data(using: .utf8) {
                        // do your stuff here
                        print(data)
                        multipart.append(data, withName: key)
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                    }
                }
            }
        }
        
        DispatchQueue.main.async
        {
            AF.upload(multipartFormData: block, to: url)
                .uploadProgress(queue: .main, closure: { progress in
                    //Current upload progress of file
                    print("Upload Progress: \(progress.fractionCompleted)")
                })
                .response{
                    response in
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        
                        let loginBaseResponse = try? decoder.decode(AddVisitorsJSON.self, from: data)
                        
                        
                        let code_str = loginBaseResponse?.code
                        print(String(data: data, encoding: String.Encoding.utf8) as Any)
                        
                        DispatchQueue.main.async { [self] in
                            
                            if code_str == 200 {
                                let cell = historyTabel.cellForRow(at: row) as! HistoryCell
                                cell.heathImage.image = UIImage(named: "like")?.withTintColor(.systemRed)

                                let alert = UIAlertController(title: "New Visitor", message: "Favoutire added ", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                              
                                NotificationCenter.default.post(name: Notification.Name("\(notificationNames.favouriteAddedOrUpdated)"), object: nil)
                            }
                            else
                            {
                                let alert = UIAlertController(title: "New Visitor", message: "\(loginBaseResponse?.response ?? "Cant add")", preferredStyle: UIAlertController.Style.alert)
                                
                                // add an action (button)
                                alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                
                                // show the alert
                                self.present(alert, animated: true, completion: nil)
                            }
                            
                        }
                        
                        
                    }
                }
        }
        
        
    }
    func convertDateFormater(_ date: String) -> String
        {
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            let date = dateFormatter.date(from: date)
            dateFormatter.dateFormat = "dd-MM-yyyy"
            return  dateFormatter.string(from: date!)

        }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch historyStatusSelected {
        case "ALL":
            return visitorRecordData?.visitorList?.count ?? 0
        case "ACTIVE":
           return activeRecord.count
        case "EXPIRED":
           return expiredRecord.count
        case "REACHED" :
            return reachedRecord.count
        default:
            return 0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        historyTabel.deselectRow(at: indexPath, animated: true)
        var s : VisitorList?
        switch historyStatusSelected {
        case "ALL":
            s = visitorRecordData?.visitorList?[indexPath.row]
        case "ACTIVE":
            s = activeRecord[indexPath.row]
        case "EXPIRED":
            s = expiredRecord[indexPath.row]
        case "REACHED" :
            s = reachedRecord[indexPath.row]
        default:
            print("err")
        }
        if let safeData = s
        {
            let cell = historyTabel.cellForRow(at: indexPath) as! HistoryCell
            let status = cell.status.text
            if safeData.status == "REACHED" || safeData.status == "VISITED" || safeData.status == "EXPIRED" || status == "EXPIRED" || safeData.status == "REJECTED"
            {
                
            }
            else
            {
                //print(safeData.status)
                //print(visitorRecordData?.visitorList?[indexPath.row])
                openPass(index: indexPath)
            }

        }
    }
    @objc func openPass(index : IndexPath)
    {
        let storyboard = UIStoryboard(name: "VisitorPass", bundle: nil)
        let myAlert = storyboard.instantiateViewController(withIdentifier: "visitorPass") as! VisitorPass
        myAlert.visitorHistoryData = visitorRecordData?.visitorList?[index.row]
        myAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        myAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        self.present(myAlert, animated: true, completion: nil)
    }
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    func tableView(_ tableView: UITableView,trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration?
        {
        
        let titleForSwipe = "REMOVE"
        let colorForTitle : UIColor = .red
   
        let action = UIContextualAction(style: .normal, title:  titleForSwipe, handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
                print("Update action ...")
                success(true)
            self.makePostCallRemoveVisitor(visitorID: (self.visitorRecordData?.visitorList?[indexPath.row].visitorID)!)
            
            })
            action.backgroundColor = colorForTitle
        action.image = UIImage(systemName: "trash")

        
            return UISwipeActionsConfiguration(actions: [action])
        }
    func makePostCallRemoveVisitor(visitorID : String) {
     
        let decoder = JSONDecoder()
        let json: [String: Any] = ["visitor_id":"\(visitorID)"]
        print(json)
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)remove_visitor")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(RemoveVisitorAndFavouritesJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                     //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                            makePostCallVisitorRecord(addressID: addressIdSelected! , status: historyStatusSelected , date: historyDate!)
                            let alert = UIAlertController(title: "Visitor", message: "Successfully Deleted", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Visitor", message: "Error", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

}
