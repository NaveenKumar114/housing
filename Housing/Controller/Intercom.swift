//
//  Intercom.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-14.
//

import UIKit

class Intercom: UIViewController {
    let child = SpinnerViewController()

    @IBOutlet var viewForBlur: UIView!
    var intercomData : IntercomJSON?
    @IBOutlet weak var numbersTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        numbersTable.delegate = self
        numbersTable.dataSource = self
        numbersTable.register(UINib(nibName: reuseNibIdentifier.intercomNibIdentifier, bundle: nil) , forCellReuseIdentifier: reuseCellIdentifier.intercomCellIdentifier)
        numbersTable.separatorColor = UIColor(named: ConstantsUsedInProject.appThemeColorName)
        makePostCall()
        setupBlur()
        createSpinnerView()
        self.numbersTable.tableFooterView = UIView()


    }
    func createSpinnerView() {
        addChild(child)
        child.view.frame = view.frame
        view.addSubview(child.view)
        child.didMove(toParent: self)
    }
    func removeSpinnerView()
    {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    func setupBlur()
    {
        let blurEffect = UIBlurEffect(style: .light) // .extraLight or .dark
        let blurEffectView = UIVisualEffectView(effect: blurEffect)
        blurEffectView.frame = viewForBlur.frame
        viewForBlur.addSubview(blurEffectView)
    }
    @IBAction func closeButtonPressed(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
        
    }
    func makePostCall() {
        let defaults = UserDefaults.standard
        let sub = defaults.string(forKey: "subID")
        let usr = defaults.string(forKey: "userID")
        let decoder = JSONDecoder()
        let json: [String: Any] = ["subscriber_id" : "\(sub!)" , "userid" : "\(usr!)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
      if  let url = URL(string: "\(ConstantsUsedInProject.baseUrl)get_intercomlist")
      {
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        
        // insert json data to the request
        request.httpBody = jsonData
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(IntercomJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async { [self] in
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        intercomData = loginBaseResponse
                        numbersTable.reloadData()
                        removeSpinnerView()
                         if loginBaseResponse?.intercomList?.count == 0
                          {
                             let alert = UIAlertController(title: "Intercom", message: "No Result Found", preferredStyle: UIAlertController.Style.alert)
                             alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                             self.present(alert, animated: true, completion: nil)
                            }
                        if loginBaseResponse?.intercomList?.count == 1
                         {
                            
                            print("1")
                            if let number = intercomData?.intercomList?[0].phoneNumber
                            {
                            // dialNumber(number: number)
                            }
                           }
                     
                        
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Login", message: "Invalid Email & Password", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
            }
        }
        
        task.resume()
    }
    }
    func dialNumber(number : String) {

     if let url = URL(string: "tel://\(number)"),
       UIApplication.shared.canOpenURL(url) {
          if #available(iOS 10, *) {
            UIApplication.shared.open(url, options: [:], completionHandler:nil)
           } else {
               UIApplication.shared.openURL(url)
           }
       } else {
                // add error message here
       }
    }
}

extension Intercom: UITableViewDelegate , UITableViewDataSource
{
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = numbersTable.dequeueReusableCell(withIdentifier: reuseCellIdentifier.intercomCellIdentifier) as! IntercomCell
        cell.numberLabel.text = intercomData?.intercomList?[indexPath.row].phoneNumber
      //  cell.timingLabel.text = "\(intercomData?.intercomList?[indexPath.row].startTime ?? "") - \(intercomData?.intercomList?[indexPath.row].endTime ?? "")"
        cell.timingLabel.text = intercomData?.intercomList?[indexPath.row].name
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 75
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return intercomData?.intercomList?.count ?? 0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       if let number = intercomData?.intercomList?[indexPath.row].phoneNumber
       {
        dialNumber(number: number)
       }
        
    }
    
    
}
