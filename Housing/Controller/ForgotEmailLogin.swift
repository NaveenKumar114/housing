//
//  ForgotEmailLogin.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-11.
//

import UIKit

class ForgotEmailLogin: UIViewController, UITextFieldDelegate {

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
    @IBOutlet weak var viewForShadow: UIView!
    @IBOutlet weak var emailLabel: UITextField!
    @IBOutlet weak var profileImage: UIImageView!
    var hcode : String?
    override func viewDidLoad() {
        super.viewDidLoad()
        let defaults = UserDefaults.standard

        emailLabel.delegate = self
        emailLabel.font = UIFont(name: "Arial-Rounded", size: 15.0)
    }


    @IBAction func endEmailPressed(_ sender: Any) {
        if emailLabel.text != ""
        {
            let number = arc4random_uniform(900000) + 100000;
            print(number)
            hcode = String(number)
            makePostCall()
        }
        else
        {
            let alert = UIAlertController(title: "Reset Password", message: "Please enter a email", preferredStyle: UIAlertController.Style.alert)
            
            // add an action (button)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
            
            // show the alert
            self.present(alert, animated: true, completion: nil)
        }
    }
    @IBAction func cancelPressed(_ sender: Any) {
        navigationController?.popToRootViewController(animated: true)
    }
    
    func makePostCall() {
        let decoder = JSONDecoder()
        let request = NSMutableURLRequest(url: NSURL(string: "\(ConstantsUsedInProject.baseUrl)changePassswordRequest")! as URL)
        request.httpMethod = "POST"
        let postString = "email=\(emailLabel.text!)&hcode=\(hcode!)"
        print(postString)
        request.setValue("application/x-www-form-urlencoded; charset=utf-8", forHTTPHeaderField: "Content-Type")
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
            guard error == nil && data != nil else {                                                          // check for fundamental networking error
                print("error=\(String(describing: error))")
                
                return
            }
            do {
                
                let loginBaseResponse = try? decoder.decode(ChangePasswordKcodeJSON.self, from: data!)
                let code_str = loginBaseResponse!.code
                 print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                
                DispatchQueue.main.async {
                    
                    if code_str == 200 {
                        
                        //       print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        // print("success")
                        print(loginBaseResponse as Any)
                        self.performSegue(withIdentifier: segueIdentifiers.confirmPasswordEmailToPassword, sender: loginBaseResponse)
                       // NotificationCenter.default.post(name: Notification.Name("confirmEmail"), object: nil)
                    }else if code_str == 201  {
                        //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                        
                        
                        let alert = UIAlertController(title: "Reset Password", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }
                    
                }
                
                
                
            }
        }
        task.resume()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.confirmPasswordEmailToPassword
        {
            let a = sender as! ChangePasswordKcodeJSON
            let destVC = segue.destination as! ForgotLoginPassword
            destVC.hcode = self.hcode
            destVC.userDetails = a
        }
     
    }

}
