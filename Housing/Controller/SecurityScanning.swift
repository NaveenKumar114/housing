//
//  SecurityScanning.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-08.
//
import AVFoundation

import UIKit

class SecurityScanning: UIViewController , AVCaptureMetadataOutputObjectsDelegate {
    @IBOutlet weak var viewForQrScanner: UIView!
    var scanType : securityScanType?
    var captureSession: AVCaptureSession!
        var previewLayer: AVCaptureVideoPreviewLayer!

        override func viewDidLoad() {
            super.viewDidLoad()
            print(scanType ?? "error")
            
           // view.backgroundColor = UIColor.black
            captureSession = AVCaptureSession()

            guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
            let videoInput: AVCaptureDeviceInput

            do {
                videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
            } catch {
                return
            }

            if (captureSession.canAddInput(videoInput)) {
                captureSession.addInput(videoInput)
            } else {
                failed()
                return
            }

            let metadataOutput = AVCaptureMetadataOutput()

            if (captureSession.canAddOutput(metadataOutput)) {
                captureSession.addOutput(metadataOutput)

                metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
                metadataOutput.metadataObjectTypes = [.qr]
            } else {
                failed()
                return
            }
            previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
            previewLayer.frame = viewForQrScanner.layer.bounds
            previewLayer.videoGravity = .resizeAspectFill
            viewForQrScanner.layer.addSublayer(previewLayer)
            captureSession.startRunning()
        }

        func failed() {
            let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
            captureSession = nil
        }

        override func viewWillAppear(_ animated: Bool) {
            super.viewWillAppear(animated)

            if (captureSession?.isRunning == false) {
                captureSession.startRunning()
            }
        }

        override func viewWillDisappear(_ animated: Bool) {
            super.viewWillDisappear(animated)

            if (captureSession?.isRunning == true) {
                captureSession.stopRunning()
            }
        }

        func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
            captureSession.stopRunning()

            if let metadataObject = metadataObjects.first {
                guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
                guard let stringValue = readableObject.stringValue else { return }
                AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
                found(code: stringValue)
            }

            dismiss(animated: true)
        }

        func found(code: String) {
            print(code)
            captureSession.startRunning()
            switch scanType {
            case .access:
               makePostCallAccess(scanData: code)
            case .facility:
                makePostCallFacility(scanData: code)
            case .visitor:
                makePostCallVisitor(scanData: code)
            default:
                print("error")
            }        }

        override var prefersStatusBarHidden: Bool {
            return false
        }

        override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
            return .portrait
        }
    func makePostCallAccess(scanData : String  ) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["scan_data" : "\(scanData)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)decryptaccess")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(SecurityAccessJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                         
                            self.performSegue(withIdentifier: segueIdentifiers.qrScanToAccess, sender: loginBaseResponse)
                            print(loginBaseResponse as Any)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Security", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }
    
    func makePostCallFacility(scanData : String  ) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["scan_data" : "\(scanData)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)decryptfacilities")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    let loginBaseResponse = try? decoder.decode(SecurityFacilityJSON.self, from: data!)
                    let code_str = loginBaseResponse!.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                         
                            self.performSegue(withIdentifier: segueIdentifiers.qrScanToFacility, sender: loginBaseResponse)
                            print(loginBaseResponse as Any)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Security", message: "\(loginBaseResponse?.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                    
                }
            }
            task.resume()
        }
    }

    func makePostCallVisitor(scanData : String  ) {
        let decoder = JSONDecoder()
        let json: [String: Any] = ["scan_data" : "\(scanData)"]
        let jsonData = try? JSONSerialization.data(withJSONObject: json)
        // create post request
        if let url = URL(string:"\(ConstantsUsedInProject.baseUrl)decrypt")
        {
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            
            // insert json data to the request
            request.httpBody = jsonData
            let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
                guard error == nil && data != nil else {                                                          // check for fundamental networking error
                    print("error=\(String(describing: error))")
                    
                    return
                }
                do {
                    
                    if let loginBaseResponse = try? decoder.decode(SecurityVisitorPassJSON.self, from: data!)
                 {
                        let code_str = loginBaseResponse.code
                    // print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                    
                    DispatchQueue.main.async { [self] in
                        
                        if code_str == 200 {
                         
                            self.performSegue(withIdentifier: segueIdentifiers.qrScanToVisitorPass, sender: loginBaseResponse)
                            print(loginBaseResponse as Any)
                            
                        }else if code_str == 201  {
                            //print(String(data: data!, encoding: String.Encoding.utf8) as Any)
                            
                            
                            let alert = UIAlertController(title: "Security", message: "\(loginBaseResponse.response ?? "Error")", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        }
                        
                    }
                    
                }
                    else
                    {
                        let alert = UIAlertController(title: "Security", message: "This is not a visitor pass", preferredStyle: UIAlertController.Style.alert)
                        
                        // add an action (button)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (_) in
                            DispatchQueue.main.async {
                                self.captureSession.startRunning()

                              }

                        }))
                        
                        // show the alert
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
            task.resume()
        }
    }

    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueIdentifiers.qrScanToAccess
        {
            let data = sender as! SecurityAccessJSON
            let destVC = segue.destination as! AccessSecurity
            destVC.passData = data
            
        }
        else
        if segue.identifier == segueIdentifiers.qrScanToFacility
        {
            let data = sender as! SecurityFacilityJSON
            let destVC = segue.destination as! FacilitySecurity
            destVC.passData = data
            
        }
        else
        if segue.identifier == segueIdentifiers.qrScanToVisitorPass
        {
            let data = sender as! SecurityVisitorPassJSON
            let destVC = segue.destination as! VisitorPassSecurity
            destVC.passData = data
            
        }
    }
}


