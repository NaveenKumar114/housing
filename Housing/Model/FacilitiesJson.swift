//
//  FacilitiesJson.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-04.
//

import Foundation
struct FacilitiesJSON: Codable {
    let code: Int?
    let response: String?
    let facilitiesCategoryList: [FacilitiesCategoryList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case facilitiesCategoryList = "facilities_category_list"
    }
}

// MARK: - FacilitiesCategoryList
struct FacilitiesCategoryList: Codable {
    let id, facilityName, advanceAmount, bookingAmount: String?
    let icon, subscriberID, createBy, createDate: String?
    let modifyBy, modifyDate: String?
    let capacity : String?

    enum CodingKeys: String, CodingKey {
        case id
        case capacity = "capacity"
        case facilityName = "facility_name"
        case advanceAmount = "advance_amount"
        case bookingAmount = "booking_amount"
        case icon
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
