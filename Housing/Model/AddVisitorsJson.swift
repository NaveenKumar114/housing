//
//  AddVisitorsJson.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-28.
//

import Foundation
// MARK: - AddVisitorsJSON
struct AddVisitorsJSON: Codable {
    let code: Int?
    let response: String?
    let visitor: Visitor?
}

// MARK: - Visitor
struct Visitor: Codable {
    let visitorID: Int?
    let category, name, housingName, email: String?
    let contactNo, vehicleType, seasonType, vehicleNo: String?
    let identificationType, identificationNo, purpose, validityStart: String?
    let validityEnd, startTime, endTime, visitorType: String?
    let status, qrImage, address, addressID: String?
    let userid, subscriberID, modifyBy, createBy: String?
    let modifyDate, createDate: String?

    enum CodingKeys: String, CodingKey {
        case visitorID = "visitor_id"
        case category, name
        case housingName = "housing_name"
        case email
        case contactNo = "contact_no"
        case vehicleType = "vehicle_type"
        case seasonType = "season_type"
        case vehicleNo = "vehicle_no"
        case identificationType = "identification_type"
        case identificationNo = "identification_no"
        case purpose
        case validityStart = "validity_start"
        case validityEnd = "validity_end"
        case startTime = "start_time"
        case endTime = "end_time"
        case visitorType = "visitor_type"
        case status
        case qrImage = "qr_image"
        case address
        case addressID = "address_id"
        case userid
        case subscriberID = "subscriber_id"
        case modifyBy = "modify_by"
        case createBy = "create_by"
        case modifyDate = "modify_date"
        case createDate = "create_date"
    }
}
