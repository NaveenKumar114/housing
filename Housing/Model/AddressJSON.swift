//
//  AddressJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-19.
//

import Foundation

// MARK: - AddressJSON
struct AddressJSON: Codable {
    let code: Int?
    let response: String?
    let addressList: [AddressList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case addressList = "address_list"
    }
}

// MARK: - AddressList
struct AddressList: Codable {
    let addressID, userid, subscriberID, houseNo: String?
    let address, createBy, createDate, modifyBy: String?
    let modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case addressID = "address_id"
        case userid
        case subscriberID = "subscriber_id"
        case houseNo = "house_no"
        case address
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
