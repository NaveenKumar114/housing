//
//  AccessJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-22.
//

import Foundation

// MARK: - AccessJSON
struct AccessJSON: Codable {
    let code: Int?
    let response: String?
    let accessList: [AccessLists]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case accessList = "access_list"
    }
}

// MARK: - AccessList
struct AccessLists: Codable {
    let id, accessName, userName, userID: String?
    let qrImage, subscriberID, createBy, createDate: String?
    let startDate , startTime , endDate , endTime : String?
    let modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case startDate = "start_date"
        case startTime = "start_time"
        case endDate = "end_date"
        case endTime = "end_time"
        case accessName = "access_name"
        case userName = "user_name"
        case userID = "user_id"
        case qrImage = "qr_image"
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
