//
//  LoginJson.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-17.
//

import Foundation

// MARK: - LoginJSON
struct LoginJson: Codable {
    let code: Int?
    let response: String?
    let userLogindetails: UserLogindetails?
    let subscriberDetails: SubscriberDetails?

    enum CodingKeys: String, CodingKey {
        case code, response
        case userLogindetails = "user_logindetails"
        case subscriberDetails = "subscriber_details"
    }
}

// MARK: - SubscriberDetails
struct SubscriberDetails: Codable {
    let userid, housingName: String?
    let houseNo: String?
    let name: String?
    let profileImage: String?
    let email, password, mobile, city: String?
    let address, identificationNo, identificationType, identificationTypeID: String?
    let fax, hotlineNumber, role, roleName: String?
    let images, status, planExpire, planID: String?
    let terms, privacy, forgottenCode, freeTrialStatus: String?
    let subscriberID: String?
    let createdBy, createdDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case userid
        case housingName = "housing_name"
        case houseNo = "house_no"
        case name
        case profileImage = "profile_image"
        case email, password, mobile, city, address
        case identificationNo = "identification_no"
        case identificationType = "identification_type"
        case identificationTypeID = "identification_type_id"
        case fax
        case hotlineNumber = "hotline_number"
        case role
        case roleName = "role_name"
        case images, status
        case planExpire = "plan_expire"
        case planID = "plan_id"
        case terms, privacy
        case forgottenCode = "forgotten_code"
        case freeTrialStatus = "free_trial_status"
        case subscriberID = "subscriber_id"
        case createdBy = "created_by"
        case createdDate = "created_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

// MARK: - UserLogindetails
struct UserLogindetails: Codable {
    let userMemberid, memberName, memberHousingname, memberRole: String?
    let memberType, memberAddress, memberAddressID, memberAlladdressID: String?
    let memberEmail, memberContact, memberPassword, memberIndentificationNo: String?
    let memberIdentificationType, memberIdentificationTypeID, userRole, memberStatus: String?
    let verificationCode, userid, subscriberID, tokenid: String?
    let profileImg, createBy, createDate, modifyBy: String?
    let modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case userMemberid = "user_memberid"
        case memberName = "member_name"
        case memberHousingname = "member_housingname"
        case memberRole = "member_role"
        case memberType = "member_type"
        case memberAddress = "member_address"
        case memberAddressID = "member_address_id"
        case memberAlladdressID = "member_alladdress_id"
        case memberEmail = "member_email"
        case memberContact = "member_contact"
        case memberPassword = "member_password"
        case memberIndentificationNo = "member_indentification_no"
        case memberIdentificationType = "member_identification_type"
        case memberIdentificationTypeID = "member_identification_type_id"
        case userRole = "user_role"
        case memberStatus = "member_status"
        case verificationCode = "verification_code"
        case userid
        case subscriberID = "subscriber_id"
        case tokenid
        case profileImg = "profile_img"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
