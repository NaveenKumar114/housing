//
//  RemoveVisitorAndFavouritesJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-01.
//

import Foundation

// MARK: - RemoveVisitorAndFavouritesJSON
struct RemoveVisitorAndFavouritesJSON: Codable {
    let code: Int?
    let response: String?
}
