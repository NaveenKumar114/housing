//
//  ConstantsUsedInProject.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-19.
//

import Foundation
import UIKit

struct ConstantsUsedInProject {
    static let appThemeColor : UIColor = #colorLiteral(red: 0.1033141688, green: 0.326824069, blue: 0.4980535507, alpha: 1)
    static let appThemeColorName = "AccentColor"
    static let baseUrl = "http://e-visitor.my/housing_android_api/userapi/"
    static let baseImgUrl = "http://e-visitor.my/uploads/"
}
