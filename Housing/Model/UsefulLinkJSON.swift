//
//  UsefulLinkJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-21.
//

import Foundation

struct UsefulLinkJSON: Codable {
    let code: Int?
    let response: String?
    let usefullinkList: [UsefullinkList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case usefullinkList = "usefullink_list"
    }
}

// MARK: - UsefullinkList
struct UsefullinkList: Codable {
    let id, subscriberID, name, image: String?
    let createBy, createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case subscriberID = "subscriber_id"
        case name, image
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
