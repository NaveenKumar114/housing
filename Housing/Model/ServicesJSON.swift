//
//  ServicesJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-19.
//

import Foundation

// MARK: - ServicesJSON
struct ServicesJSON: Codable {
    let code: Int?
    let response: String?
    let servicesList: [ServicesList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case servicesList = "services_list"
    }
}

// MARK: - ServicesList
struct ServicesList: Codable {
    let serviceID, serviceName, subscriberID, createBy: String?
    let createDate, modifyBy, modifyDate: String?
    let subService: [SubService]?

    enum CodingKeys: String, CodingKey {
        case serviceID = "service_id"
        case serviceName = "service_name"
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
        case subService = "sub_service"
    }
}

// MARK: - SubService
struct SubService: Codable {
    let id, serviceID, subscriberID, name: String?
    let contact, address, createBy, createDate: String?
    let modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case serviceID = "service_id"
        case subscriberID = "subscriber_id"
        case name, contact, address
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
