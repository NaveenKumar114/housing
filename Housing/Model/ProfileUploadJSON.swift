//
//  ProfileUploadJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-23.
//

import Foundation

// MARK: - ProfileUploadJSON
struct ProfileUploadJSON: Codable {
    let code: Int?
    let response: String?
    let userLogindetails: ProfileUploadData?

    enum CodingKeys: String, CodingKey {
        case code, response
        case userLogindetails = "user_logindetails"
    }
}

// MARK: - UserLogindetails
struct ProfileUploadData: Codable {
    let userMemberid, memberName, memberHousingname, memberRole: String?
    let memberType, memberAddress, memberAddressID, memberEmail: String?
    let memberContact, memberPassword, memberIndentificationNo, memberIdentificationType: String?
    let userRole, memberStatus, verificationCode, userid: String?
    let subscriberID, profileImg, createBy, createDate: String?
    let modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case userMemberid = "user_memberid"
        case memberName = "member_name"
        case memberHousingname = "member_housingname"
        case memberRole = "member_role"
        case memberType = "member_type"
        case memberAddress = "member_address"
        case memberAddressID = "member_address_id"
        case memberEmail = "member_email"
        case memberContact = "member_contact"
        case memberPassword = "member_password"
        case memberIndentificationNo = "member_indentification_no"
        case memberIdentificationType = "member_identification_type"
        case userRole = "user_role"
        case memberStatus = "member_status"
        case verificationCode = "verification_code"
        case userid
        case subscriberID = "subscriber_id"
        case profileImg = "profile_img"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}

