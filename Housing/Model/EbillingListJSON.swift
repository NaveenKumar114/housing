//
//  EbillingListJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-06.
//

import Foundation


// MARK: - EbillingListJSON
struct EbillingListJSON: Codable {
    let code: Int?
    let response: String?
    let billingDetailsList: [BillingDetailsList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case billingDetailsList = "billing_details_list"
    }
}

// MARK: - BillingDetailsList
struct BillingDetailsList: Codable {
    let billingID, userid, subscriberID, addressID: String?
    let houseNo, address, billingName, billingFrom: String?
    let billingTo, pendingAmount, dueDate, penaltyAmount: String?
    let attachmentImage, billingType: String?
    let csvFileName, paidDate: String?
    let bulkID, status, createBy, createDate: String?
    let modifyDate, modifyBy: String?
    let totalAmount : String?
    let facilityID , advanceAmount , bookingAmount : String?

    enum CodingKeys: String, CodingKey {
        case billingID = "billing_id"
        case userid
        case facilityID = "facility_id"
        case totalAmount = "total_amount"
        case subscriberID = "subscriber_id"
        case addressID = "address_id"
        case houseNo = "house_no"
        case address
        case bookingAmount = "booking_amount"
        case advanceAmount = "advance_amount"
        case billingName = "billing_name"
        case billingFrom = "billing_from"
        case billingTo = "billing_to"
        case pendingAmount = "pending_amount"
        case dueDate = "due_date"
        case penaltyAmount = "penalty_amount"
        case attachmentImage = "attachment_image"
        case billingType = "billing_type"
        case csvFileName = "csv_file_name"
        case paidDate = "paid_date"
        case bulkID = "bulk_id"
        case status
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyDate = "modify_date"
        case modifyBy = "modify_by"
    }
}

