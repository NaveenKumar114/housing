//
//  FaqJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-19.
//

import Foundation

// MARK: - FAQJSON
struct FAQJSON: Codable {
    let code: Int?
    let response: String?
    let faqList: [FAQList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case faqList = "faq_list"
    }
}

// MARK: - FAQList
struct FAQList: Codable {
    let id, question, answer, subscriberID: String?
    let createBy, createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id, question, answer
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
