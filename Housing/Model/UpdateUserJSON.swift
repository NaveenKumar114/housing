//
//  UpdateUserJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-16.
//

import Foundation
// MARK: - UpdateUserJSON
struct UpdateUserJSON: Codable {
    let code: Int?
    let response: String?
    let usersMembers: UsersMembersUpdate?

    enum CodingKeys: String, CodingKey {
        case code, response
        case usersMembers = "users_members"
    }
}

// MARK: - UsersMembers
struct UsersMembersUpdate: Codable {
    let memberName, memberHousingname, memberRole, memberType: String?
    let memberEmail, memberContact, memberIndentificationNo, memberIdentificationType: String?
    let userRole, memberStatus, memberAddress, memberAddressID: String?
    let userid, subscriberID, modifyBy, createBy: String?
    let modifyDate, createDate: String?

    enum CodingKeys: String, CodingKey {
        case memberName = "member_name"
        case memberHousingname = "member_housingname"
        case memberRole = "member_role"
        case memberType = "member_type"
        case memberEmail = "member_email"
        case memberContact = "member_contact"
        case memberIndentificationNo = "member_indentification_no"
        case memberIdentificationType = "member_identification_type"
        case userRole = "user_role"
        case memberStatus = "member_status"
        case memberAddress = "member_address"
        case memberAddressID = "member_address_id"
        case userid
        case subscriberID = "subscriber_id"
        case modifyBy = "modify_by"
        case createBy = "create_by"
        case modifyDate = "modify_date"
        case createDate = "create_date"
    }
}
