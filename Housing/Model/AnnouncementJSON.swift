//
//  AnnouncementJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-17.
//

import Foundation

// MARK: - AnnouncementJSON
struct AnnouncementJSON: Codable {
    let code: Int?
    let response: String?
    let announcementList: [AnnouncementList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case announcementList = "announcement_list"
    }
}

// MARK: - AnnouncementList
struct AnnouncementList: Codable {
    let id, subscriberID, heading, announcementListDescription: String?
    let startDate, endDate, image, createBy: String?
    let createDate, modifyBy, modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case subscriberID = "subscriber_id"
        case heading
        case announcementListDescription = "description"
        case startDate = "start_date"
        case endDate = "end_date"
        case image
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
