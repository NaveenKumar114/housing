//
//  EbillingAttachmentJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-07.
//

import Foundation
// MARK: - EbillingAttachmentJSON
struct EbillingAttachmentJSON: Codable {
    let code: Int?
    let response: String?
    let billingDetail: BillingDetail?

    enum CodingKeys: String, CodingKey {
        case code, response
        case billingDetail = "billing_detail"
    }
}

// MARK: - BillingDetail
struct BillingDetail: Codable {
    let billingID, userid, subscriberID, addressID: String?
    let houseNo, address, billingName, billingFrom: String?
    let billingTo, pendingAmount, dueDate, penaltyAmount: String?
    let attachmentImage, billingType: String?
    let csvFileName, paidDate: String?
    let bulkID, status, createBy, createDate: String?
    let modifyDate, modifyBy: String?

    enum CodingKeys: String, CodingKey {
        case billingID = "billing_id"
        case userid
        case subscriberID = "subscriber_id"
        case addressID = "address_id"
        case houseNo = "house_no"
        case address
        case billingName = "billing_name"
        case billingFrom = "billing_from"
        case billingTo = "billing_to"
        case pendingAmount = "pending_amount"
        case dueDate = "due_date"
        case penaltyAmount = "penalty_amount"
        case attachmentImage = "attachment_image"
        case billingType = "billing_type"
        case csvFileName = "csv_file_name"
        case paidDate = "paid_date"
        case bulkID = "bulk_id"
        case status
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyDate = "modify_date"
        case modifyBy = "modify_by"
    }
}
