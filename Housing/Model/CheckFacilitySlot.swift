//
//  CheckFacilitySlot.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-03-02.
//

import Foundation

// MARK: - CheckFacilitySlot
struct CheckFacilitySlot: Codable {
    let code: Int?
    let response: String?
    let facilities: FacilitySlotData?
}

// MARK: - Facilities
struct FacilitySlotData: Codable {
    let slot1, slot2, slot3, slot4: String?
    let uslot1, uslot2, uslot3, uslot4: String?
    let aslot1 , aslot2 , aslot3 , aslot4 : String?
}
