//
//  CustomDataTypes.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-31.
//

import Foundation
import UIKit
struct seaseonBulkDataType
{
    var name : String
    var email : String
    var phoneNumber : String
    var icOrPassport : String
    var vehicleModel : String
    var vehicleNumber : String
    var proofPhoto : UIImage
    var facePhoto : UIImage
    var identifiationType : String
}

enum securityScanType {
    case access
    case facility
    case visitor
}
