//
//  SecurityAccessJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-09.
//

import Foundation
// MARK: - SecurityAccessJSON
struct SecurityAccessJSON: Codable {
    let code: Int?
    let response: String?
    let access: Access?
}

// MARK: - Access
struct Access: Codable {
    let id, accessName, userName, userID: String?
    let addressID, address, houseNo, startDate: String?
    let startTime, endDate, endTime, qrImage: String?
    let subscriberID, createBy, createDate, modifyBy: String?
    let modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case accessName = "access_name"
        case userName = "user_name"
        case userID = "user_id"
        case addressID = "address_id"
        case address
        case houseNo = "house_no"
        case startDate = "start_date"
        case startTime = "start_time"
        case endDate = "end_date"
        case endTime = "end_time"
        case qrImage = "qr_image"
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
