//
//  CheckUserJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-20.
//

import Foundation
// MARK: - CheckUserJSON
struct SignUPJson: Codable {
    let code: Int?
    let response: String?
    let userLogindetails: UserLog?

    enum CodingKeys: String, CodingKey {
        case code, response
        case userLogindetails = "user_logindetails"
    }
}

// MARK: - UserLogindetails
struct UserLog: Codable {
    let memberPassword: String?
    let verificationCode: Int?

    enum CodingKeys: String, CodingKey {
        case memberPassword = "member_password"
        case verificationCode = "verification_code"
    }
}
