//
//  IntercomJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-22.
//

import Foundation

// MARK: - IntercomJSON
struct IntercomJSON: Codable {
    let code: Int?
    let response: String?
    let intercomList: [IntercomList]?

    enum CodingKeys: String, CodingKey {
        case code, response
        case intercomList = "intercom_list"
    }
}

// MARK: - IntercomList
struct IntercomList: Codable {
    let id, phoneNumber, startTime, endTime: String?
    let subscriberID, createBy, createDate, modifyBy: String?
    let name : String?
    let modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case name = "intercom_name"
        case phoneNumber = "phone_number"
        case startTime = "start_time"
        case endTime = "end_time"
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
