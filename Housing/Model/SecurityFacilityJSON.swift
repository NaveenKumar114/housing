//
//  SecurityFacilityJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-10.
//

import Foundation

// MARK: - SecurityFacilityJSON
struct SecurityFacilityJSON: Codable {
    let code: Int?
    let response: String?
    let facilities: Facility?
}

// MARK: - Facilities
struct Facility: Codable {
    let id, facilitiesName, facilitiesType, facilitiesStartdate: String?
    let facilitiesStarttime, facilitiesEnddate, facilitiesEndtime, address: String?
    let addressID, status, qrImage, userid: String?
    let subscriberID, createBy, createDate, modifyBy: String?
    let modifyDate: String?

    enum CodingKeys: String, CodingKey {
        case id
        case facilitiesName = "facilities_name"
        case facilitiesType = "facilities_type"
        case facilitiesStartdate = "facilities_startdate"
        case facilitiesStarttime = "facilities_starttime"
        case facilitiesEnddate = "facilities_enddate"
        case facilitiesEndtime = "facilities_endtime"
        case address
        case addressID = "address_id"
        case status
        case qrImage = "qr_image"
        case userid
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
