//
//  AddFacilities.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-01-05.
//

import Foundation
struct AddFacilities: Codable {
    let code: Int?
    let response: String?
    let facilities: FacilitiesAdded?
}

// MARK: - Facilities
struct FacilitiesAdded: Codable {
    let facilitiesName, facilitiesType, facilityCategoryID, facilitiesStartdate: String?
    let facilitiesEnddate, facilitiesStarttime, facilitiesEndtime, userMemberid: String?
    let status, address, addressID, qrImage: String?
    let userid, subscriberID, modifyBy, createBy: String?
    let modifyDate, createDate: String?

    enum CodingKeys: String, CodingKey {
        case facilitiesName = "facilities_name"
        case facilitiesType = "facilities_type"
        case facilityCategoryID = "facility_category_id"
        case facilitiesStartdate = "facilities_startdate"
        case facilitiesEnddate = "facilities_enddate"
        case facilitiesStarttime = "facilities_starttime"
        case facilitiesEndtime = "facilities_endtime"
        case userMemberid = "user_memberid"
        case status, address
        case addressID = "address_id"
        case qrImage = "qr_image"
        case userid
        case subscriberID = "subscriber_id"
        case modifyBy = "modify_by"
        case createBy = "create_by"
        case modifyDate = "modify_date"
        case createDate = "create_date"
    }
}
