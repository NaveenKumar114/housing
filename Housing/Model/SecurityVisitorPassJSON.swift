//
//  SecurityVisitorPassJSON.swift
//  Housing
//
//  Created by Naveen Natrajan on 2021-02-10.
//

import Foundation

// MARK: - SecurityVisitorPassJSON
struct SecurityVisitorPassJSON: Codable {
    let code: Int?
    let response: String?
    let visitor: VisitorData?
}

// MARK: - Visitor
struct VisitorData: Codable {
    let visitorID, category, name, housingName: String?
    let email, contactNo, vehicleType, vehicleNo: String?
    let identificationType, identificationNo, purpose, visitorType: String?
    let seasonType, validityStart, validityEnd, startTime: String?
    let endTime, inTime, outTime, address: String?
    let addressID, status, remarks, facePhoto: String?
    let identificationPhoto, qrImage, userid, subscriberID: String?
    let createBy, createDate, modifyBy, modifyDate: String?
    let userMemberId : String?

    enum CodingKeys: String, CodingKey {
        case visitorID = "visitor_id"
        case category, name
        case userMemberId  = "user_memberid"
        case housingName = "housing_name"
        case email
        case contactNo = "contact_no"
        case vehicleType = "vehicle_type"
        case vehicleNo = "vehicle_no"
        case identificationType = "identification_type"
        case identificationNo = "identification_no"
        case purpose
        case visitorType = "visitor_type"
        case seasonType = "season_type"
        case validityStart = "validity_start"
        case validityEnd = "validity_end"
        case startTime = "start_time"
        case endTime = "end_time"
        case inTime = "in_time"
        case outTime = "out_time"
        case address
        case addressID = "address_id"
        case status, remarks
        case facePhoto = "face_photo"
        case identificationPhoto = "identification_photo"
        case qrImage = "qr_image"
        case userid
        case subscriberID = "subscriber_id"
        case createBy = "create_by"
        case createDate = "create_date"
        case modifyBy = "modify_by"
        case modifyDate = "modify_date"
    }
}
