//
//  ReuseIdentifier.swift
//  Housing
//
//  Created by Naveen Natrajan on 2020-12-12.
//

import Foundation
struct reuseCellIdentifier
{
    static let historyCellIdentifier = "history"
    static let servicesCellIdentifier = "services"
    static let faqCellIdentifier = "faq"
    static let intercomCellIdentifier = "intercom"
    static let noticeCellIdentifier = "noticeCell"
    static let accessCellIdentifier = "AccessListCell"
    static let visitorRecordCellIdentifier = "VisitorRecordCell"
    static let bulkSeasonListCellIdentifier = "BulkPassListCell"
    static let facilityHistoryCellIdentifier = "FacilityHistory"
    static let EbillingListCellIdentifier = "EbillingListCell"
    static let StatementOfAccountCellIdentifier = "StatementOfAccountCell"
    static let UsersListCellIdentifier = "UsersListCell"
}

struct reuseNibIdentifier
{
    static let historyNibIdentifier = "HistoryCell"
    static let servicesNibIdentifier = "ServicesCell"
    static let faqNibIdentifier = "FAQCell"
    static let intercomNibIdentifier = "IntercomCell"
    static let noticeNibIdentifier = "NoticeCell"
    static let accessNibIdentifier = "AccessListCell"
    static let visitorRecordNibIdentifier = "VisitorRecordCell"
    static let bulkSeasonListNibIdentifier = "BulkPassListCell"
    static let facilityHistoryNibIdentifier = "FacilityHistoryCell"
    static let EbillingListNibIdentifier = "EbillingListCell"
    static let StatementOfAccountCellNibIdentifier = "StatementOfAccountCell"
    static let UsersListCellNibIdentifier = "UsersListCell"
}

struct reuseCollectionIdentifier {
    static let announcemnetCellIdentifier = "AnnouncementCollectionCell"
    static let addressCellIdentifier = "AddressCollectionCell"
    static let dashboardCellIdentfier = "DashboardCell"
}


struct reuseCollectionNibIdentifier {
    static let announcemnetNibIdentifier = "AnnouncementCollectionCell"
    static let addressNibIdentifier = "AddressCollectionCell"
    static let dashboardNibIdentifier = "DashboardCell"
}

struct segueIdentifiers {
    static let vistorFavTOFavDetail = "visitorTOfav"
    static let noticeToSingle = "noticeToSingle"
    static let dashToSingle = "dashToSingle"
    static let dashToNotice = "dashToNotice"
    static let usefulInfoSingle = "usefulInfo"
    static let dashToAddress = "dashToAddress"
    static let addVisitorToBulkSeaseon = "toBulk"
    static let newVisitorContainer = "newVisitor"
    static let ebillToAttachment = "toBillAttachment"
    static let userToShowUser = "showUser"
    static let userToAddUser = "addUser"
    static let signUpToAgree = "signUPToAgree"
    static let agreeToPassword = "AgreeToPassword"
    static let passwordToCode = "passwordToCode"
    static let showUserToEdit = "showToEdit"
    static let qrScanToAccess = "scanToAccess"
    static let qrScanToVisitorPass = "qrToVisitor"
    static let qrScanToFacility = "qrScanToFacility"
    static let dashToSecurityScan = "dashToQrScan"
    static let settingsToMyProfile = "settingsToProfile"
    static let confirmPasswordEmailToPassword = "emailToPassword"
    static let facilityToPass = "factilityToPass"
    static let editProfileToUploadPRofileImage = "userToProfile"
    static let statementToPDF = "statementPDF"
    static let dashToSecurityRecord = "visitorRecordSecurity"
    static let dashPassword = "dashPassword"
}
struct dashBoardUserSegueIdentifiers
{
    static let visitor = "dashToVisitor"
    static let access = "dashToAccess"
    static let service = "dashToService"
    static let useful = "dashToUseful"
    static let users = "dashToUsers"
    static let facility = "dashToFacilities"
    static let faq = "dashToFaq"
    static let visitroRecord = "dashToVisitorRecord"
    static let ebilling = "dashToEbilling"
    static let statement = "dashToStatement"
    static let address = "dashToAddress"
}
struct userDefaultsKey
{
    static let address = "address"
    static let addressId = "addressID"
    static let isloggedIn = "isLoggedIn"
    static let subId = "subID"
    static let userID = "userID"
    static let name = "name"
    static let email = "email"
    static let housingName = "housingName"
    static let phoneNumber = "phoneNo"
    static let memberRole = "memberRole"
    static let identifiactionType = "identType"
    static let identificationNumber = "identNumber"
    static let memberRoleText = "memberRoleText"
    static let userMemberID = "userMemberId"
    static let profileImg = "img"
    static let termsAndCondition = "terms"
    static let privacyPolicy = "privacy"
    static let housingEmail = "hemail"
    static let housingPhone = "hphone"
    static let housinAddress = "haddress"
    static let housingIdentNUmber = "hident"
    static let housingIdentType = "htype"
    static let housingImage = "himg"
}

struct notificationNames {
    static let userAddedORUpdated = "userUpdated"
    static let passAddedOrUpdated = "passUpdated"
    static let favouriteAddedOrUpdated = "favouriteUpdated"
    static let applyFavoutites = "applyFavourite"
    static let applyFavChangeView = "applyFavChangeView"
    static let imageUpdated = "profileImageUpdated"
}

struct settingsSegue {
    static let profile = "settingsToProfile"
    static let contactUs = "settingsToContactUs"
    static let forgotPassword = "settingsToForgotPassword"
}
